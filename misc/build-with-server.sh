#!/usr/bin/env bash

echo Server: $OCELLUS_SERVER_URL

spring_profiles_active=inbuild ./gradlew bootRun --no-daemon &>run.log &

pushd frontend || exit
yarn build:scratch || exit
popd || exit

echo =========================

cat run.log
