package tech.acheron.ocellus.config

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType
import io.swagger.v3.oas.annotations.security.SecurityScheme
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import tech.acheron.ocellus.config.properties.SecurityProperties
import tech.acheron.ocellus.util.isDevelopment
import java.time.Duration

@Configuration
@EnableMethodSecurity(
    prePostEnabled = true,
)
@SecurityScheme(
    name = SecurityConfig.securitySchemeName,
    type = SecuritySchemeType.HTTP,
    scheme = "bearer",
    bearerFormat = "JWT",
)
class SecurityConfig(
    private val securityProperties: SecurityProperties,
) {
    companion object {
        const val securitySchemeName = "ocellus-bearer"
        val commonAuthority = SimpleGrantedAuthority("Ocellus")
    }

    val recoveryLifetime by lazy {
        Duration.ofSeconds(securityProperties.recoveryLifetime)
    }

    @Bean
    fun securityFilterChain(
        http: HttpSecurity,
        environment: Environment,
    ) =
        http.also { security ->
            security
                .csrf { it.disable() }
                .formLogin { it.disable() }

            if (isDevelopment(environment)) {
                security.cors { cors ->
                    cors.configurationSource(cors())
                }
            }
        }.build()!!

    @Bean
    fun passwordEncoder() = BCryptPasswordEncoder()

    @Bean
    @Profile("dev")
    fun cors() =
        UrlBasedCorsConfigurationSource().apply {
            registerCorsConfiguration(
                "/**",
                CorsConfiguration().apply {
                    allowedOrigins = listOf("*")
                    allowedHeaders = listOf("*")
                    allowedMethods = listOf("*")
                }
            )
        }
}