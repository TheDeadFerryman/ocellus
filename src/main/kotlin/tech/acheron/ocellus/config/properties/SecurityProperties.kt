package tech.acheron.ocellus.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.bind.ConstructorBinding

@ConfigurationProperties("ocellus.security")
data class SecurityProperties @ConstructorBinding constructor(
    val minPassword: Int,
    val signingKey: String,
    val tokenLifetime: Long,
    val tokenIssuer: String,
    val recoveryLifetime: Long,
)
