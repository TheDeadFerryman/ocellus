package tech.acheron.ocellus.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.bind.ConstructorBinding

@ConfigurationProperties("ocellus.meta")
data class MetaProperties @ConstructorBinding constructor(
    val baseUrl: String,
)
