package tech.acheron.ocellus.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.bind.ConstructorBinding

@ConfigurationProperties("ocellus.mailing")
data class MailingProperties @ConstructorBinding constructor(
    val recoverySender: String,
)