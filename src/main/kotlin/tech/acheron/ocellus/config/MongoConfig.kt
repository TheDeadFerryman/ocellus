package tech.acheron.ocellus.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.AuditorAware
import org.springframework.data.mongodb.config.EnableMongoAuditing
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import org.springframework.security.core.context.SecurityContextHolder
import tech.acheron.ocellus.data.model.impl.Commander
import java.util.*

@Profile("dev", "prod")
@Configuration
@EnableMongoAuditing(auditorAwareRef = "currentCmdrAuditor")
@EnableMongoRepositories(basePackages = ["tech.acheron.ocellus.data"])
class MongoConfig {
    @Bean("currentCmdrAuditor")
    fun auditorProvider() =
        AuditorAware {
            SecurityContextHolder.getContext()
                ?.authentication?.principal
                .let { it as? Commander }
                ?.let { Optional.of(it) }
                ?: Optional.empty()
        }
}