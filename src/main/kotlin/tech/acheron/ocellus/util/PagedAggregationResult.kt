package tech.acheron.ocellus.util

import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable

data class PagedAggregationResult<T>(
    val total: Long,
    val results: List<T>,
) {
    fun toPage(pageable: Pageable) =
        PageImpl(results, pageable, total)
}