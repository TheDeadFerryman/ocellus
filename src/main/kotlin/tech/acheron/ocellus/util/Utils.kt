package tech.acheron.ocellus.util

import org.bson.types.ObjectId
import org.springframework.core.env.Environment

fun wrapId(id: String) = try {
    ObjectId(id)
} catch (ex: IllegalArgumentException) {
    throw IllegalArgumentException("Invalid id!", ex)
}

fun isDevelopment(environment: Environment) =
    "dev" in environment.activeProfiles