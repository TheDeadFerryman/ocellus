package tech.acheron.ocellus.util

import org.springframework.security.core.context.SecurityContextHolder
import tech.acheron.ocellus.data.model.impl.Commander

object AuthUtils {
    fun currentUser() =
        SecurityContextHolder.getContext().authentication.principal as? Commander

    fun <T> withCurrentUser(action: (cmdr: Commander) -> T) =
        currentUser()?.let { action(it) }
}