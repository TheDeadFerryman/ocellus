package tech.acheron.ocellus.dto.ship

import com.fasterxml.jackson.annotation.JsonIgnore

data class ShipUpdateDTO(
    val name: String,
    val model: String,
    val data: String,
) {
    @get:JsonIgnore
    val dataRaw by lazy {
        ShipTransferHelper.decodeData(data)
    }
}