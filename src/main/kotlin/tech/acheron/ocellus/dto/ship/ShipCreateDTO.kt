package tech.acheron.ocellus.dto.ship

import com.fasterxml.jackson.annotation.JsonIgnore
import tech.acheron.ocellus.data.model.impl.Commander
import tech.acheron.ocellus.service.fleet.ShipCreator

data class ShipCreateDTO(
    val name: String,
    val model: String,
    val data: String,
) {
    @get:JsonIgnore
    val dataRaw by lazy {
        ShipTransferHelper.decodeData(data)
    }

    fun toCreator(author: Commander) = ShipCreator(
        name = name,
        author = author,
        model = model,
        data = dataRaw,
    )
}