package tech.acheron.ocellus.dto.ship

import tech.acheron.ocellus.data.model.impl.AccessLevel

data class ShipWithAccessLevelDTO(
    val ship: ShipDTO,
    val level: AccessLevel,
)