package tech.acheron.ocellus.dto.ship

import tech.acheron.ocellus.data.model.impl.Ship
import tech.acheron.ocellus.dto.cmdr.CommanderDTO
import tech.acheron.ocellus.service.fleet.ShipOperations
import java.time.Instant

data class ShipDTO(
    val id: String,
    val name: String,
    val author: CommanderDTO,
    val createdOn: Instant,
    val updatedOn: Instant,
    val updatedBy: CommanderDTO,

    val model: String,
    val data: String,
) {
    companion object {
        fun of(ship: Ship, data: ByteArray) = ShipDTO(
            id = ship.id,
            name = ship.name,
            author = CommanderDTO.of(ship.author),
            model = ship.model,
            createdOn = ship.createdOn,
            updatedOn = ship.updatedOn,
            updatedBy = CommanderDTO.of(ship.updatedBy),
            data = ShipTransferHelper.encodeData(data),
        )

        fun of(shipOperations: ShipOperations) =
            of(shipOperations.ship, shipOperations.getData())
    }
}