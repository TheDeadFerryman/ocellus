package tech.acheron.ocellus.dto.ship

import java.util.*

object ShipTransferHelper {
    fun encodeData(data: ByteArray): String =
        Base64.getEncoder().encodeToString(data)

    fun decodeData(data: String): ByteArray =
        Base64.getDecoder().decode(data)
}