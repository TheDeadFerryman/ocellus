package tech.acheron.ocellus.dto.ship

data class ShipCopyDTO(
    val toFleet: String,
)