package tech.acheron.ocellus.dto.ship

data class ShipRenameDTO(
    val name: String,
)