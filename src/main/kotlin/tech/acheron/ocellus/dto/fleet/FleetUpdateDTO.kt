package tech.acheron.ocellus.dto.fleet

class FleetUpdateDTO(
    val fleetId: String,
    val name: String,
    val isPublic: Boolean,
)