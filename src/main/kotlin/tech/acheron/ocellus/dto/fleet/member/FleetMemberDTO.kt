package tech.acheron.ocellus.dto.fleet.member

import tech.acheron.ocellus.data.model.impl.FleetMember
import tech.acheron.ocellus.data.model.impl.AccessLevel
import tech.acheron.ocellus.dto.cmdr.CommanderDTO

data class FleetMemberDTO(
    val cmdr: CommanderDTO,
    val level: AccessLevel,
) {
    companion object {
        fun of(fleetMember: FleetMember) = FleetMemberDTO(
            cmdr = CommanderDTO.of(fleetMember.cmdr),
            level = fleetMember.level,
        )
    }
}