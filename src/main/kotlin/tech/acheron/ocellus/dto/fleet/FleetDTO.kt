package tech.acheron.ocellus.dto.fleet

import tech.acheron.ocellus.data.model.Fleet
import tech.acheron.ocellus.dto.cmdr.CommanderDTO
import tech.acheron.ocellus.service.fleet.FleetOperations
import java.time.Instant

data class FleetDTO(
    val id: String,
    val name: String,
    val owner: CommanderDTO,
    val isPublic: Boolean,
    val membersCount: Int,
    val shipsCount: Int,
    val createdOn: Instant,
) {
    companion object {
        fun of(
            fleet: Fleet,
            membersCount: Int,
            shipsCount: Int,
        ) = FleetDTO(
            id = fleet.id,
            name = fleet.name,
            owner = CommanderDTO.of(fleet.owner),
            isPublic = fleet.isPublic,
            membersCount = membersCount,
            shipsCount = shipsCount,
            createdOn = fleet.createdOn,
        )

        fun of(
            fleetOperations: FleetOperations,
        ) = of(
            fleet = fleetOperations.fleet,
            membersCount = fleetOperations.access.countMembers(),
            shipsCount = fleetOperations.ships.count()
        )
    }
}