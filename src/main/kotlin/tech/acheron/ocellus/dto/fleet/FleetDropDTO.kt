package tech.acheron.ocellus.dto.fleet

class FleetDropDTO(
    val fleetId: String,
)