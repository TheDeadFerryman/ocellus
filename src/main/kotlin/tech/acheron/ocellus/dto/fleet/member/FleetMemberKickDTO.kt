package tech.acheron.ocellus.dto.fleet.member

data class FleetMemberKickDTO(
    val cmdr: String,
)