package tech.acheron.ocellus.dto.fleet

class FleetInputDTO(
    val name: String,
    val isPublic: Boolean,
)