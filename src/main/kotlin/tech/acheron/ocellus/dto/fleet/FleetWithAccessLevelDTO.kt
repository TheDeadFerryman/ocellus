package tech.acheron.ocellus.dto.fleet

import tech.acheron.ocellus.data.model.impl.AccessLevel

data class FleetWithAccessLevelDTO(
    val fleet: FleetDTO,
    val level: AccessLevel,
)
