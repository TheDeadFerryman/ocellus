package tech.acheron.ocellus.dto.fleet.member

import tech.acheron.ocellus.data.model.impl.AccessLevel

data class FleetMemberInputDTO(
    val cmdr: String,
    val level: AccessLevel,
)