package tech.acheron.ocellus.dto.cmdr

data class TokenDTO(
    val token: String?,
)