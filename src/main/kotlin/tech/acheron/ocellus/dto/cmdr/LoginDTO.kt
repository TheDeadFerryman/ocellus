package tech.acheron.ocellus.dto.cmdr

data class LoginDTO(
    val name: String,
    val password: String,
)