package tech.acheron.ocellus.dto.cmdr

data class RecoveryRequestDTO(
    val name: String,
    val email: String,
)
