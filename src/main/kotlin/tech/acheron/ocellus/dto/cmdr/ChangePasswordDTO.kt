package tech.acheron.ocellus.dto.cmdr

data class ChangePasswordDTO(
    val current: String,
    val new: String,
)
