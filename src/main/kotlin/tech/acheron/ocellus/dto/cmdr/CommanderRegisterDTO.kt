package tech.acheron.ocellus.dto.cmdr

data class CommanderRegisterDTO(
    var email: String = "",
    var name: String = "",
    var password: String = "",
)