package tech.acheron.ocellus.dto.cmdr

import tech.acheron.ocellus.data.model.impl.Commander

data class CommanderDTO(
    val id: String,
    val name: String,
) {
    companion object {
        fun of(cmdr: Commander) = CommanderDTO(
            cmdr.id, cmdr.name
        )
    }
}
