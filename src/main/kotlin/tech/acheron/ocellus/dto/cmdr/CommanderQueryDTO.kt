package tech.acheron.ocellus.dto.cmdr

data class CommanderQueryDTO(
    val query: String,
    val excludeIds: List<String>?,
    val limit: Int?,
)
