package tech.acheron.ocellus.dto.cmdr

data class RecoveryDTO(
    val token: String,
    val password: String,
)
