package tech.acheron.ocellus.dto.common

import org.springframework.data.domain.Page

class PageDTO<T> private constructor(
    val content: List<T>,
    val total: Long,
    val pages: Int,
) {
    companion object {
        fun <T> of(page: Page<T>) = PageDTO(
            content = page.content,
            total = page.totalElements,
            pages = page.totalPages,
        )
    }
}
