package tech.acheron.ocellus.filter

import jakarta.servlet.FilterChain
import jakarta.servlet.ServletRequest
import jakarta.servlet.ServletResponse
import jakarta.servlet.http.HttpServletRequest
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.GenericFilterBean
import tech.acheron.ocellus.config.SecurityConfig
import tech.acheron.ocellus.service.security.AuthenticationService

@Component
class BearerTokenFilter(
    private val authenticationService: AuthenticationService,
) : GenericFilterBean() {
    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        val authentication = (extractToken(request))?.let {
            authenticationService.getAuthentication(it)
        }

        if (authentication != null) {
            SecurityContextHolder.getContext().authentication = authentication
        }

        chain.doFilter(request, response)
    }

    private fun extractToken(request: ServletRequest?) =
        (request as? HttpServletRequest)?.let {
            extractHeaderToken(it) ?: extractCookieToken(it)
        }

    private fun extractCookieToken(request: HttpServletRequest?) =
        request?.let { req ->
            req.cookies?.find {
                it.name == SecurityConfig.securitySchemeName && it.isHttpOnly
            }?.value
        }


    private fun extractHeaderToken(request: HttpServletRequest?) =
        request?.getHeader("Authorization")
            ?.let {
                if (it.startsWith("Bearer ")) {
                    it.replace(Regex("^(Bearer )"), "")
                } else {
                    null
                }
            }
}