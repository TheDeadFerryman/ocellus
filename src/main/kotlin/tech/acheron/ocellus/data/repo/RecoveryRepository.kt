package tech.acheron.ocellus.data.repo

import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import tech.acheron.ocellus.data.model.impl.Recovery

interface RecoveryRepository : MongoRepository<Recovery, ObjectId>