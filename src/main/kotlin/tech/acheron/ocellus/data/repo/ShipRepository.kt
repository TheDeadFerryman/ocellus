package tech.acheron.ocellus.data.repo

import org.bson.types.ObjectId
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import tech.acheron.ocellus.data.model.impl.MongoFleet
import tech.acheron.ocellus.data.model.impl.Ship

interface ShipRepository : MongoRepository<Ship, ObjectId> {
    @Query("{ _id: ?0, 'fleet.\$id': ?1 }")
    fun findByIdAndFleet(id: ObjectId, fleetId: ObjectId): Ship?

    fun findAllByFleet(fleet: MongoFleet): List<Ship>

    fun findAllByFleet(fleet: MongoFleet, page: Pageable): Page<Ship>

    fun countAllByFleet(fleet: MongoFleet): Int

    @Query("{ \$and: [ { 'fleet.\$id': ?0 }, { \$text: { \$search: ?1 } } ] }")
    fun searchForFleet(fleetId: ObjectId, query: String): List<Ship>

    @Query("{ \$and: [ { 'fleet.\$id': ?0 }, { \$text: { \$search: ?1 } } ] }")
    fun searchForFleet(fleetId: ObjectId, query: String, page: Pageable): Page<Ship>
}