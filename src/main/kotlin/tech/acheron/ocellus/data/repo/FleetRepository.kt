package tech.acheron.ocellus.data.repo

import org.bson.types.ObjectId
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import tech.acheron.ocellus.data.model.impl.MongoFleet

interface FleetRepository : MongoRepository<MongoFleet, ObjectId> {
    @Query(
        "{ \$and: [" +
                "{ 'owner.\$id': ?0 }," +
                "{  \$text: { \$search: ?1 } }] }"
    )
    fun searchByOwnerAndQuery(
        cmdrId: ObjectId,
        name: String,
    ): List<MongoFleet>

    @Query(
        "{ \$and: [" +
                "{ 'owner.\$id': ?0 }," +
                "{  \$text: { \$search: ?1 } }] }"
    )
    fun searchByOwnerAndQuery(
        cmdrId: ObjectId,
        name: String,
        page: Pageable,
    ): Page<MongoFleet>
}
