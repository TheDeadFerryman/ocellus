package tech.acheron.ocellus.data.repo

import org.bson.types.ObjectId
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import tech.acheron.ocellus.data.model.impl.Commander

interface CommanderRepository : MongoRepository<Commander, ObjectId> {
    @Query("{ name: { \$regex: ?0, \$options: 'i' } }")
    fun searchNameMatches(query: String, page: Pageable): Page<Commander>

    @Query(
        "{ \$and: [{ name: { \$regex: ?0, \$options: 'i' } }, " +
                "{ _id: { \$nin: ?1 } }] }"
    )
    fun searchNameMatchesExcludingIds(
        query: String,
        excludeIds: List<ObjectId>,
        page: Pageable,
    ): Page<Commander>

    fun findByName(name: String): Commander?

    fun findByNameAndEmail(name: String, email: String): Commander?

    fun existsByEmail(email: String): Boolean
    fun existsByName(name: String): Boolean
}