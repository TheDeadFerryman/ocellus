package tech.acheron.ocellus.data.repo

import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import tech.acheron.ocellus.data.model.impl.Commander
import tech.acheron.ocellus.data.model.impl.FleetMember
import tech.acheron.ocellus.data.model.impl.MongoFleet

interface FleetMemberRepository : MongoRepository<FleetMember, ObjectId> {
    fun countAllByFleet(fleet: MongoFleet): Int

    fun findAllByFleet(fleet: MongoFleet): List<FleetMember>

    fun findByFleetAndCmdr(fleet: MongoFleet, cmdr: Commander): FleetMember?
}