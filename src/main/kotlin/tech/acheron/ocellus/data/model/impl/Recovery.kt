package tech.acheron.ocellus.data.model.impl

import org.bson.types.ObjectId
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document
data class Recovery(
    @DBRef
    val cmdr: Commander,
) : MongoDocument() {
    @Id
    override lateinit var mongoId: ObjectId

    @Indexed(expireAfter = "#{@securityConfig.recoveryLifetime}")
    @CreatedDate
    lateinit var issuedOn: Instant
}