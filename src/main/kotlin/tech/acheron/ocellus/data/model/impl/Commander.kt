package tech.acheron.ocellus.data.model.impl

import com.fasterxml.jackson.annotation.JsonIgnore
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.security.core.userdetails.UserDetails
import tech.acheron.ocellus.config.SecurityConfig


@Document
data class Commander(
    val name: String,
    val email: String,
    var encodedPassword: String,
) : UserDetails, MongoDocument() {
    @Id
    @JsonIgnore
    override lateinit var mongoId: ObjectId

    override fun getAuthorities() = listOf(
        SecurityConfig.commonAuthority,
    )

    override fun getPassword() = encodedPassword

    override fun getUsername() = name

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true

    override fun isCredentialsNonExpired() = true

    override fun isEnabled() = true
}