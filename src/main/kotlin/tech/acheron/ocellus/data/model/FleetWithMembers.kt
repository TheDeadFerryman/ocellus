package tech.acheron.ocellus.data.model

import com.fasterxml.jackson.annotation.JsonIgnore
import tech.acheron.ocellus.data.model.impl.FleetMember

data class FleetWithMembers(
    @JsonIgnore
    val fleet: Fleet,
    val members: List<FleetMember>,
) : Fleet by fleet