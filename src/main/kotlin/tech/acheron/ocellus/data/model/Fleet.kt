package tech.acheron.ocellus.data.model

import tech.acheron.ocellus.data.model.impl.Commander
import java.time.Instant

interface Fleet {
    val id: String

    var name: String

    var owner: Commander
    var isPublic: Boolean

    var createdOn: Instant
}