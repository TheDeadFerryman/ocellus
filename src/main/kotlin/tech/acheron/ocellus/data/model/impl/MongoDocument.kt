package tech.acheron.ocellus.data.model.impl

import org.bson.types.ObjectId
import org.springframework.data.annotation.Transient

abstract class MongoDocument {
    abstract var mongoId: ObjectId

    @get:Transient
    val id get() = mongoId.toHexString()!!
}