package tech.acheron.ocellus.data.model.impl

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document

@Document(FleetMember.collection)
data class FleetMember(
    @DBRef
    val fleet: MongoFleet,
    @DBRef
    val cmdr: Commander,
    var level: AccessLevel,
) : MongoDocument() {
    companion object {
        const val collection = "fleetMember"
    }

    @Id
    override lateinit var mongoId: ObjectId
}