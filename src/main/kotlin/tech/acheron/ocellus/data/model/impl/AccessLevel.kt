package tech.acheron.ocellus.data.model.impl

import io.swagger.v3.oas.annotations.media.Schema

@Schema(enumAsRef = true)
enum class AccessLevel(
    val level: Int,
) {
    None(0),
    Read(1),
    Add(2),
    Edit(4),
    Admin(256),
}