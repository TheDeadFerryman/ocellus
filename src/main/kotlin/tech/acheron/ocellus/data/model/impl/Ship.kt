package tech.acheron.ocellus.data.model.impl

import com.fasterxml.jackson.annotation.JsonIgnore
import org.bson.types.Binary
import org.bson.types.ObjectId
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.mongodb.core.index.TextIndexed
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document
data class Ship(
    @TextIndexed
    var name: String,
    @DBRef(lazy = true)
    val author: Commander,
    @JsonIgnore
    @DBRef(lazy = true)
    val fleet: MongoFleet,
    var model: String,
    var data: Binary,
) : MongoDocument() {
    @Id
    override lateinit var mongoId: ObjectId

    @CreatedDate
    lateinit var createdOn: Instant

    @LastModifiedDate
    lateinit var updatedOn: Instant

    @DBRef
    @LastModifiedBy
    lateinit var updatedBy: Commander
}