package tech.acheron.ocellus.data.model.impl

import com.fasterxml.jackson.annotation.JsonIgnore
import org.bson.types.ObjectId
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.index.TextIndexed
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import tech.acheron.ocellus.data.model.Fleet
import java.time.Instant

@Document(MongoFleet.collection)
data class MongoFleet(
    @TextIndexed
    override var name: String,
    @DBRef
    @Indexed
    override var owner: Commander,
    override var isPublic: Boolean,
) : Fleet, MongoDocument() {
    companion object {
        const val collection = "fleet"
    }

    @Id
    @JsonIgnore
    override lateinit var mongoId: ObjectId

    @CreatedDate
    override lateinit var createdOn: Instant
}