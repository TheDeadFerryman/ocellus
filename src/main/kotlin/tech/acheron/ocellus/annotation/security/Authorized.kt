package tech.acheron.ocellus.annotation.security

import io.swagger.v3.oas.annotations.security.SecurityRequirement
import org.springframework.security.access.prepost.PreAuthorize
import tech.acheron.ocellus.config.SecurityConfig

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@PreAuthorize("isAuthenticated()")
@SecurityRequirement(name = SecurityConfig.securitySchemeName)
annotation class Authorized
