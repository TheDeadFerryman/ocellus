package tech.acheron.ocellus

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.servers.Server
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan(basePackages = ["tech.acheron.ocellus.config.properties"])
@OpenAPIDefinition(
    info = Info(
        title = "Ocellus API",
        contact = Contact(
            name = "Karl F. Meinkopf aka CMDR The Dead Ferryman",
            url = "https://acheron.tech/",
            email = "k.meinkopf@gmail.com",
        ),
        version = "1.0.1"
    ),
    servers = [
        Server(
            url = "https://ocellus.acheron.tech/",
        ),
        Server(
            url = "http://localhost:8080/",
        )
    ],
)
class OcellusApplication

fun main(args: Array<String>) {
    runApplication<OcellusApplication>(*args)
}
