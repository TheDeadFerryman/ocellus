package tech.acheron.ocellus.service.util.impl

import org.springframework.stereotype.Service
import tech.acheron.ocellus.service.util.IdGenerator
import java.time.Instant

@Service
class TimeBasedIdGenerator : IdGenerator {
    override fun generate(seed: String): String {
        val slug = Instant.now().epochSecond.toString(24)
        val nameTag = seed.split(Regex("""\s+""")).joinToString("-") { it.lowercase() }

        return "$slug-$nameTag"
    }
}