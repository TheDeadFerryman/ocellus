package tech.acheron.ocellus.service.util.impl

import org.apache.commons.compress.compressors.lzma.LZMACompressorInputStream
import org.apache.commons.compress.compressors.lzma.LZMACompressorOutputStream
import org.springframework.stereotype.Service
import tech.acheron.ocellus.service.util.DataCompressor
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream

@Service
class LZMADataCompressor : DataCompressor {
    override fun compress(data: ByteArray) =
        ByteArrayOutputStream()
            .also {
                LZMACompressorOutputStream(it)
                    .apply { write(data) }
                    .finish()
            }
            .toByteArray()

    override fun decompress(compressed: ByteArray) =
        LZMACompressorInputStream(ByteArrayInputStream(compressed))
            .readAllBytes()
}