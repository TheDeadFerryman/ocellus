package tech.acheron.ocellus.service.util.impl

import org.bson.types.Binary
import org.springframework.stereotype.Service
import tech.acheron.ocellus.service.util.DataCompressor

@Service
class MongoCompressedBinaryHelper(
    private val dataCompressor: DataCompressor,
) {
    fun wrap(data: ByteArray) =
        Binary(dataCompressor.compress(data))

    fun unwrap(data: Binary) =
        dataCompressor.decompress(data.data)
}