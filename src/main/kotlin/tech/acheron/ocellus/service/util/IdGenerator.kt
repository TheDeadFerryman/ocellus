package tech.acheron.ocellus.service.util

interface IdGenerator {
    fun generate(seed: String): String
}