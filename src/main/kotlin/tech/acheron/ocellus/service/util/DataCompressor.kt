package tech.acheron.ocellus.service.util

interface DataCompressor {
    fun compress(data: ByteArray): ByteArray

    fun decompress(compressed: ByteArray): ByteArray
}