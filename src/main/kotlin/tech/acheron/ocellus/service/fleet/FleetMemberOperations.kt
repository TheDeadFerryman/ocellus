package tech.acheron.ocellus.service.fleet

import tech.acheron.ocellus.data.model.impl.AccessLevel
import tech.acheron.ocellus.data.model.impl.FleetMember

interface FleetMemberOperations {
    val member: FleetMember

    fun setLevel(newLevel: AccessLevel): FleetMemberOperations

    fun delete()
}