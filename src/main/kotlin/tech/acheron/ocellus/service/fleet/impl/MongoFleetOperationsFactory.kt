package tech.acheron.ocellus.service.fleet.impl

import org.springframework.stereotype.Service
import tech.acheron.ocellus.data.model.Fleet
import tech.acheron.ocellus.data.model.FleetWithMembers
import tech.acheron.ocellus.data.model.impl.FleetMember
import tech.acheron.ocellus.data.model.impl.MongoFleet
import tech.acheron.ocellus.data.model.impl.Ship
import tech.acheron.ocellus.data.repo.FleetMemberRepository
import tech.acheron.ocellus.data.repo.FleetRepository
import tech.acheron.ocellus.data.repo.ShipRepository
import tech.acheron.ocellus.service.util.impl.MongoCompressedBinaryHelper

@Service
class MongoFleetOperationsFactory(
    private val fleetRepository: FleetRepository,
    private val memberRepository: FleetMemberRepository,
    private val shipRepository: ShipRepository,
    private val binaryHelper: MongoCompressedBinaryHelper,
) {
    fun wrapFleet(fleet: Fleet) =
        MongoFleetOperations(ensureFleet(fleet), fleetRepository, this)

    fun wrapFleetAccess(fleet: Fleet) =
        MongoFleetAccessOperations(ensureFleet(fleet), fleetRepository, memberRepository, this)

    fun wrapFleetShips(fleet: Fleet) =
        MongoFleetShipsOperations(ensureFleet(fleet), shipRepository, binaryHelper, this)

    fun wrapShip(ship: Ship) =
        MongoShipOperations(ship, shipRepository, binaryHelper)

    fun wrapMember(member: FleetMember) =
        MongoFleetMemberOperations(member, memberRepository)

    private fun ensureFleet(fleet: Fleet): MongoFleet =
        when (fleet) {
            is FleetWithMembers -> ensureFleet(fleet.fleet)
            is MongoFleet -> fleet
            else -> throw IllegalArgumentException("implementation mismatch!")
        }
}