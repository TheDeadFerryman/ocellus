package tech.acheron.ocellus.service.fleet.impl

import com.mongodb.MongoQueryException
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import tech.acheron.ocellus.data.model.impl.Commander
import tech.acheron.ocellus.data.model.impl.MongoFleet
import tech.acheron.ocellus.data.model.impl.Ship
import tech.acheron.ocellus.data.repo.ShipRepository
import tech.acheron.ocellus.service.fleet.FleetShipsOperations
import tech.acheron.ocellus.service.fleet.ShipCreator
import tech.acheron.ocellus.service.util.impl.MongoCompressedBinaryHelper
import tech.acheron.ocellus.util.wrapId

class MongoFleetShipsOperations(
    private val fleet: MongoFleet,
    private val shipRepository: ShipRepository,
    private val binaryHelper: MongoCompressedBinaryHelper,
    private val fleetOperationsFactory: MongoFleetOperationsFactory,
) : FleetShipsOperations {
    override fun create(
        creator: ShipCreator,
    ) = shipRepository.save(
        shipFromCreator(creator)
    )

    override fun create(
        name: String,
        author: Commander,
        model: String,
        data: ByteArray,
    ) = create(ShipCreator(name, author, model, data))

    override fun createBulk(creators: List<ShipCreator>): List<Ship> =
        shipRepository.saveAll(creators.map(::shipFromCreator))

    override fun get(id: String) =
        shipRepository.findByIdAndFleet(wrapId(id), wrapId(fleet.id))

    override fun getAll() =
        shipRepository.findAllByFleet(fleet)

    override fun getAll(page: Pageable) =
        shipRepository.findAllByFleet(fleet, page)

    override fun count() =
        shipRepository.countAllByFleet(fleet)

    override fun findByName(query: String) = try {
        shipRepository.searchForFleet(wrapId(fleet.id), query)
    } catch (e: MongoQueryException) {
        if (e.errorCodeName == "IndexNotFound") {
            listOf()
        } else {
            throw e
        }
    }

    override fun findByName(query: String, page: Pageable) = try {
        shipRepository.searchForFleet(wrapId(fleet.id), query, page)
    } catch (e: MongoQueryException) {
        if (e.errorCodeName == "IndexNotFound") {
            Page.empty(page)
        } else {
            throw e
        }
    }

    override fun wrap(ship: Ship) =
        fleetOperationsFactory.wrapShip(ship)

    private fun shipFromCreator(creator: ShipCreator) = Ship(
        name = creator.name,
        author = creator.author,
        fleet = fleet,
        model = creator.model,
        data = binaryHelper.wrap(creator.data),
    )
}