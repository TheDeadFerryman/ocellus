package tech.acheron.ocellus.service.fleet.impl

import tech.acheron.ocellus.data.model.impl.AccessLevel
import tech.acheron.ocellus.data.model.impl.FleetMember
import tech.acheron.ocellus.data.repo.FleetMemberRepository
import tech.acheron.ocellus.service.fleet.FleetMemberOperations

class MongoFleetMemberOperations(
    override val member: FleetMember,
    private val memberRepository: FleetMemberRepository,
) : FleetMemberOperations {
    override fun setLevel(newLevel: AccessLevel) = wrap {
        memberRepository.save(member.apply {
            level = newLevel
        })
    }

    override fun delete() =
        memberRepository.delete(member)

    private fun wrap(action: () -> FleetMember) =
        MongoFleetMemberOperations(action(), memberRepository)
}