package tech.acheron.ocellus.service.fleet

import tech.acheron.ocellus.data.model.Fleet

interface FleetOperations {
    val fleet: Fleet

    val access: FleetAccessOperations
    val ships: FleetShipsOperations

    fun rename(newName: String): FleetOperations

    fun delete()
}