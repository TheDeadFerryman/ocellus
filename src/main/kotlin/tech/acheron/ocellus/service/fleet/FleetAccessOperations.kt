package tech.acheron.ocellus.service.fleet

import tech.acheron.ocellus.data.model.impl.AccessLevel
import tech.acheron.ocellus.data.model.impl.Commander
import tech.acheron.ocellus.data.model.impl.FleetMember
import tech.acheron.ocellus.data.model.impl.Ship

interface FleetAccessOperations {
    fun getAccessLevel(cmdr: Commander): AccessLevel

    fun hasAccessLevel(cmdr: Commander, requiredLevel: AccessLevel): Boolean

    fun getShipAccessLevel(cmdr: Commander, ship: Ship): AccessLevel

    fun hasShipAccessLevel(cmdr: Commander, ship: Ship, requiredLevel: AccessLevel): Boolean

    fun setPublic(newPublic: Boolean): FleetAccessOperations

    fun getMembers(): List<FleetMember>

    fun countMembers(): Int

    fun getMember(cmdr: Commander): FleetMember?

    fun addMember(cmdr: Commander, level: AccessLevel): FleetMember

    fun wrap(fleetMember: FleetMember): FleetMemberOperations
}