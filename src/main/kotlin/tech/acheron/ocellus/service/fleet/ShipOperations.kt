package tech.acheron.ocellus.service.fleet

import tech.acheron.ocellus.data.model.impl.Commander
import tech.acheron.ocellus.data.model.impl.Ship

interface ShipOperations {
    val ship: Ship

    fun getData(): ByteArray

    fun rename(newName: String): ShipOperations

    fun update(newName: String, newModel: String, newData: ByteArray): ShipOperations

    fun copy(to: FleetShipsOperations, asCmdr: Commander): ShipOperations

    fun delete()
}