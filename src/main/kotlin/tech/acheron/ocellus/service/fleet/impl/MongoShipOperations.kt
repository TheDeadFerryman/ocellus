package tech.acheron.ocellus.service.fleet.impl

import tech.acheron.ocellus.data.model.impl.Commander
import tech.acheron.ocellus.data.model.impl.Ship
import tech.acheron.ocellus.data.repo.ShipRepository
import tech.acheron.ocellus.service.fleet.FleetShipsOperations
import tech.acheron.ocellus.service.fleet.ShipOperations
import tech.acheron.ocellus.service.util.impl.MongoCompressedBinaryHelper

class MongoShipOperations(
    override val ship: Ship,
    private val shipRepository: ShipRepository,
    private val binaryHelper: MongoCompressedBinaryHelper,
) : ShipOperations {
    override fun getData() =
        binaryHelper.unwrap(ship.data)

    override fun rename(newName: String) = wrap {
        saveUpdated {
            name = newName
        }
    }

    override fun update(newName: String, newModel: String, newData: ByteArray) = wrap {
        saveUpdated {
            name = newName
            model = newModel
            data = binaryHelper.wrap(newData)
        }
    }

    override fun copy(to: FleetShipsOperations, asCmdr: Commander) = wrap {
        to.create(
            name = ship.name,
            author = asCmdr,
            model = ship.model,
            data = binaryHelper.unwrap(ship.data),
        )
    }

    override fun delete() =
        shipRepository.delete(ship)

    private fun wrap(action: () -> Ship) =
        MongoShipOperations(action(), shipRepository, binaryHelper)

    private fun saveUpdated(action: Ship.() -> Unit) =
        shipRepository.save(ship.apply(action))
}