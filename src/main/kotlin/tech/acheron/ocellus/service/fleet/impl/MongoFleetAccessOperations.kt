package tech.acheron.ocellus.service.fleet.impl

import tech.acheron.ocellus.data.model.impl.*
import tech.acheron.ocellus.data.repo.FleetMemberRepository
import tech.acheron.ocellus.data.repo.FleetRepository
import tech.acheron.ocellus.service.fleet.FleetAccessOperations

class MongoFleetAccessOperations(
    private val fleet: MongoFleet,
    private val fleetRepository: FleetRepository,
    private val fleetMemberRepository: FleetMemberRepository,
    private val fleetOperationsFactory: MongoFleetOperationsFactory,
) : FleetAccessOperations {
    override fun getAccessLevel(cmdr: Commander): AccessLevel =
        if (cmdr.id == fleet.owner.id) {
            AccessLevel.Admin
        } else {
            getMember(cmdr)?.level ?: if (fleet.isPublic) {
                AccessLevel.Read
            } else {
                AccessLevel.None
            }
        }

    override fun hasAccessLevel(cmdr: Commander, requiredLevel: AccessLevel) =
        getAccessLevel(cmdr).level >= requiredLevel.level

    override fun getShipAccessLevel(cmdr: Commander, ship: Ship) =
        if (
            hasAccessLevel(cmdr, AccessLevel.Edit) ||
            (hasAccessLevel(cmdr, AccessLevel.Add)
                    && ship.author.id == cmdr.id)
        ) {
            AccessLevel.Edit
        } else {
            getAccessLevel(cmdr)
        }

    override fun hasShipAccessLevel(
        cmdr: Commander, ship: Ship, requiredLevel: AccessLevel,
    ) = getShipAccessLevel(cmdr, ship).level >= requiredLevel.level

    override fun getMembers() =
        fleetMemberRepository.findAllByFleet(fleet)

    override fun countMembers() =
        fleetMemberRepository.countAllByFleet(fleet)

    override fun getMember(cmdr: Commander) =
        fleetMemberRepository.findByFleetAndCmdr(fleet, cmdr)

    override fun setPublic(newPublic: Boolean) =
        fleetRepository.save(fleet.apply {
            isPublic = newPublic
        }).let(fleetOperationsFactory::wrapFleetAccess)

    override fun addMember(cmdr: Commander, level: AccessLevel) =
        fleetMemberRepository.save(
            FleetMember(fleet, cmdr, level)
        )

    override fun wrap(fleetMember: FleetMember) =
        fleetOperationsFactory.wrapMember(fleetMember)
}