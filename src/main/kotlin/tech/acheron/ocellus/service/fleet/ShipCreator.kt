package tech.acheron.ocellus.service.fleet

import tech.acheron.ocellus.data.model.impl.Commander

data class ShipCreator(
    val name: String,
    val author: Commander,
    val model: String,
    val data: ByteArray,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        if (other !is ShipCreator) return false

        if (name != other.name) return false
        if (author != other.author) return false
        if (model != other.model) return false
        return data.contentEquals(other.data)
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + author.hashCode()
        result = 31 * result + model.hashCode()
        result = 31 * result + data.contentHashCode()
        return result
    }
}