package tech.acheron.ocellus.service.fleet.impl

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregate
import org.springframework.data.mongodb.core.aggregation.Aggregation
import org.springframework.data.mongodb.core.aggregation.LookupOperation
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.stereotype.Service
import tech.acheron.ocellus.data.model.Fleet
import tech.acheron.ocellus.data.model.impl.AccessLevel
import tech.acheron.ocellus.data.model.impl.Commander
import tech.acheron.ocellus.data.model.impl.FleetMember
import tech.acheron.ocellus.data.model.impl.MongoFleet
import tech.acheron.ocellus.data.repo.FleetRepository
import tech.acheron.ocellus.service.fleet.FleetService
import tech.acheron.ocellus.util.PagedAggregationResult
import tech.acheron.ocellus.util.wrapId

@Service
class MongoFleetService(
    private val fleetRepository: FleetRepository,
    private val fleetOperationsFactory: MongoFleetOperationsFactory,
    private val mongoTemplate: MongoTemplate,
) : FleetService {
    companion object {
        private val writableLevels = listOf(
            AccessLevel.Add, AccessLevel.Edit, AccessLevel.Admin,
        )
    }

    private val writableLevelsCriteria by lazy {
        Criteria().orOperator(
            writableLevels.map {
                Criteria.where("members.level").`is`(it)
            }
        )
    }

    override fun create(name: String, owner: Commander, isPublic: Boolean) =
        fleetRepository.save(
            MongoFleet(
                name = name,
                owner = owner,
                isPublic = isPublic,
            )
        )

    override fun wrap(fleet: Fleet) =
        fleetOperationsFactory.wrapFleet(fleet)

    override fun get(id: String): Fleet? =
        fleetRepository.findById(wrapId(id))
            .orElse(null)

    override fun searchAll(cmdr: Commander, query: String): List<Fleet> =
        Aggregation.newAggregation(
            *searchAllAggregation(query),
            *getAllAggregation(cmdr),
        ).let {
            mongoTemplate
                .aggregate<Fleet>(it, MongoFleet.collection)
                .mappedResults
        }

    override fun searchAll(cmdr: Commander, query: String, page: Pageable) =
        Aggregation.newAggregation(
            *searchAllAggregation(query),
            *getAllAggregation(cmdr),
            *paginationAggregation(page)
        ).let {
            mongoTemplate
                .aggregate<PagedAggregationResult<Fleet>>(
                    it, MongoFleet.collection,
                )
                .uniqueMappedResult
                ?.toPage(page)
                ?: Page.empty(page)
        }

    override fun getAll(cmdr: Commander): List<Fleet> =
        Aggregation.newAggregation(
            *getAllAggregation(cmdr)
        ).let {
            mongoTemplate
                .aggregate<Fleet>(it, MongoFleet.collection)
                .mappedResults
        }

    override fun getAll(cmdr: Commander, page: Pageable): Page<Fleet> =
        Aggregation.newAggregation(
            *getAllAggregation(cmdr),
            *paginationAggregation(page),
        ).let {
            mongoTemplate
                .aggregate<PagedAggregationResult<Fleet>>(
                    it, MongoFleet.collection,
                )
                .uniqueMappedResult
                ?.toPage(page)
                ?: Page.empty(page)
        }

    override fun getAllWritable(cmdr: Commander, limit: Int): List<Fleet> =
        Aggregation.newAggregation(
            *getAllWritableAggregation(cmdr),
            Aggregation.limit(limit.toLong()),
        ).let {
            mongoTemplate
                .aggregate<Fleet>(it, MongoFleet.collection)
                .mappedResults
        }

    override fun searchAllWritable(cmdr: Commander, query: String, limit: Int): List<Fleet> =
        Aggregation.newAggregation(
            *searchAllAggregation(query),
            *getAllWritableAggregation(cmdr),
            Aggregation.limit(limit.toLong()),
        ).let {
            mongoTemplate
                .aggregate<Fleet>(it, MongoFleet.collection)
                .mappedResults
        }

    private fun getAllAggregation(cmdr: Commander) = listOf(
        LookupOperation.newLookup()
            .from(FleetMember.collection)
            .localField("_id")
            .foreignField("fleet.\$id")
            .`as`("members"),
        Aggregation.match(
            Criteria().orOperator(
                Criteria.where("members.cmdr.\$id").`is`(wrapId(cmdr.id)),
                Criteria.where("owner.\$id").`is`(wrapId(cmdr.id))
            )
        )
    ).toTypedArray()

    private fun searchAllAggregation(query: String) = listOf(
        Aggregation.match(
            Criteria.where("name")
                .regex(Regex.escape(query))
        ),
    ).toTypedArray()

    private fun getAllWritableAggregation(cmdr: Commander) = listOf(
        LookupOperation.newLookup()
            .from(FleetMember.collection)
            .localField("_id")
            .foreignField("fleet.\$id")
            .`as`("members"),
        Aggregation.match(
            Criteria().orOperator(
                Criteria().andOperator(
                    Criteria.where("members.cmdr.\$id").`is`(wrapId(cmdr.id)),
                    writableLevelsCriteria,
                ),
                Criteria.where("owner.\$id").`is`(wrapId(cmdr.id))
            )
        )
    ).toTypedArray()

    private fun paginationAggregation(page: Pageable) = listOf(
        Aggregation.facet()
            .and(
                Aggregation.count().`as`("total")
            ).`as`("total")
            .and(
                Aggregation.skip(page.offset),
                Aggregation.limit(page.pageSize.toLong())
            ).`as`("results"),
        Aggregation.unwind("total"),
        Aggregation.project()
            .and("total.total").`as`("total")
            .and("results").`as`("results")
    ).toTypedArray()
}