package tech.acheron.ocellus.service.fleet

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import tech.acheron.ocellus.data.model.Fleet
import tech.acheron.ocellus.data.model.impl.Commander

interface FleetService {
    fun create(name: String, owner: Commander, isPublic: Boolean): Fleet

    fun get(id: String): Fleet?

    fun wrap(fleet: Fleet): FleetOperations

    fun searchAllWritable(
        cmdr: Commander,
        query: String,
        limit: Int,
    ): List<Fleet>

    fun getAllWritable(
        cmdr: Commander,
        limit: Int,
    ): List<Fleet>

    fun searchAll(cmdr: Commander, query: String): List<Fleet>

    fun searchAll(cmdr: Commander, query: String, page: Pageable): Page<Fleet>

    fun getAll(cmdr: Commander): List<Fleet>

    fun getAll(cmdr: Commander, page: Pageable): Page<Fleet>
}