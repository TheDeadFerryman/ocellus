package tech.acheron.ocellus.service.fleet.impl

import tech.acheron.ocellus.data.model.impl.MongoFleet
import tech.acheron.ocellus.data.repo.FleetRepository
import tech.acheron.ocellus.service.fleet.FleetOperations

class MongoFleetOperations(
    override val fleet: MongoFleet,
    private val fleetRepository: FleetRepository,
    private val fleetOperationsFactory: MongoFleetOperationsFactory,
) : FleetOperations {

    override val access by lazy {
        fleetOperationsFactory.wrapFleetAccess(fleet)
    }

    override val ships by lazy {
        fleetOperationsFactory.wrapFleetShips(fleet)
    }

    override fun rename(newName: String) = wrap {
        saveModified(fleet) {
            name = newName
        }
    }

    override fun delete() {
        ships.getAll()
            .forEach { ships.wrap(it).delete() }
        access.getMembers()
            .forEach { access.wrap(it).delete() }
        fleetRepository.delete(fleet)
    }

    private fun saveModified(fleet: MongoFleet, action: MongoFleet.() -> Unit) =
        fleetRepository.save(fleet.apply(action))

    private fun wrap(action: () -> MongoFleet) =
        fleetOperationsFactory.wrapFleet(action())
}