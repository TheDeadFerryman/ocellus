package tech.acheron.ocellus.service.fleet

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import tech.acheron.ocellus.data.model.impl.Commander
import tech.acheron.ocellus.data.model.impl.Ship

interface FleetShipsOperations {
    fun create(
        name: String,
        author: Commander,
        model: String,
        data: ByteArray,
    ): Ship

    fun create(
        creator: ShipCreator,
    ): Ship

    fun createBulk(
        creators: List<ShipCreator>,
    ): List<Ship>

    fun get(id: String): Ship?

    fun getAll(): List<Ship>

    fun getAll(page: Pageable): Page<Ship>

    fun count(): Int

    fun findByName(query: String): List<Ship>
    fun findByName(query: String, page: Pageable): Page<Ship>

    fun wrap(ship: Ship): ShipOperations
}