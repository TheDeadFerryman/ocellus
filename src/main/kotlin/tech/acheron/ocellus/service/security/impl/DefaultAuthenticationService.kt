package tech.acheron.ocellus.service.security.impl

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import tech.acheron.ocellus.service.security.AuthenticationService
import tech.acheron.ocellus.service.security.TokenService
import tech.acheron.ocellus.service.security.TokenService.Purpose

@Service
class DefaultAuthenticationService(
    private val tokenService: TokenService,
    private val userDetailsService: UserDetailsService,
) : AuthenticationService {

    override fun issueToken(userDetails: UserDetails) =
        tokenService.issue(userDetails.username, Purpose.Auth)

    override fun getAuthentication(token: String) = try {
        tokenService.getSubject(token, Purpose.Auth)
            ?.let {
                userDetailsService.loadUserByUsername(it)
            }
            ?.let { UsernamePasswordAuthenticationToken(it, token, it.authorities) }
    } catch (e: UsernameNotFoundException) {
        null
    }
}