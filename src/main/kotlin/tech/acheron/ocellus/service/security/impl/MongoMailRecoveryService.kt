package tech.acheron.ocellus.service.security.impl

import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Service
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import tech.acheron.ocellus.config.properties.MailingProperties
import tech.acheron.ocellus.config.properties.MetaProperties
import tech.acheron.ocellus.data.model.impl.Commander
import tech.acheron.ocellus.data.model.impl.Recovery
import tech.acheron.ocellus.data.repo.RecoveryRepository
import tech.acheron.ocellus.service.mail.MailService
import tech.acheron.ocellus.service.security.RecoveryService
import tech.acheron.ocellus.service.security.TokenService
import tech.acheron.ocellus.service.security.TokenService.Purpose
import tech.acheron.ocellus.util.wrapId

@Service
class MongoMailRecoveryService(
    private val recoveryRepository: RecoveryRepository,
    private val tokenService: TokenService,
    private val mailService: MailService,
    private val templateEngine: TemplateEngine,
    private val mailingProperties: MailingProperties,
    private val metaProperties: MetaProperties,
) : RecoveryService {
    override fun issue(cmdr: Commander): Recovery =
        recoveryRepository.save(Recovery(cmdr))

    override fun commit(recovery: Recovery) =
        tokenService.issue(recovery.id, Purpose.Recovery).also { token ->
            mailService.send {
                MimeMessageHelper(it).apply {
                    setFrom(mailingProperties.recoverySender)
                    setTo(recovery.cmdr.email)
                    setSubject("Account recovery")
                    recoveryLink(token)
                        .let(::buildEmailContent)
                        .also { text ->
                            setText(text, true)
                        }
                }
            }
        }

    override fun resolve(token: String) =
        tokenService.getSubject(token, Purpose.Recovery)
            ?.let { recoveryRepository.findById(wrapId(it)) }
            ?.orElse(null)

    override fun consume(recovery: Recovery) =
        recoveryRepository.delete(recovery)

    private fun recoveryLink(token: String) =
        "${metaProperties.baseUrl}/recover/commit#token=$token"

    private fun buildEmailContent(link: String) =
        Context()
            .also {
                it.setVariable("link", link)
            }
            .let {
                templateEngine.process("mail/recovery", it)
            }
}