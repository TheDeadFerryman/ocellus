package tech.acheron.ocellus.service.security.impl

import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import tech.acheron.ocellus.data.repo.CommanderRepository

@Service
class MongoUserDetailsService(
    private val commanderRepository: CommanderRepository,
) : UserDetailsService {
    override fun loadUserByUsername(username: String?) =
        username?.let {
            commanderRepository.findByName(it)
        } ?: throw UsernameNotFoundException("no CMDR '$username'")
}