package tech.acheron.ocellus.service.security

interface TokenService {
    enum class Purpose(
        val value: String,
    ) {
        Auth("a"),
        Recovery("r"),
    }

    fun issue(subject: String, purpose: Purpose): String

    fun getSubject(token: String, purpose: Purpose): String?
}