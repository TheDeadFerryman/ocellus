package tech.acheron.ocellus.service.security

import tech.acheron.ocellus.data.model.impl.Commander
import tech.acheron.ocellus.data.model.impl.Recovery

interface RecoveryService {
    fun issue(cmdr: Commander): Recovery

    fun commit(recovery: Recovery): String?

    fun resolve(token: String): Recovery?

    fun consume(recovery: Recovery)
}