package tech.acheron.ocellus.service.security

import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetails

interface AuthenticationService {
    fun issueToken(userDetails: UserDetails): String
    fun getAuthentication(token: String): Authentication?
}