package tech.acheron.ocellus.service.security.impl

import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import org.springframework.stereotype.Service
import tech.acheron.ocellus.config.properties.SecurityProperties
import tech.acheron.ocellus.service.security.TokenService
import tech.acheron.ocellus.service.security.TokenService.Purpose
import java.time.Duration
import java.time.Instant
import java.util.*

@Service
class JWTokenService(
    private val securityProperties: SecurityProperties,
) : TokenService {
    companion object {
        private const val purposeClaim = "prp"
    }

    private val signingKey by lazy {
        Keys.hmacShaKeyFor(securityProperties.signingKey.toByteArray())
    }

    private fun createExpiration() = Date.from(
        Instant.now() + Duration.ofSeconds(securityProperties.tokenLifetime)
    )


    override fun issue(subject: String, purpose: Purpose) =
        builderBase(purpose)
            .setSubject(subject)
            .compact()!!

    override fun getSubject(token: String, purpose: Purpose) = catchTokenErrors {
        parserBase(purpose)
            .parseClaimsJws(token)
            ?.body?.subject
    }

    private fun parserBase(purpose: Purpose) =
        Jwts.parserBuilder()
            .setSigningKey(signingKey)
            .requireIssuer(securityProperties.tokenIssuer)
            .require(purposeClaim, purpose.value)
            .build()

    private fun builderBase(purpose: Purpose) =
        Jwts.builder()
            .setExpiration(createExpiration())
            .setIssuedAt(Date.from(Instant.now()))
            .setIssuer(securityProperties.tokenIssuer)
            .addClaims(mapOf(purposeClaim to purpose.value))
            .signWith(signingKey)


    private fun <T> catchTokenErrors(action: () -> T) =
        try {
            action()
        } catch (e: JwtException) {
            null
        } catch (e: IllegalArgumentException) {
            null
        }

}