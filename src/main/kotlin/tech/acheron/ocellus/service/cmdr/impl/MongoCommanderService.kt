package tech.acheron.ocellus.service.cmdr.impl

import org.springframework.data.domain.Pageable
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import tech.acheron.ocellus.config.properties.SecurityProperties
import tech.acheron.ocellus.data.model.impl.Commander
import tech.acheron.ocellus.data.repo.CommanderRepository
import tech.acheron.ocellus.service.cmdr.CommanderService
import tech.acheron.ocellus.util.wrapId

@Service
class MongoCommanderService(
    private val cmdrRepository: CommanderRepository,
    private val passwordEncoder: PasswordEncoder,
    private val securityProperties: SecurityProperties,
) : CommanderService {

    override fun get(name: String) =
        cmdrRepository.findByName(name)

    override fun get(name: String, email: String) =
        cmdrRepository.findByNameAndEmail(name, email)

    override fun search(query: String, limit: Int): List<Commander> =
        cmdrRepository.searchNameMatches(
            "^${Regex.escape(query)}",
            Pageable.ofSize(limit)
        ).content

    override fun search(query: String, excludeIds: List<String>, limit: Int): List<Commander> =
        cmdrRepository.searchNameMatchesExcludingIds(
            "^${Regex.escape(query)}",
            safeMapObjectIds(excludeIds),
            Pageable.ofSize(limit)
        ).content

    override fun create(email: String, name: String, password: String): Commander {
        require(password.length >= securityProperties.minPassword) {
            "Password must be at least ${securityProperties.minPassword} characters"
        }
        require(name.isNotEmpty()) {
            "Name must be present"
        }
        require(email.isNotEmpty()) {
            "Email must be present"
        }
        require(!cmdrRepository.existsByEmail(email)) {
            "CMDR exists with this email!"
        }
        require(!cmdrRepository.existsByName(name)) {
            "CMDR exists with this name!"
        }

        return cmdrRepository.save(
            Commander(
                name = name,
                email = email,
                encodedPassword = passwordEncoder.encode(password),
            )
        )
    }

    override fun getIfValid(name: String, password: String) =
        cmdrRepository.findByName(name)?.let {
            if (validatePassword(it, password)) {
                it
            } else {
                null
            }
        }

    override fun validatePassword(cmdr: Commander, password: String) =
        passwordEncoder.matches(password, cmdr.encodedPassword)

    override fun setPassword(cmdr: Commander, password: String): Commander {
        require(password.length >= securityProperties.minPassword) {
            "Password must be at least ${securityProperties.minPassword} characters"
        }

        return cmdrRepository.save(cmdr.apply {
            encodedPassword = passwordEncoder.encode(password)
        })
    }

    private fun safeMapObjectIds(ids: List<String>) = ids.mapNotNull {
        try {
            wrapId(it)
        } catch (e: IllegalArgumentException) {
            null
        }
    }
}