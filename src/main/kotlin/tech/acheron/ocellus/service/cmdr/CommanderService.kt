package tech.acheron.ocellus.service.cmdr

import tech.acheron.ocellus.data.model.impl.Commander

interface CommanderService {
    fun create(email: String, name: String, password: String): Commander

    fun search(query: String, limit: Int): List<Commander>

    fun search(query: String, excludeIds: List<String>, limit: Int): List<Commander>

    fun get(name: String): Commander?

    fun get(name: String, email: String): Commander?

    fun getIfValid(name: String, password: String): Commander?

    fun validatePassword(cmdr: Commander, password: String): Boolean

    fun setPassword(cmdr: Commander, password: String): Commander
}