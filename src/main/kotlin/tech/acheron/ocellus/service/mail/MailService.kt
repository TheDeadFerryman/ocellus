package tech.acheron.ocellus.service.mail

import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.MimeMessagePreparator

interface MailService {
    fun send(message: SimpleMailMessage)

    fun send(message: MimeMessagePreparator)

    fun sendSimple(from: String, to: String, subject: String, text: String)
}