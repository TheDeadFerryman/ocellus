package tech.acheron.ocellus.service.mail.impl

import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessagePreparator
import org.springframework.stereotype.Service
import tech.acheron.ocellus.service.mail.MailService

@Suppress("UNUSED_EXPRESSION")
@Service
class DefaultMailService(
    private val mailSender: JavaMailSender,
) : MailService {
    override fun send(message: SimpleMailMessage) =
        mailSender.send(message)

    override fun send(message: MimeMessagePreparator) =
        mailSender.send(message)

    override fun sendSimple(from: String, to: String, subject: String, text: String) =
        send(SimpleMailMessage().apply {
            setFrom(from)
            setTo(to)
            setSubject(subject)
            setText(text)
        })
}