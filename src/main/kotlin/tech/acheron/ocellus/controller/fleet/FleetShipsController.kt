package tech.acheron.ocellus.controller.fleet

import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.*
import tech.acheron.ocellus.annotation.security.Authorized
import tech.acheron.ocellus.data.model.impl.AccessLevel
import tech.acheron.ocellus.dto.common.PageDTO
import tech.acheron.ocellus.dto.ship.*

@Authorized
@RestController
@RequestMapping("/api/fleet/{fleetId}/ships")
class FleetShipsController(
    private val fleetOperationsHelper: FleetOperationsHelper,
) {
    companion object {
        const val pageSize = 20
    }

    @GetMapping
    fun getAll(@PathVariable("fleetId") fleetId: String) =
        fleetOperationsHelper
            .requiresAccess(fleetId, AccessLevel.Read) { fleet ->
                fleet.ships.getAll()
                    .map { fleet.ships.wrap(it) }
                    .map { ShipDTO.of(it) }
            }

    @GetMapping("search")
    fun search(
        @PathVariable fleetId: String,
        q: String?,
    ) = fleetOperationsHelper.requiresAccess(fleetId, AccessLevel.Read) { fleet, cmdr ->
        if (q.isNullOrBlank()) {
            fleet.ships.getAll()
        } else {
            fleet.ships.findByName(q)
        }.map {
            ShipWithAccessLevelDTO(
                ship = ShipDTO.of(fleet.ships.wrap(it)),
                level = fleet.access.getShipAccessLevel(cmdr, it),
            )
        }
    }

    @GetMapping("search/page")
    fun searchPage(
        @PathVariable fleetId: String,
        q: String?,
        page: Int?,
        pageSize: Int?,
    ) = fleetOperationsHelper.requiresAccess(fleetId, AccessLevel.Read) { fleet, cmdr ->
        PageRequest.of(
            page ?: 0,
            pageSize ?: FleetShipsController.pageSize
        ).let { page ->
            if (q.isNullOrBlank()) {
                fleet.ships.getAll(page)
            } else {
                fleet.ships.findByName(q, page)
            }.map {
                ShipWithAccessLevelDTO(
                    ship = ShipDTO.of(fleet.ships.wrap(it)),
                    level = fleet.access.getShipAccessLevel(cmdr, it),
                )
            }.let { PageDTO.of(it) }
        }
    }

    @GetMapping("page")
    fun getAllPaged(
        @PathVariable("fleetId") fleetId: String,
        page: Int?,
        pageSize: Int?,
    ) =
        fleetOperationsHelper
            .requiresAccess(fleetId, AccessLevel.Read) { fleet ->
                fleet.ships.getAll(
                    PageRequest.of(
                        page ?: 0,
                        pageSize ?: FleetShipsController.pageSize
                    )
                )
                    .map { fleet.ships.wrap(it) }
                    .map(ShipDTO::of)
                    .let { PageDTO.of(it) }
            }

    @GetMapping("{id}")
    fun get(
        @PathVariable("fleetId") fleetId: String,
        @PathVariable("id") shipId: String,
    ) =
        fleetOperationsHelper
            .requiresShipAccess(fleetId, shipId, AccessLevel.Read) { ship, fleet, cmdr ->
                ShipWithAccessLevelDTO(
                    ship = ShipDTO.of(ship),
                    level = fleet.access.getShipAccessLevel(cmdr, ship.ship),
                )
            }

    @PostMapping("create")
    fun create(
        @PathVariable("fleetId") fleetId: String,
        @RequestBody data: ShipCreateDTO,
    ) =
        fleetOperationsHelper
            .requiresAccess(fleetId, AccessLevel.Add) { fleet, cmdr ->
                fleet.ships.create(
                    data.toCreator(cmdr)
                ).let { ShipDTO.of(fleet.ships.wrap(it)) }
            }

    @PostMapping("createBulk")
    fun createBulk(
        @PathVariable("fleetId") fleetId: String,
        @RequestBody data: List<ShipCreateDTO>,
    ) =
        fleetOperationsHelper
            .requiresAccess(fleetId, AccessLevel.Add) { fleet, cmdr ->
                fleet.ships.createBulk(
                    data.map { it.toCreator(cmdr) }
                ).map { ShipDTO.of(fleet.ships.wrap(it)) }
            }

    @PostMapping("{id}/update")
    fun update(
        @PathVariable("fleetId") fleetId: String,
        @PathVariable("id") shipId: String,
        @RequestBody data: ShipUpdateDTO,
    ) =
        fleetOperationsHelper
            .requiresShipAccess(
                fleetId, shipId, AccessLevel.Edit,
            ) { ship ->
                ship.update(data.name, data.model, data.dataRaw)
                    .let(ShipDTO::of)
            }

    @PostMapping("{id}/rename")
    fun rename(
        @PathVariable("fleetId") fleetId: String,
        @PathVariable("id") shipId: String,
        @RequestBody data: ShipRenameDTO,
    ) =
        fleetOperationsHelper
            .requiresShipAccess(
                fleetId, shipId, AccessLevel.Edit,
            ) { ship ->
                ship.rename(data.name).let(ShipDTO::of)
            }

    @PostMapping("{id}/copy")
    fun copy(
        @PathVariable("fleetId") fleetId: String,
        @PathVariable("id") shipId: String,
        @RequestBody data: ShipCopyDTO,
    ) =
        fleetOperationsHelper
            .requiresShipAccess(
                fleetId, shipId, AccessLevel.Read,
            ) { ship ->
                fleetOperationsHelper.requiresAccess(data.toFleet, AccessLevel.Add) { toFleet, cmdr ->
                    ship.copy(toFleet.ships, cmdr).let(ShipDTO::of)
                }
            }

    @PostMapping("{id}/drop")
    fun drop(
        @PathVariable("fleetId") fleetId: String,
        @PathVariable("id") shipId: String,
    ) =
        fleetOperationsHelper
            .requiresShipAccess(
                fleetId, shipId, AccessLevel.Edit,
            ) { ship ->
                ship.delete().let { true }
            }
}