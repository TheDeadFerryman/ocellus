package tech.acheron.ocellus.controller.fleet

import org.springframework.stereotype.Service
import tech.acheron.ocellus.data.model.impl.AccessLevel
import tech.acheron.ocellus.data.model.impl.Commander
import tech.acheron.ocellus.exception.NotFoundException
import tech.acheron.ocellus.service.fleet.FleetOperations
import tech.acheron.ocellus.service.fleet.ShipOperations
import tech.acheron.ocellus.service.fleet.impl.MongoFleetService
import tech.acheron.ocellus.util.AuthUtils

@Service
class FleetOperationsHelper(
    private val fleetService: MongoFleetService,
) {
    fun <T> requiresAccess(
        fleetId: String,
        level: AccessLevel,
        action: (fleet: FleetOperations) -> T,
    ) = requiresAccess(fleetId, level) { fleet, _ ->
        action(fleet)
    }

    fun <T> requiresAccess(
        fleetId: String,
        level: AccessLevel,
        action: (fleet: FleetOperations, cmdr: Commander) -> T,
    ) = AuthUtils.withCurrentUser { cmdr ->
        val fleet = fleetService.get(fleetId)
            ?: throw NotFoundException("No such fleet!")

        val fleetOperations = fleetService.wrap(fleet)

        require(fleetOperations.access.hasAccessLevel(cmdr, level)) {
            "Access denied for fleet!"
        }

        action(fleetOperations, cmdr)
    }

    fun <T> requiresShipAccess(
        fleetId: String,
        shipId: String,
        level: AccessLevel,
        action: (ship: ShipOperations) -> T,
    ) = requiresShipAccess(fleetId, shipId, level) { ship, _ ->
        action(ship)
    }

    fun <T> requiresShipAccess(
        fleetId: String,
        shipId: String,
        level: AccessLevel,
        action: (ship: ShipOperations, fleet: FleetOperations) -> T,
    ) = requiresShipAccess(fleetId, shipId, level) { ship, fleet, _ ->
        action(ship, fleet)
    }

    fun <T> requiresShipAccess(
        fleetId: String,
        shipId: String,
        level: AccessLevel,
        action: (ship: ShipOperations, fleet: FleetOperations, cmdr: Commander) -> T,
    ) = AuthUtils.withCurrentUser { cmdr ->
        val fleet = fleetService.get(fleetId)
            ?: throw NotFoundException("No such fleet!")

        val fleetOperations = fleetService.wrap(fleet)

        val ship = fleetOperations.ships.get(shipId)
            ?: throw NotFoundException("No such ship!")

        require(fleetOperations.access.hasShipAccessLevel(cmdr, ship, level)) {
            "Access denied for ship!"
        }

        action(fleetOperations.ships.wrap(ship), fleetOperations, cmdr)
    }
}