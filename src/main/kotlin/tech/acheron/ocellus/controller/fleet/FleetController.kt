package tech.acheron.ocellus.controller.fleet

import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.*
import tech.acheron.ocellus.annotation.security.Authorized
import tech.acheron.ocellus.data.model.impl.AccessLevel
import tech.acheron.ocellus.dto.common.PageDTO
import tech.acheron.ocellus.dto.fleet.FleetDTO
import tech.acheron.ocellus.dto.fleet.FleetInputDTO
import tech.acheron.ocellus.dto.fleet.FleetWithAccessLevelDTO
import tech.acheron.ocellus.service.fleet.FleetService
import tech.acheron.ocellus.util.AuthUtils

@RestController
@Authorized
@RequestMapping("/api/fleet")
class FleetController(
    private val fleetService: FleetService,
    private val fleetOperationsHelper: FleetOperationsHelper,
) {
    companion object {
        const val defaultPageSize = 20
        const val defaultLimit = 10
    }

    @PostMapping("create")
    fun create(@RequestBody fleet: FleetInputDTO) =
        AuthUtils.withCurrentUser { cmdr ->
            fleetService.create(
                name = fleet.name,
                owner = cmdr,
                isPublic = fleet.isPublic,
            ).let {
                FleetDTO.of(
                    fleetService.wrap(it)
                )
            }
        }

    @GetMapping("all/page")
    fun getPaged(page: Int?, pageSize: Int?) =
        AuthUtils.withCurrentUser { cmdr ->
            fleetService.getAll(
                cmdr = cmdr,
                page = PageRequest.of(
                    page ?: 0,
                    pageSize ?: defaultPageSize,
                )
            )
                .map { FleetDTO.of(fleetService.wrap(it)) }
                .let { PageDTO.of(it) }
        }

    @GetMapping("search/page")
    fun searchPage(q: String?, page: Int?, pageSize: Int?) =
        if (q.isNullOrBlank()) {
            getPaged(page, pageSize)
        } else {
            AuthUtils.withCurrentUser { cmdr ->
                fleetService.searchAll(
                    cmdr = cmdr,
                    query = q,
                    page = PageRequest.of(
                        page ?: 0,
                        pageSize ?: defaultPageSize,
                    )
                )
                    .map { FleetDTO.of(fleetService.wrap(it)) }
                    .let { PageDTO.of(it) }

            }
        }

    @GetMapping("searchWritable")
    fun searchWritable(q: String?, limit: Int?) =
        AuthUtils.withCurrentUser { cmdr ->
            if (q.isNullOrBlank()) {
                fleetService.getAllWritable(
                    cmdr, limit ?: defaultLimit
                )
            } else {
                fleetService.searchAllWritable(
                    cmdr, q, limit ?: defaultLimit
                )
            }.map { FleetDTO.of(fleetService.wrap(it)) }
        }

    @GetMapping("all")
    fun getAll() =
        AuthUtils.withCurrentUser { cmdr ->
            fleetService.getAll(cmdr)
                .map { FleetDTO.of(fleetService.wrap(it)) }
        }

    @GetMapping("{id}")
    fun get(@PathVariable("id") id: String) =
        fleetOperationsHelper.requiresAccess(id, AccessLevel.Read) { fleet ->
            FleetDTO.of(fleet)
        }

    @GetMapping("{id}/withAccessLevel")
    fun getWithAccessLevel(@PathVariable("id") id: String) =
        fleetOperationsHelper.requiresAccess(id, AccessLevel.Read) { fleet, cmdr ->
            FleetWithAccessLevelDTO(
                fleet = FleetDTO.of(fleet),
                level = fleet.access.getAccessLevel(cmdr)
            )
        }


    @PostMapping("{id}/update")
    fun update(
        @PathVariable("id") fleetId: String,
        @RequestBody data: FleetInputDTO,
    ) =
        fleetOperationsHelper.requiresAccess(fleetId, AccessLevel.Admin) { fleet ->
            fleet.rename(data.name)
            fleet.access.setPublic(data.isPublic)

            FleetDTO.of(fleet)
        }

    @PostMapping("{id}/drop")
    fun drop(@PathVariable("id") fleetId: String) =
        fleetOperationsHelper.requiresAccess(fleetId, AccessLevel.Admin) { fleet ->
            fleet.delete()
        }
}