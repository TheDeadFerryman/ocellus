package tech.acheron.ocellus.controller.fleet

import org.springframework.web.bind.annotation.*
import tech.acheron.ocellus.annotation.security.Authorized
import tech.acheron.ocellus.data.model.impl.AccessLevel
import tech.acheron.ocellus.data.model.impl.Commander
import tech.acheron.ocellus.dto.fleet.FleetDTO
import tech.acheron.ocellus.dto.fleet.member.FleetMemberDTO
import tech.acheron.ocellus.dto.fleet.member.FleetMemberInputDTO
import tech.acheron.ocellus.dto.fleet.member.FleetMemberKickDTO
import tech.acheron.ocellus.exception.NotFoundException
import tech.acheron.ocellus.service.cmdr.CommanderService
import tech.acheron.ocellus.service.fleet.FleetOperations

@Authorized
@RestController
@RequestMapping("/api/fleet/{id}/members")
class FleetMembersController(
    private val fleetOperationsHelper: FleetOperationsHelper,
    private val cmdrService: CommanderService,
) {

    @PostMapping("add")
    fun addMember(
        @PathVariable("id") fleetId: String,
        @RequestBody data: FleetMemberInputDTO,
    ) =
        fleetMemberOperation(fleetId, data.cmdr) { fleet, member ->
            fleet.access.addMember(member, data.level)
                .let(FleetMemberDTO::of)
        }

    @GetMapping
    fun getMembers(@PathVariable("id") fleetId: String) =
        fleetOperationsHelper.requiresAccess(fleetId, AccessLevel.Read) { fleet ->
            fleet.access.getMembers().map(FleetMemberDTO::of)
        }

    @PostMapping("edit")
    fun editMember(
        @PathVariable("id") fleetId: String,
        @RequestBody data: FleetMemberInputDTO,
    ) =
        fleetMemberOperation(fleetId, data.cmdr) { fleet, member ->
            fleet.access.getMember(member)?.let {
                fleet.access.wrap(it)
                    .setLevel(data.level)
                    .member.let(FleetMemberDTO::of)
            } ?: throw NotFoundException("No such member!")
        }

    @PostMapping("kick")
    fun kickMember(
        @PathVariable("id") fleetId: String,
        @RequestBody data: FleetMemberKickDTO,
    ) =
        fleetMemberOperation(fleetId, data.cmdr) { fleet, member ->
            fleet.access.getMember(member)
                ?.let { fleet.access.wrap(it) }
                ?.delete()

            FleetDTO.of(fleet)
        }


    private fun <T> fleetMemberOperation(
        fleetId: String, member: String,
        action: (fleet: FleetOperations, member: Commander) -> T?,
    ) = fleetOperationsHelper.requiresAccess(fleetId, AccessLevel.Admin) { fleet ->
        val memberCmdr = cmdrService.get(member)
            ?: throw NotFoundException("No such CMDR!")

        action(fleet, memberCmdr)
    }
}