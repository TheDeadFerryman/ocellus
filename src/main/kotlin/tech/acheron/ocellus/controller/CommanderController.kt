package tech.acheron.ocellus.controller


import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.web.bind.annotation.*
import tech.acheron.ocellus.annotation.security.Authorized
import tech.acheron.ocellus.dto.cmdr.*
import tech.acheron.ocellus.exception.NotFoundException
import tech.acheron.ocellus.service.cmdr.CommanderService
import tech.acheron.ocellus.service.security.AuthenticationService
import tech.acheron.ocellus.service.security.RecoveryService
import tech.acheron.ocellus.util.AuthUtils

@RestController
@RequestMapping("/api/cmdr")
class CommanderController(
    private val cmdrService: CommanderService,
    private val authService: AuthenticationService,
    private val recoveryService: RecoveryService,
) {
    companion object {
        const val cmdrSearchLimit = 8
        const val cmdrQueryThreshold = 3
    }

    @PostMapping("register")
    fun register(@RequestBody cmdr: CommanderRegisterDTO) =
        cmdrService.create(
            name = cmdr.name,
            email = cmdr.email,
            password = cmdr.password,
        )
            .let { authService.issueToken(it) }
            .let { TokenDTO(it) }

    @PostMapping("login")
    fun login(@RequestBody credentials: LoginDTO, response: HttpServletResponse) =
        cmdrService.getIfValid(credentials.name, credentials.password)
            ?.let { authService.issueToken(it) }
            .let { TokenDTO(it) }

    @Authorized
    @GetMapping("me")
    fun me() =
        AuthUtils.withCurrentUser(CommanderDTO::of)
            ?: throw NotFoundException()

    @Authorized
    @PostMapping("search")
    fun search(@RequestBody data: CommanderQueryDTO) = if (data.query.length >= cmdrQueryThreshold) {
        if (data.excludeIds == null) {
            cmdrService.search(
                data.query,
                data.limit ?: cmdrSearchLimit,
            )
        } else {
            cmdrService.search(
                data.query,
                data.excludeIds,
                data.limit ?: cmdrSearchLimit,
            )
        }.map(CommanderDTO::of)
    } else {
        listOf()
    }

    @Authorized
    @PostMapping("password")
    fun changePassword(@RequestBody change: ChangePasswordDTO) =
        AuthUtils.withCurrentUser {
            require(cmdrService.validatePassword(it, change.current)) {
                "Invalid password!"
            }

            cmdrService.setPassword(it, change.new)
        }

    @PostMapping("recover")
    fun recover(@RequestBody recover: RecoveryRequestDTO, request: HttpServletRequest) =
        cmdrService.get(recover.name, recover.email).let { cmdr ->
            requireNotNull(cmdr) {
                "Invalid credentials!"
            }

            recoveryService.issue(cmdr).let {
                recoveryService.commit(it) != null
            }
        }

    @GetMapping("recover/info")
    fun recoverInfo(token: String) =
        recoveryService.resolve(token)
            ?.let { CommanderDTO.of(it.cmdr) }
            ?: throw NotFoundException("Recovery request not found!")

    @PostMapping("recover/commit")
    fun recoverCommit(@RequestBody recover: RecoveryDTO) =
        recoveryService.resolve(recover.token)?.let { recovery ->
            cmdrService.setPassword(recovery.cmdr, recover.password)
                .let { CommanderDTO.of(it) }
                .also { recoveryService.consume(recovery) }
        } ?: throw NotFoundException("Recovery request not found!")
}