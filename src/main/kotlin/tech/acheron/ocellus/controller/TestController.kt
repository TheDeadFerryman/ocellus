package tech.acheron.ocellus.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import tech.acheron.ocellus.data.repo.CommanderRepository
import tech.acheron.ocellus.service.fleet.impl.MongoFleetService

@RestController
class TestController(
    private val fleetService: MongoFleetService,
    private val cmdrRepository: CommanderRepository,
) {
    @GetMapping("/api/test")
    fun spamFleets() {
        val owr = cmdrRepository.findByName("The Dead Ferryman")!!

        repeat(200) {
            fleetService.create(
                name = "Test Fleet $it",
                owner = owr,
                isPublic = false,
            )
        }
    }
}