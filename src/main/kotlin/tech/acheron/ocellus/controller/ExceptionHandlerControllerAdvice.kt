package tech.acheron.ocellus.controller

import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import tech.acheron.ocellus.exception.InvalidRequestException

@RestControllerAdvice
class ExceptionHandlerControllerAdvice : ResponseEntityExceptionHandler() {
    @ExceptionHandler(IllegalArgumentException::class)
    fun handleIllegalArgument(ex: IllegalArgumentException, request: WebRequest) =
        super.handleException(InvalidRequestException(ex.message), request)
}