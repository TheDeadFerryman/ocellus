package tech.acheron.ocellus.exception

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

class NotFoundException(
    message: String? = null,
) : ResponseStatusException(HttpStatus.NOT_FOUND, message)

class InvalidRequestException(
    message: String? = null,
) : ResponseStatusException(HttpStatus.BAD_REQUEST, message)