#syntax=docker/dockerfile:1.4
# Run this from within this directory. Change the location of coriolis-data repo and image name/tag as needed.
# docker buildx build --build-context data=../coriolis-data --tag coriolis .

FROM eclipse-temurin:17.0.7_7-jre AS build-all

RUN mkdir -p /tmp/build/frontend
WORKDIR /tmp/build

RUN curl -sL https://deb.nodesource.com/setup_16.x | bash

RUN apt-get update && \
    apt-get -y install git && \
    apt-get -y install build-essential libcairo2-dev libpango1.0-dev libjpeg-dev libgif-dev librsvg2-dev && \
    apt-get -y install nodejs && \
    node -v

RUN npm install -g yarn

ADD gradle/ ./gradle/

ADD *.gradle.kts ./
ADD gradlew .

RUN chmod +x gradlew

ADD src/ ./src/

RUN ./gradlew bootJar --no-daemon

ADD frontend/package.json ./frontend/
ADD frontend/yarn.lock ./frontend/

RUN cd frontend && yarn install

ADD frontend/ ./frontend/

ADD misc/build-with-server.sh .

ARG OCELLUS_SERVER_URL

RUN OCELLUS_SERVER_URL=$OCELLUS_SERVER_URL bash build-with-server.sh

RUN ./gradlew properties | grep ^version | awk -F: '{ print $2; }' | xargs > app.version

RUN mv -v build/libs/ocellus-$(cat app.version).jar ./ocellus.jar

FROM nginx:alpine3.17 AS frontend

WORKDIR /usr/share/nginx/html

RUN rm -rf *

ADD misc/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build-all /tmp/build/frontend/build/* ./

FROM eclipse-temurin:17.0.7_7-jre AS backend

RUN mkdir -p /opt/ocellus
WORKDIR /opt/ocellus

RUN mkdir -p config

COPY --from=build-all /tmp/build/ocellus.jar ./

CMD java -jar ocellus.jar - spring.config.location=$(pwd)/config