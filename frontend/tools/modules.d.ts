declare module 'to-ico' {
  import { Buffer } from 'buffer';
  const toIco: (data: Buffer[]) => Promise<Buffer>;

  export default toIco;
}

declare module 'svgdom' {
  export const createSVGWindow: () => Window;
}
