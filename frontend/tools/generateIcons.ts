import config from '../config/icons.json';
import { get, isArray, isFunction, isInteger, isNil, template, toNumber, isNumber, isString } from 'lodash';
import { registerWindow, SVG } from '@svgdotjs/svg.js';
import * as console from 'console';
import { createCanvas, loadImage } from 'canvas';
import { Buffer } from 'buffer';
import toIco from 'to-ico';
import * as fs from 'fs/promises';
import { JSDOM } from 'jsdom';

const { window } = new JSDOM();
const { document } = window;

registerWindow(window as unknown as Window, window.document);

const getSVGFromString = (svg: string) => {
  const container = document.createElement('div');
  container.innerHTML = svg;

  const elem = container.getElementsByTagName('svg').item(0);

  if (isNil(elem)) {
    throw new Error('failed to parse SVG!');
  }

  return SVG(elem.outerHTML);
};

type ResizeString = string;

type TransformToolFunc<Config> =
  (svg: string, config: Config) => string;

type TransformTool<Id extends string, Config> =
  TransformToolFunc<Config>;

const isValidNumber = (v: number): boolean => (
  !isNaN(v) && v !== Infinity
);

const transformTool = <Id extends string, Arg>(id: Id, tool: TransformToolFunc<Arg>): TransformTool<Id, Arg> => tool;

const unpackResizeString = (str: ResizeString) => {
  const parsed = str.split('x', 2)
    .map(el => toNumber(el))
    .filter(isInteger);

  if (parsed.length !== 2) {
    if (parsed.length === 1) {
      return [parsed[0], parsed[0]];
    } else {
      throw new Error('invalid resize string!');
    }
  }

  return parsed;
};

const resize = transformTool(
  'resize', (svg: string, size: ResizeString) => {
    const element = getSVGFromString(svg);

    const [width, height] = unpackResizeString(size);


    return element.size(width, height).svg();
  },
);

const registeredTransformTools = { resize } as const;

type GetToolId<T> =
  T extends TransformTool<infer Id, any>
    ? Id
    : never;

type ObjectValues<O> = O[keyof O];

type TransformToolIn = ObjectValues<typeof registeredTransformTools>;
type TransformToolId = GetToolId<TransformToolIn>;

type GetToolConfig<Id extends string> =
  TransformToolIn extends TransformTool<Id, infer Config>
    ? Config
    : never;

type Transform = {
  [id in TransformToolId]?: GetToolConfig<id> | GetToolConfig<id>[];
};

type StrictTransform = {
  [id in TransformToolId]?: GetToolConfig<id>;
};

const normalizeTransform = (transform: Transform) => {
  const usedTools = Object.keys(transform) as TransformToolId[];

  let batchSize: number | undefined = undefined;

  for (const tool of usedTools) {
    const toolConfig = transform[tool];

    if (isArray(toolConfig)) {
      if (isNil(batchSize)) {
        batchSize = toolConfig.length;
      } else if (batchSize !== toolConfig.length) {
        throw new Error('invalid config: batch sizes don\'t match for tools!');
      }
    }
  }

  if (isNil(batchSize)) {
    batchSize = 1;
  }

  const transforms: StrictTransform[] = [];

  for (let i = 0; i < batchSize; i++) {
    const strictXform: StrictTransform = {};

    for (const tool of usedTools) {
      const config = transform[tool];
      strictXform[tool] = isArray(config) ? config[i] : config;
    }

    transforms.push(strictXform);
  }

  return transforms;
};

const applyTransform = (initial: string, transform: StrictTransform) => {
  let current = initial;

  for (const key of Object.keys(transform) as TransformToolId[]) {
    const config = transform[key];
    if (!isNil(config)) {
      current = registeredTransformTools[key](current, config);
    }
  }

  return current;
};

const parseNumber = (num: unknown): number => {
  if (isNumber(num)) {
    return num;
  }

  if (isString(num)) {
    return toNumber(num.replace(/[^0-9]/, ''));
  }

  return NaN;
};

const getSvgSize = (svg: string) => {
  const svgObj = getSVGFromString(svg);

  const width = parseNumber(svgObj.width());
  const height = parseNumber(svgObj.height());

  if (isValidNumber(width) && isValidNumber(height)) {
    return [width, height];
  }

  const viewBox = String(svgObj.attr('viewbox'))
    .split(' ', 4)
    .map(toNumber)
    .filter(isValidNumber)
  ;

  if (viewBox.length !== 4) {
    throw new Error('Cannot get SVG size!');
  }

  return [
    viewBox[2] - viewBox[0],
    viewBox[3] - viewBox[1],
  ];
};

const svgToPng = async (svg: string): Promise<Buffer> => {
  const [width, height] = getSvgSize(svg);
  const svgImage = await loadImage(new Buffer(svg, 'utf-8'));
  const canvas = createCanvas(width, height);
  canvas.getContext('2d').drawImage(svgImage, 0, 0);

  return canvas.toBuffer();
};

const svgToIco = async (svg: string): Promise<Buffer> => {
  const png = await svgToPng(svg);

  return toIco([png]);
};

const supportedFormats = {
  png: svgToPng,
  ico: svgToIco,
} as const;

type Format = keyof typeof supportedFormats;

interface Task {
  source: string;
  output: string;
  format: string;
  transform: Transform;
}

const isSupportedFormat = (format: string): format is Format =>
  isFunction(get(supportedFormats, format));

const executeTask = async ({ source, format, transform, output }: Task) => {
  const svg = await fs.readFile(source, { encoding: 'utf-8' });

  const outTemplate = template(output);

  if (!isSupportedFormat(format)) {
    throw new Error(`'${format}' is not a supported format!`);
  }

  const formatWriter = supportedFormats[format];

  const bulk = normalizeTransform(transform).map(async transform => {
    const transformed = applyTransform(svg, transform);
    const outPath = outTemplate(transform);

    return fs.writeFile(outPath, await formatWriter(transformed));
  });

  await Promise.all(bulk);
};

const executeTasks = async (tasks: Task[]) => Promise.all(
  tasks.map(executeTask),
);

const main = async () => {
  await executeTasks(config);
};

main().catch(e => console.error(e));
