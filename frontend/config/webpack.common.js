const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const pkgJson = require('../package.json');
const buildDate = new Date();

module.exports = {
  entry: {
    main: './src/entrypoint.js',
  },
  resolve: {
    // When requiring, you don't need to add these extensions
    extensions: ['.js', '.jsx', '.json', '.less', '.ts', '.tsx'],
    alias: {
      'app': path.resolve('src/app'),
      'kit': path.resolve('src/kit'),
      'less': path.resolve('src/less'),
      'images': path.resolve('src/images'),
      'data': path.resolve('src/data'),
      'schemas': path.resolve('src/schemas'),
    },
    fallback: {
      // Consider replacing brwoserify-zlib-next c. 2016 package with pako, which it's just a wrapper for
      /* Some of these polyfills may not even be necessary, and were added in an attempt to deal with build issues
            while upgrading to Webpack v5 */
      'zlib': require.resolve('browserify-zlib-next'),
      'assert': require.resolve('assert/'),
      'buffer': require.resolve('buffer/'),
      'stream': require.resolve('stream-browserify'),
      /*
      "url": require.resolve("url/"),
      "path": require.resolve("path-browserify"),
      "crypto": require.resolve("crypto-browserify"),
      "os": require.resolve("os-browserify/browser"),
      "https": require.resolve("https-browserify"),
      "http": require.resolve("stream-http"),
      "vm": require.resolve("vm-browserify"),
      "constants": require.resolve("constants-browserify"),
      // "fs": false
      */
    },
  },
  optimization: {
    usedExports: true,
  },
  output: {
    path: path.join(__dirname, '../build'),
    chunkFilename: '[name].bundle.js',
    // assetModuleFilename: '[contenthash][ext]',
    publicPath: '/',
    clean: true, // we already do rimraf on the build dir, but this should obviate that
  },
  plugins: [
    // new webpack.optimize.CommonsChunkPlugin({
    //   name: 'lib',
    //   filename: 'lib.js'
    // }),
    new HtmlWebpackPlugin({
      inject: true,
      template: path.join(__dirname, '../src/index.ejs'),
      version: pkgJson.version,
      // gapiKey: process.env.CORIOLIS_GAPI_KEY || '',
      date: buildDate,
    }),
    new MiniCssExtractPlugin({
      filename: 'app.css',
    }),
    // Solve missing Buffer polyfill that breaks module engineering
    new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/, use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader'],
      },
      {
        test: /\.less$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'less-loader'],
      },
      {
        test: /\.(js|jsx)$/,
        use: ['babel-loader'],
      },
      {
        test: /\.icon\.svg$/i,
        use: [
          {
            loader: '@svgr/webpack',
            options: {
              typescript: true,
            },
          },
        ],
      },
      {
        test: /\.(jpe?g|png|gif|ico|eot|ttf|woff|woff2?)(\?v=\d+\.\d+\.\d+)?$/i,
        type: 'asset/resource',
      },
      {
        test: path => (
          path.endsWith('.svg') && !path.endsWith('.icon.svg')
        ),
        type: 'asset/resource',
      },
    ],
  },
};
