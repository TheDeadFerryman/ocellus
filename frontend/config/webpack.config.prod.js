const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = merge(common, {
  devtool: 'source-map',
  mode: 'production',
  optimization: {
    minimize: true,
  },
  output: {
    globalObject: 'this',
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        { from: 'src/schemas', to: 'schemas' },
      ],
    }),
    new MiniCssExtractPlugin({
      filename: '[contenthash:6].css',
    }),
    new webpack.DefinePlugin({
      'window.OCELLUS_SERVER_URL': JSON.stringify(process.env.OCELLUS_SERVER_URL),
    }),
  ],
});
