import React from 'react';
import OutfittingPage from '../../coriolis/pages/OutfittingPage';
import PropTypes from 'prop-types';
import Tooltip from '../../coriolis/components/Tooltip';
import { EventEmitter } from 'fbemitter';
import { getLanguage } from '../../coriolis/i18n/Language';

class CoriolisOutfittingAdapter extends React.Component {
  static childContextTypes = {
    language: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    noTouch: PropTypes.bool.isRequired,
    sizeRatio: PropTypes.number.isRequired,
    tooltip: PropTypes.func.isRequired,
    termtip: PropTypes.func.isRequired,
    showModal: PropTypes.func.isRequired,
    hideModal: PropTypes.func.isRequired,
    openMenu: PropTypes.func.isRequired,
    closeMenu: PropTypes.func.isRequired,
    onWindowResize: PropTypes.func.isRequired,
    onCommand: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.emitter = new EventEmitter();

    this.state = {
      language: getLanguage(this.props.lang),
      tooltip: null,
    };
  }

  getChildContext() {
    return {
      language: this.state.language,
      route: {},
      noTouch: !('ontouchstart' in window || navigator.msMaxTouchPoints || navigator.maxTouchPoints),
      sizeRatio: 1,
      tooltip: this.tooltip,
      termtip: this.termtip,
      showModal: this.showModal,
      hideModal: this.hideModal,
      openMenu: this.openMenu,
      closeMenu: this.closeMenu,
      onWindowResize: cb => this.emitter.addListener('windowResize', cb),
      onCommand: cb => this.emitter.addListener('command', listener),
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.lang !== this.props.lang) {
      this.setState({
        language: getLanguage(this.props.lang),
      });
    }
  }

  componentWillMount() {
    // window.onerror = this._onError.bind(this);
    window.addEventListener('resize', () => this.emitter.emit('windowResize'));
    document.addEventListener('keydown', this._keyDown);
  }


  tooltip = (content, rect, opts) => {
    if (!content && this.state.tooltip) {
      this.setState({ tooltip: null });
    } else if (content) {
      this.setState({ tooltip: <Tooltip rect={rect} options={opts}>{content}</Tooltip> });
    }
  };

  termtip = (term, opts, event, e2) => {
    if (opts && opts.nativeEvent) { // Opts is the SyntheticEvent
      event = opts;
      opts = { cap: true };
    }
    if (e2 instanceof Object && e2.nativeEvent) { // E2 is the SyntheticEvent
      event = e2;
    }

    this.tooltip(
      <div className={'cen' + (opts.cap ? ' cap' : '')}>{this.state.language.translate(term)}</div>,
      event.currentTarget.getBoundingClientRect(),
      opts,
    );
  };

  showModal = (content) => {
    let modal = (
      <div
        className="modal-bg"
        onClick={(e) => this.hideModal()}
      >
        {content}
      </div>
    );
    this.setState({ modal });
  };

  /**
   * Hides any open modal
   */
  hideModal = () => {
    this.setState({ modal: null });
  };

  openMenu = (currentMenu) => {
    if (this.state.currentMenu != currentMenu) {
      this.setState({ currentMenu });
    }
  };

  /**
   * Closes the open menu
   */
  closeMenu = () => {
    if (this.state.currentMenu) {
      this.setState({ currentMenu: null });
    }
  };


  render() {
    return (
      <>
        <OutfittingPage
          name={this.props.name}
          ship={this.props.model}
          code={this.props.data}
          onSave={this.props.onSave}
          canSave={this.props.canSave}
          currentMenu={this.state.currentMenu}
        />
        {this.state.modal}
        {this.state.tooltip}
      </>
    );
  }
}

export default CoriolisOutfittingAdapter;
