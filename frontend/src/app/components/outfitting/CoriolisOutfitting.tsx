import * as React from 'react';
import CoriolisOutfittingAdapterUntyped from './CoriolisOutfittingAdapter';
import { Ship } from 'data/coriolis';
import { useLang } from 'app/util/lang';

export interface CoriolisOutfittingProps {
  ship: Omit<Ship, 'data'> & {
    data?: string;
  };
  canSave?: boolean;
  onSave: (ship: Ship) => unknown;
}

interface AdapterProps extends Omit<Ship, 'data'> {
  lang: string;
  data?: string;
  canSave: boolean;
  onSave: (ship: Ship) => unknown;
}

const CoriolisOutfittingAdapter =
  CoriolisOutfittingAdapterUntyped as unknown as React.ComponentClass<AdapterProps>;

const CoriolisOutfitting: React.FC<CoriolisOutfittingProps> = ({
  ship: {
    name,
    model,
    data,
  },
  canSave,
  onSave,
}) => {
  const [lang] = useLang();

  return (
    <CoriolisOutfittingAdapter
      lang={lang}
      name={name}
      data={data}
      model={model}
      onSave={onSave}
      canSave={canSave ?? false}
    />
  );
};

export default CoriolisOutfitting;
