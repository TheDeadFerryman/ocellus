import React, { useEffect } from 'react';
import Topbar from 'app/components/page/topbar';
import { isNil } from 'lodash-es';
import LoadingPage from 'app/router/LoadingPage';
import ErrorPage from 'app/router/ErrorPage';
import classNames from 'classnames';

export interface BasicPageProps extends React.PropsWithChildren {
  title?: string;
  error?: string;
  loading?: boolean;
  scrollable?: boolean;
  bodyClassName?: string;
}

const BasicPage: React.FC<BasicPageProps> = ({
  children, title, error,
  loading = false, scrollable = false,
  bodyClassName,
}) => {

  useEffect(() => {
    document.title = isNil(title) ? 'Ocellus' : `${title} | Ocellus`;
  }, [title]);

  const isSteadyState = !loading && isNil(error);

  return (
    <div className={'flex flex-col h-full items-stretch'}>
      <Topbar />
      <div
        className={classNames(
          bodyClassName,
          'flex flex-row grow',
          'overflow-auto',
          { scrollable },
        )}
      >
        {(loading) && <LoadingPage />}
        {(error && !loading) && <ErrorPage error={error} />}
        {isSteadyState && children}
      </div>
    </div>
  );
};

export default BasicPage;
