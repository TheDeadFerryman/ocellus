import React, { PropsWithChildren } from 'react';
import classNames from 'classnames';
import LoadingSplash from 'kit/components/LoadingSplash';
import { isNil } from 'lodash-es';
import Input from 'kit/components/Input';
import Button from 'kit/components/Button';

export interface FormPageProps extends PropsWithChildren {
  title: React.ReactNode;
  footer: React.ReactNode;
  loading: boolean;
}

const FormPage: React.FC<FormPageProps> = ({
  title, footer, children,
  loading,
}) => (
  <div className={'flex flex-row grow items-center'}>
    <div className={'flex flex-col items-center grow gap-2'}>
      <h1 className={'text-center'}>{title}</h1>
      <div className={classNames(
        'relative',
        'border border-primary',
      )}>
        <LoadingSplash
          visible={loading}
          className={'absolute w-full h-full'}
        />
        <div className={'flex flex-col items-stretch gap-1 p-6'}>
          {children}
        </div>
      </div>
      <div className={'flex flex-row items-center gap-4'}>
        {footer}
      </div>
    </div>
  </div>
);

export default FormPage;
