import React from 'react';
import Modal from 'kit/components/Modal';
import { BaseOption, Select } from 'kit';
import { Languages } from 'app/coriolis/i18n/Language';
import { AvailableLanguages, Lang, useLang } from 'app/util/lang';


const LanguageOptions: BaseOption<Lang>[] = AvailableLanguages.map(lang => ({
  value: lang,
  render: Languages[lang],
}));

export interface SetLangModalProps {
  visible: boolean;
  onClose: () => unknown;
}

const SetLangModal: React.FC<SetLangModalProps> = ({
  visible, onClose,
}) => {
  const [lang, setLang] = useLang();

  return (
    <Modal
      visible={visible}
      onClose={onClose}
    >
      <div className={'flex flex-col gap-2'}>
        <h3 className={'glow-primary'}>Set shipyard language</h3>
        <Select
          value={lang}
          onChange={setLang}
          options={LanguageOptions}
        />
      </div>
    </Modal>
  );
};

export default SetLangModal;
