import React, { useCallback, useEffect, useState } from 'react';
import Modal from 'kit/components/Modal';
import { useUpdatePassword } from 'app/api/hooks';
import { Button, Input, LoadingSplash } from 'kit';
import { isNil } from 'lodash-es';

export interface ChangePasswordModalProps {
  visible: boolean;
  onClose: () => unknown;
}

const ChangePasswordModal: React.FC<ChangePasswordModalProps> = ({
  visible, onClose,
}) => {
  const [oldPwd, setOldPwd] = useState('');
  const [newPwd, setNewPwd] = useState('');

  const {
    data, loading, error,
    reset, fetch,
  } = useUpdatePassword();

  useEffect(() => {
    if (!isNil(data)) {
      reset();
      onClose();
    }
  }, [data, reset, onClose]);

  const setPassword = useCallback(() => {
    fetch({
      current: oldPwd,
      _new: newPwd,
    });
  }, [oldPwd, newPwd]);

  return (
    <Modal
      visible={visible}
      onClose={onClose}
    >
      <div className={'relative flex flex-col gap-2'}>
        <LoadingSplash
          visible={loading}
          className={'absolute inset-0'}
        />
        <h3 className={'glow-primary'}>Change password</h3>
        <div className={'flex flex-col'}>
          <label htmlFor={'pwd'}>Current password</label>
          <Input
            value={oldPwd}
            type={'password'}
            onChange={setOldPwd}
          />
        </div>
        <div className={'flex flex-col'}>
          <label htmlFor={'pwd'}>New password</label>
          <Input
            value={newPwd}
            type={'password'}
            onChange={setNewPwd}
          />
        </div>
        <div className={'text-error glow-error'}>
          {error}
        </div>
        <Button
          variant={'secondary'}
          onClick={setPassword}
        >
          Submit
        </Button>
      </div>
    </Modal>
  );
};

export default ChangePasswordModal;
