import React, { Dispatch, PropsWithChildren, SetStateAction, useCallback, useState } from 'react';
import { useMe } from 'app/router/MeContext';
import CmdrIcon from 'images/icons/CmdrIcon.icon.svg';
import classNames from 'classnames';
import { useNavigate } from 'react-router-dom';
import { AppTokenProvider } from 'app/config';
import SetLangModal from 'app/components/page/topbar/profile/SetLangModal';
import ChangePasswordModal from 'app/components/page/topbar/profile/ChangePasswordModal';

interface MenuItemProps extends PropsWithChildren {
  onClick: () => void;
}

const MenuItem: React.FC<MenuItemProps> = ({
  onClick, children,
}) => (
  <div
    className={classNames(
      'px-1',
      'box-border',
      'bg-black text-shade',
      'hover:bg-shade hover:text-black',
      'active:bg-shade/75',
      'transition cursor-pointer !select-none',
    )}
    onClick={onClick}
  >
    {children}
  </div>
);

const ProfileMenu: React.FC = () => {
  const { name } = useMe();

  const [opened, setOpened] = useState(false);
  const [langModal, setLangModal] = useState(false);
  const [pwdModal, setPwdModal] = useState(false);

  const navigate = useNavigate();

  const onLogout = useCallback(() => {
    AppTokenProvider.dropToken();
  }, [navigate]);

  const openModal = useCallback((
    setModal: Dispatch<SetStateAction<boolean>>,
  ) => () => {
    setOpened(false);
    setModal(true);
  }, [setOpened]);

  return (
    <div
      className={classNames(
        'relative overflow-visible',
        'flex flex-row items-stretch',
        'mr-4',
      )}
      onMouseEnter={() => setOpened(true)}
      onMouseLeave={() => setOpened(false)}
    >
      <div
        className={classNames(
          'flex flex-row items-center',
          'sm:px-4 px-8 gap-2',
          'text-shade bg-black',
          'hover:text-black hover:bg-shade',
          { '!text-black !bg-shade': opened },
          'transition cursor-pointer',
        )}
      >
        <CmdrIcon
          className={'h-8'}
        />
        <span
          className={'leading-none sm:inline-block hidden'}
        >
          CMDR&nbsp;{name}
        </span>
      </div>
      <div
        className={classNames(
          'absolute z-30',
          'top-[100%] left-0 w-full',
          'flex flex-col items-stretch',
          'border border-shade',
          'text-shade',
          { hidden: !opened },
        )}
      >
        <MenuItem onClick={openModal(setLangModal)}>
          Set language
        </MenuItem>
        <MenuItem onClick={openModal(setPwdModal)}>
          Change password
        </MenuItem>
        <MenuItem onClick={onLogout}>
          Log out
        </MenuItem>
      </div>
      <SetLangModal
        visible={langModal}
        onClose={() => setLangModal(false)}
      />
      <ChangePasswordModal
        visible={pwdModal}
        onClose={() => setPwdModal(false)}
      />
    </div>
  );
};

export default ProfileMenu;
