import React from 'react';
import OcellusLogo from 'images/icons/OcellusLogo.icon.svg';
import classNames from 'classnames';
import ProfileMenu from 'app/components/page/topbar/profile';
import { Link } from 'react-router-dom';

export interface TopbarProps {
  className?: string;
}

const Topbar: React.FC<TopbarProps> = ({
  className,
}) => (
  <div className={classNames(
    className,
    'flex flex-row',
    'border-b border-shade',
  )}>
    <Link
      className={classNames(
        'flex flex-row items-center sm:gap-2 gap-1',
        '!text-secondary p-2 px-4',
        '!no-underline',
      )}
      to={'/'}
    >
      <OcellusLogo className={'h-8'} />
      <span className={classNames(
        'uppercase text-lg leading-none glow-secondary',
        'sm:block hidden',
      )}>
        Ocellus
      </span>
    </Link>
    <div className={'flex flex-row grow items-stretch sm:ml-4 ml-2'}>
      <Link
        to={'/fleet'}
        className={classNames(
          'flex flex-row items-center sm:px-2 px-1',
          'bg-black text-primary',
          'hover:bg-shade hover:text-black',
          'transition !no-underline',
        )}
      >
        Fleets
      </Link>
    </div>
    <ProfileMenu />
  </div>
);

export default Topbar;
