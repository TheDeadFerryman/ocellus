import React, { useCallback, useEffect } from 'react';
import { useSearchFleets } from 'app/api/hooks';
import { isNil, noop } from 'lodash-es';
import LoadingSplash from 'kit/components/LoadingSplash';
import Fleet from 'app/components/fleet/Fleet';
import { isTruthy } from 'app/util';

export interface FleetListBodyProps {
  page: number;
  pageSize: number;
  query: string;
  onTotalChanged: (totalPages: number) => unknown;
  updateFlag?: boolean;
  onUpdateFlagUsed?: () => unknown;
}

const FleetsListBody: React.FC<FleetListBodyProps> = ({
  page, pageSize, query,
  onTotalChanged,
  updateFlag, onUpdateFlagUsed = noop,
}) => {
  const { data, loading, error, fetch } = useSearchFleets();

  const refetch = useCallback(() => {
    fetch(query, page, pageSize);
  }, [page, pageSize, query]);

  useEffect(() => refetch(), [refetch]);

  useEffect(() => {
    if (isTruthy(updateFlag)) {
      (onUpdateFlagUsed ?? noop)();
      refetch();
    }
  }, [updateFlag, onUpdateFlagUsed, refetch]);

  useEffect(() => {
    const totalPages = data?.pages;
    if (!isNil(totalPages)) {
      onTotalChanged(totalPages);
    }
  }, [data?.pages]);

  if (isNil(data)) {
    return (
      <div className={'flex flex-col items-stretch'}>
        <LoadingSplash
          visible={loading}
          className={'py-8'}
        />
        {error && (
          <div className={'flex flex-col items-center py-8'}>
            <div className={'text-xl text-error'}>
              {error}
            </div>
          </div>
        )}
      </div>
    );
  }

  const { content, total, pages } = data;

  return (
    <div className={'flex flex-col items-stretch scrollable overflow-auto pr-1 snap-y'}>
      {content.map(fleet => (
        <Fleet
          key={fleet.id}
          fleet={fleet}
          onUpdate={refetch}
        />
      ))}
    </div>
  );
};

export default FleetsListBody;
