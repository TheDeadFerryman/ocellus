import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { AccessLevel } from 'app/api/ocellus';
import Modal from 'kit/components/Modal';
import { Button, Input, LoadingSplash } from 'kit';
import { debounce, isEmpty, isNil } from 'lodash-es';
import MembersSearchResults from 'app/components/fleet/fleetManager/addMember/MembersSearchResults';
import MembershipLevelSelect from 'app/components/fleet/fleetManager/MembershipLevelSelect';
import { useAddFleetMember } from 'app/api/hooks';
import { isNotNil } from 'app/util';

export interface AddMemberModalProps {
  fleetId: string;
  visible: boolean;
  excludeIds: string[];
  onClose: () => unknown;
  onMemberAdded: () => unknown;
}

const AddMemberModal: React.FC<AddMemberModalProps> = ({
  fleetId, visible, excludeIds,
  onClose, onMemberAdded,
}) => {
  const [query, setQuery] = useState('');
  const [
    level, setLevel,
  ] = useState<AccessLevel>('Read');
  const [cmdr, setCmdr] = useState<string>();
  const [formError, setFormError] = useState<string>();

  const updateQuery = useCallback(debounce((value: string) => {
    setQuery(value);
  }, 500), [setQuery]);

  const { data, loading, error, fetch } = useAddFleetMember();

  const addMember = useCallback(() => {
    if (isNil(cmdr)) {
      setFormError('Specify a CMDR to hire!');
    } else {
      fetch(fleetId, { cmdr, level });
    }
  }, [cmdr, level, fetch, setFormError]);

  const errors = useMemo(() => [
    formError,
    error,
  ].filter(isNotNil), [formError, error]);

  useEffect(() => {
    if (!isNil(data)) {
      onMemberAdded();
      onClose();
    }
  }, [data]);

  return (
    <Modal
      visible={visible}
      onClose={onClose}
    >
      <div className={'flex flex-col grow gap-2 relative'}>
        <LoadingSplash
          visible={loading}
          className={'absolute inset-0'}
        />
        <h3 className={'glow-primary'}>Hire member</h3>
        <div className={'flex flex-col'}>
          <label htmlFor={'cmdr'}>
            CMDR call sign
          </label>
          <Input
            id={'cmdr'}
            className={'w-64'}
            onChange={updateQuery}
          />
        </div>
        <MembersSearchResults
          query={query}
          selected={cmdr}
          onSelected={setCmdr}
          excludeIds={excludeIds}
        />
        <div className={'flex flex-col mt-2'}>
          <label>Membership level</label>
          <MembershipLevelSelect
            level={level}
            onChange={setLevel}
            queryClassName={'w-full'}
          />
        </div>
        {!isEmpty(errors) && errors.map(error => (
          <div className={'text-center text-error text-lg'}>
            {error}
          </div>
        ))}
        <div className={'flex flex-row grow gap-2'}>
          <Button
            variant={'error'}
            onClick={onClose}
          >
            Cancel
          </Button>
          <Button
            className={'grow'}
            variant={'primary'}
            onClick={addMember}
          >
            Hire
          </Button>
        </div>
      </div>
    </Modal>
  );
};

export default AddMemberModal;
