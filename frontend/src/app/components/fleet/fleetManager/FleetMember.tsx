import React, { useCallback, useEffect, useMemo, useState } from 'react';
import classNames from 'classnames';
import CmdrIcon from 'images/icons/CmdrIcon.icon.svg';
import Button from 'kit/components/Button';
import { FleetMemberDTO } from 'app/api/ocellus';
import MembershipLevelSelect from 'app/components/fleet/fleetManager/MembershipLevelSelect';
import { LoadingSplash } from 'kit';
import { useEditFleetMember, useKickFleetMember } from 'app/api/hooks';
import { isNil } from 'lodash-es';

export interface FleetMemberProps {
  fleetId: string;
  member: FleetMemberDTO;
  onUpdated: () => unknown;
}

const FleetMember: React.FC<FleetMemberProps> = ({
  fleetId,
  member, onUpdated,
}) => {
  const [level, setLevel] = useState(member.level);
  const [error, setError] = useState<string>();

  const {
    loading: editLoading,
    data: editData,
    error: editError,
    fetch: editFetch,
    reset: editReset,
  } = useEditFleetMember();
  const {
    loading: kickLoading,
    data: kickData,
    error: kickError,
    fetch: kickFetch,
    reset: kickReset,
  } = useKickFleetMember();

  useEffect(() => {
    setLevel(member.level);
  }, [member.level]);

  useEffect(() => {
    if (!isNil(kickError)) {
      setError(kickError);
    }
  }, [kickError]);

  useEffect(() => {
    if (!isNil(editError)) {
      setError(editError);
    }
  }, [editError]);

  useEffect(() => {
    if (!isNil(editData) || !isNil(kickData)) {
      onUpdated();
      editReset();
      kickReset();
    }
  }, [editData, kickData]);

  const editMember = useCallback(() => {
    editFetch(fleetId, {
      cmdr: member.cmdr.name, level,
    });
  }, [member.cmdr.name, level]);

  const kickMember = useCallback(() => {
    kickFetch(fleetId, {
      cmdr: member.cmdr.name,
    });
  }, []);

  const renderError = useMemo(() => (
    error ?? editError ?? kickError
  ), [editError, kickError, error]);

  const loading = kickLoading || editLoading;

  return (
    <div className={classNames(
      'relative',
      'flex flex-row items-center gap-4',
      'p-4 border-shade border',
    )}>
      <LoadingSplash
        visible={loading}
        className={'absolute inset-0'}
      />
      <CmdrIcon className={'h-12'} />
      <div className={'flex flex-col grow items-start'}>
        <span>CMDR</span>
        <h2>{member.cmdr.name}</h2>
      </div>
      <div className={'flex flex-col items-stretch gap-2'}>
        <div className={'flex flex-row items-stretch'}>
          <MembershipLevelSelect
            level={level}
            onChange={setLevel}
            className={'w-14'}
            queryClassName={'border-r-0'}
          />

          <Button
            className={classNames(
              '!text-sm uppercase !leading-none',
              '!px-1 !py-0',
            )}
            onClick={editMember}
          >
            Save
          </Button>
        </div>
        <Button
          variant={'error'}
          onClick={kickMember}
        >
          Fire
        </Button>
        <div className={'text-error text-sm'}>{renderError}</div>
      </div>
    </div>
  );
};

export default FleetMember;
