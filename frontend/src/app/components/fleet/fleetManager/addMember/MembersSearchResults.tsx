import React, { useCallback, useEffect, useState } from 'react';
import { isNil } from 'lodash-es';
import { LoadingSplash } from 'kit';
import { useSearchCmdrs } from 'app/api/hooks';
import classNames from 'classnames';
import CmdrIcon from 'images/icons/CmdrIcon.icon.svg';

export interface MembersSearchResultsProps {
  query: string;
  excludeIds: string[];

  selected?: string;
  onSelected: (value?: string) => unknown;
}

const MembersSearchResults: React.FC<MembersSearchResultsProps> = ({
  query, excludeIds,
  selected, onSelected,
}) => {
  const {
    data: cmdrs, loading, error, fetch,
  } = useSearchCmdrs();

  const [selectedInt, setSelectedInt] = useState<string>();

  useEffect(() => {
    setSelectedInt(selected);
  }, [selected]);

  useEffect(() => {
    setSelectedInt(undefined);
    fetch({ query, excludeIds });
  }, [query]);

  useEffect(() => {
    onSelected(selectedInt);
  }, [selectedInt, onSelected]);


  const changeSelection = useCallback((name: string) => setSelectedInt(
    selected => (
      name === selected
        ? undefined
        : name
    ),
  ), [setSelectedInt]);

  if (!isNil(error)) {
    return (
      <div className={'flex flex-col items-center my-2'}>
        <h3 className={'text-error'}>
          {error}
        </h3>
      </div>
    );
  }

  return (
    <div className={'flex flex-col items-stretch relative'}>
      {loading
        ? (
          <LoadingSplash
            visible
            className={'mt-6 mb-2'}
          />
        )
        : (cmdrs ?? []).map(cmdr => (
          <div
            key={cmdr.name}
            className={classNames(
              'flex flex-row items-center gap-2 px-2 py-0.5',
              'border border-primary not-last:border-b-0',
              'bg-black text-primary',
              'hover:bg-primary hover:text-black',
              { '!bg-secondary !text-black !border-secondary': (cmdr.name === selectedInt) },
              'transition cursor-pointer',
            )}
            onClick={() => changeSelection(cmdr.name)}
          >
            <CmdrIcon className={'h-6'} />
            <span>{cmdr.name}</span>
          </div>
        ))
      }
    </div>
  );
};

export default MembersSearchResults;
