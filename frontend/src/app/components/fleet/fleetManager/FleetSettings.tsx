import React, { useCallback, useEffect, useMemo, useState } from 'react';
import Button from 'kit/components/Button';
import { FleetDTO, FleetInputDTO } from 'app/api/ocellus';
import { LoadingSplash } from 'kit';
import { useDropFleet, useEditFleet } from 'app/api/hooks';
import { isNil } from 'lodash-es';
import { useNavigate } from 'react-router-dom';
import FleetForm from 'app/components/fleet/fleetManager/FleetForm';
import { isNotNil } from 'app/util';
import ConfirmationButton from 'kit/components/ConfirmationButton';

export interface FleetSettingsProps {
  fleet: FleetDTO;
  onUpdate: () => unknown;
}

const FleetSettings: React.FC<FleetSettingsProps> = ({
  fleet, onUpdate,
}) => {
  const navigate = useNavigate();
  const [settings, setSettings] = useState<FleetInputDTO>(fleet);

  const {
    loading: editLoading,
    error: editError,
    data: editData,
    fetch: editFetch,
    reset: editReset,
  } = useEditFleet();
  const {
    loading: dropLoading,
    error: dropError,
    data: dropData,
    fetch: dropFetch,
    reset: dropReset,
  } = useDropFleet();

  useEffect(() => {
    setSettings(fleet);
  }, [fleet]);

  useEffect(() => {
    if (!isNil(editData)) {
      console.log('refresh');
      editReset();
      dropReset();
      onUpdate();
    }
  }, [editData, editReset, dropReset]);

  useEffect(() => {
    if (!isNil(dropData)) {
      navigate('/fleets');
    }
  }, []);

  const saveFleet = useCallback(() => {
    editFetch(fleet.id, settings);
  }, [editFetch, fleet.id, settings]);

  const dropFleet = useCallback(() => {
    dropFetch(fleet.id);
  }, [dropFetch, fleet.id]);

  const loading = editLoading || dropLoading;

  const errors = useMemo(() => [
    editError,
    dropError,
  ].map(isNotNil), [editError, dropError]);

  return (
    <div className={'relative'}>
      <LoadingSplash
        visible={loading}
        className={'absolute inset-0'}
      />
      <h3 className={'glow-primary'}>Settings</h3>
      <div className={'flex flex-col gap-3'}>
        <FleetForm
          fleet={settings}
          onChange={setSettings}
        />
        <div className={'flex flex-col items-center'}>
          {errors.map(error => (
            <span>{error}</span>
          ))}
        </div>
        <div className={'flex flex-row gap-2'}>
          <Button
            onClick={saveFleet}
            variant={'secondary'}
            className={'uppercase grow w-[50%]'}
          >
            Save
          </Button>
          <ConfirmationButton
            variant={'error'}
            onClick={dropFleet}
            position={'bottom:left'}
            className={'grow w-[50%]'}
            popup={'Confirm your order, CMDR?'}
          >
            Dissolve
          </ConfirmationButton>
        </div>
      </div>
    </div>
  );
};

export default FleetSettings;
