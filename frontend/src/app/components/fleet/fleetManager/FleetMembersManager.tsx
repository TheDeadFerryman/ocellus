import React, { useCallback, useEffect, useMemo, useState } from 'react';
import classNames from 'classnames';
import LoadingSplash from 'kit/components/LoadingSplash';
import { useFetchFleetMembers } from 'app/api/hooks';
import { isNil, union } from 'lodash-es';
import FleetMember from 'app/components/fleet/fleetManager/FleetMember';
import PlusIcon from 'images/icons/PlusIcon.icon.svg';
import AddMemberModal from 'app/components/fleet/fleetManager/AddMemberModal';
import { FleetDTO } from 'app/api/ocellus';

export interface FleetMembersManagerProps {
  fleet: FleetDTO;
}

const FleetMembersManager: React.FC<FleetMembersManagerProps> = ({
  fleet,
}) => {
  const {
    data, loading, error, fetch,
  } = useFetchFleetMembers({
    resetData: false,
  });

  const [addModal, setAddModal] = useState(false);

  const refetch = useCallback(() => {
    fetch(fleet.id);
  }, [fleet]);

  useEffect(() => refetch(), [refetch]);

  if (!isNil(error)) {
    return (
      <>
        <h3>Members</h3>
        <div className={'flex flex-col mt-8'}>
          <h3 className={'text-error'}>
            {error}
          </h3>
        </div>
      </>
    );
  }

  const excludeIds = useMemo(() => (
    union((data ?? []).map(member => member.cmdr.id), [fleet.owner.id])
  ), [data]);

  return (
    <>
      <h3 className={'glow-primary'}>Members</h3>
      <div className={classNames(
        'relative grow',
        'grid grid-flow-row auto-rows-max',
        'xl:grid-cols-3 lg:grid-cols-2 grid-cols-1',
        'gap-2',
      )}>
        <LoadingSplash
          visible={loading}
          className={'absolute w-full h-full'}
        />
        {(data ?? []).map(member => (
          <FleetMember
            member={member}
            fleetId={fleet.id}
            onUpdated={refetch}
          />
        ))}
        <div
          className={classNames(
            'flex flex-row items-center gap-4',
            'p-4 border-shade border',
            'bg-black text-neutral-300',
            'hover:!bg-shade hover:!text-black',
            'active:!bg-shade/75 active:!border-black',
            'transition cursor-pointer',
          )}
          onClick={() => setAddModal(true)}
        >
          <PlusIcon className={'h-12'} />
          <h2 className={'select-none'}>Hire member</h2>
        </div>
      </div>
      <AddMemberModal
        fleetId={fleet.id}
        visible={addModal}
        excludeIds={excludeIds}
        onMemberAdded={refetch}
        onClose={() => setAddModal(false)}
      />
    </>
  );
};

export default FleetMembersManager;
