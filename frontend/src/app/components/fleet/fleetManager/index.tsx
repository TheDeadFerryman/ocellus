import React from 'react';
import LoadingPage from 'app/router/LoadingPage';
import { isNil } from 'lodash-es';
import FleetMembersManager from 'app/components/fleet/fleetManager/FleetMembersManager';
import classNames from 'classnames';
import FleetSettings from 'app/components/fleet/fleetManager/FleetSettings';
import { FleetDTO } from 'app/api/ocellus';

export interface FleetManagerProps {
  fleet?: FleetDTO;
  onUpdate: () => unknown;
}

const FleetManager: React.FC<FleetManagerProps> = ({
  fleet, onUpdate,
}) => {

  if (isNil(fleet)) {
    return (
      <LoadingPage />
    );
  }

  return (
    <div className={'flex flex-col items-stretch grow mb-16 overflow-auto'}>
      <div className={'flex flex-col mb-2'}>
        <h1 className={'glow-primary'}>{fleet.name}</h1>
        <h2 className={'text-shade'}>
          Owner: CMDR&nbsp;
          <span className={'normal-case'}>
          {fleet.owner.name}
        </span>
        </h2>
      </div>
      <div className={classNames(
        'flex flex-row items-stretch grow',
        'border-y-2 border-shade',
        'overflow-auto',
      )}>
        <div className={'flex flex-col shrink border-r-2 border-shade py-2 pr-4'}>
          <FleetSettings
            fleet={fleet}
            onUpdate={onUpdate}
          />
        </div>
        <div className={'flex flex-col grow py-2 px-4 scrollable overflow-auto'}>
          <FleetMembersManager fleet={fleet} />
        </div>
      </div>
    </div>
  );
};

export default FleetManager;
