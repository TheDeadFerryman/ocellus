import React, { useCallback } from 'react';
import Input from 'kit/components/Input';
import Checkbox from 'kit/components/Checkbox';
import { FleetInputDTO } from 'app/api/ocellus';

export interface FleetFormProps {
  fleet: FleetInputDTO;
  onChange: (fleet: FleetInputDTO) => unknown;
}

const FleetForm: React.FC<FleetFormProps> = ({
  fleet: { isPublic, name }, onChange,
}) => {
  const setName = useCallback((newName: string) => onChange({
    isPublic, name: newName,
  }), [isPublic]);

  const setIsPublic = useCallback((newPublic: boolean) => onChange({
    isPublic: newPublic, name,
  }), [name]);

  return (
    <div className={'flex flex-col gap-3'}>
      <div className={'flex flex-col'}>
        <label htmlFor={'name'}>Name</label>
        <Input
          id={'name'}
          value={name}
          onChange={setName}
          autoComplete={'new-password'}
        />
      </div>
      <div className={'flex flex-col'}>
        <Checkbox
          value={isPublic}
          onChange={setIsPublic}
          label={<>Fleet is publicly visible&nbsp;&nbsp;</>}
        />
      </div>
    </div>
  );
};

export default FleetForm;
