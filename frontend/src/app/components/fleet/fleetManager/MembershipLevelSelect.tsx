import React from 'react';
import { BaseOption, Select } from 'kit';
import { AccessLevel } from 'app/api/ocellus';

const MembershipLevels: BaseOption<AccessLevel>[] = [
  {
    value: 'Read',
    render: 'Read',
  },
  {
    value: 'Add',
    render: 'Add',
  },
  {
    value: 'Edit',
    render: 'Edit',
  },
];

export interface MembershipLevelSelectProps {
  className?: string;
  queryClassName?: string;

  level: AccessLevel;
  onChange: (value: AccessLevel) => unknown;
}

const MembershipLevelSelect: React.FC<MembershipLevelSelectProps> = ({
  level, onChange,
  className, queryClassName,
}) => (
  <Select
    value={level}
    onChange={onChange}
    options={MembershipLevels}
    className={className}
    queryClassName={queryClassName}
  />
);

export default MembershipLevelSelect;
