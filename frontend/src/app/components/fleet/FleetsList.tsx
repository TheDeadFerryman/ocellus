import React, { useState } from 'react';
import FleetsListBody from 'app/components/fleet/FleetsListBody';
import Input from 'kit/components/Input';
import Pagination from 'kit/components/Pagination';
import { Button } from 'kit';
import NewFleetModal from 'app/components/fleet/NewFleetModal';
import { useDebouncedSetState } from 'app/util/hooks';

export interface FleetsListProps {

}

const FleetsList: React.FC<FleetsListProps> = () => {
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(20);
  const [query, setQuery] = useState('');
  const [totalPages, setTotalPages] = useState(0);
  const [updateFlag, setUpdateFlag] = useState(false);

  const [newFleet, setNewFleet] = useState(false);

  const updateQuery = useDebouncedSetState(setQuery, 500);

  return (
    <div className={'flex flex-col gap-2 overflow-auto'}>
      <div className={'flex flex-row items-end'}>
        <h1 className={'glow-primary grow'}>Fleets</h1>
        <div className={'flex flex-row gap-2'}>
          <Button
            variant={'primary'}
            onClick={() => setNewFleet(true)}
          >
            NEW FLEET
          </Button>
          <Input
            onChange={updateQuery}
            placeholder={'Search'}
          />
        </div>
      </div>
      <FleetsListBody
        page={page}
        query={query}
        pageSize={pageSize}
        updateFlag={updateFlag}
        onUpdateFlagUsed={() => setUpdateFlag(false)}
        onTotalChanged={setTotalPages}
      />
      <Pagination
        page={page}
        pageSize={pageSize}
        totalPages={totalPages}
        onPageChange={setPage}
        onPageSizeChange={setPageSize}
      />
      <NewFleetModal
        visible={newFleet}
        onUpdate={() => setUpdateFlag(true)}
        onClose={() => setNewFleet(false)}
      />
    </div>
  );
};

export default FleetsList;
