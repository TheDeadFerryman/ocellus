import React, { useCallback, useEffect, useState } from 'react';
import Modal from 'kit/components/Modal';
import { Button, LoadingSplash } from 'kit';
import { useCreateFleet } from 'app/api/hooks';
import FleetForm from 'app/components/fleet/fleetManager/FleetForm';
import { isNil } from 'lodash-es';

export interface NewFleetModalProps {
  visible: boolean;
  onClose: () => unknown;
  onUpdate: () => unknown;
}

const NewFleetModal: React.FC<NewFleetModalProps> = ({
  visible,
  onClose, onUpdate,
}) => {
  const {
    loading, error, data,
    fetch, reset,
  } = useCreateFleet();

  const [fleet, setFleet] = useState({
    name: '',
    isPublic: false,
  });

  const createFleet = useCallback(() => {
    fetch(fleet);
  }, [fleet, fetch]);

  useEffect(() => {
    if (!isNil(data)) {
      reset();
      onClose();
      onUpdate();
    }
  }, [data, onUpdate, reset]);

  return (
    <Modal
      visible={visible}
      onClose={onClose}
    >
      <div className={'flex flex-col grow gap-2 relative mx-2'}>
        <LoadingSplash
          visible={loading}
          className={'absolute inset-0'}
        />
        <h3 className={'glow-primary'}>Establish a fleet</h3>
        <FleetForm
          fleet={fleet}
          onChange={setFleet}
        />
        <div className={'text-error'}>
          {error}
        </div>
        <div className={'flex flex-row gap-2 mt-2'}>
          <Button
            variant={'error'}
            className={'shrink'}
            onClick={onClose}
          >
            Cancel
          </Button>
          <Button
            variant={'secondary'}
            onClick={createFleet}
          >
            Establish
          </Button>
        </div>
      </div>
    </Modal>
  );
};

export default NewFleetModal;
