import React, { useCallback, useEffect, useState } from 'react';
import { LoadingSplash } from 'kit';
import classNames from 'classnames';
import FleetIcon from 'images/icons/FleetIcon.icon.svg';
import { useSearchWritableFleets } from 'app/api/hooks';
import { isNil } from 'lodash-es';

export interface WritableFleetsSearchResultsProps {
  query: string;
  selected?: string;
  onSelected: (value?: string) => unknown;
}

const WritableFleetsSearchResults: React.FC<WritableFleetsSearchResultsProps> = ({
  query,
  selected, onSelected,
}) => {
  const {
    loading, error, data, fetch,
  } = useSearchWritableFleets();

  const [selectedInt, setSelectedInt] = useState<string>();

  useEffect(() => {
    setSelectedInt(selected);
  }, [selected]);

  useEffect(() => {
    setSelectedInt(undefined);
    fetch(query);
  }, [query]);

  useEffect(() => {
    onSelected(selectedInt);
  }, [selectedInt, onSelected]);

  const changeSelection = useCallback((name: string) => setSelectedInt(
    selected => (
      name === selected
        ? undefined
        : name
    ),
  ), [setSelectedInt]);

  if (!isNil(error)) {
    return (
      <div className={'flex flex-col items-center my-2'}>
        <h3 className={'text-error'}>
          {error}
        </h3>
      </div>
    );
  }

  return (
    <div className={'flex flex-col items-stretch relative'}>
      {loading
        ? (
          <LoadingSplash
            visible
            className={'mt-6 mb-2'}
          />
        )
        : (data ?? []).map(fleet => (
          <div
            key={fleet.id}
            className={classNames(
              'flex flex-row items-center gap-2 px-2 py-0.5',
              'border border-primary not-last:border-b-0',
              'bg-black text-primary',
              'hover:bg-primary hover:text-black',
              { '!bg-secondary !text-black !border-secondary': (fleet.id === selectedInt) },
              'transition cursor-pointer',
            )}
            onClick={() => changeSelection(fleet.id)}
          >
            <FleetIcon className={'h-6 py-0.5'} />
            <span>{fleet.name}</span>
          </div>
        ))
      }
    </div>
  );
};

export default WritableFleetsSearchResults;
