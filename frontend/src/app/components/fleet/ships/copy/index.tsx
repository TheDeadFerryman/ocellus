import React, { useCallback, useEffect, useMemo, useState } from 'react';
import Modal from 'kit/components/Modal';
import { ShipDTO } from 'app/api/ocellus';
import { Button, Input, LoadingSplash } from 'kit';
import { useDebouncedSetState } from 'app/util/hooks';
import WritableFleetsSearchResults from 'app/components/fleet/ships/copy/WritableFleetsSearchResults';
import { useCopyShip } from 'app/api/hooks';
import { isEmpty, isNil } from 'lodash-es';
import { isNotNil } from 'app/util';
import { useNavigate } from 'react-router-dom';

export interface ShipCopyModalProps {
  fleetId: string;
  ship: ShipDTO;
  visible: boolean;
  onClose: () => unknown;
}

const ShipCopyModal: React.FC<ShipCopyModalProps> = ({
  fleetId, ship,
  visible, onClose,
}) => {
  const navigate = useNavigate();

  const [query, setQuery] = useState('');
  const [toFleet, setToFleet] = useState<string>();

  const [formError, setFormError] = useState<string>();

  const updateQuery = useDebouncedSetState(setQuery, 500);

  const { data, loading, error, fetch, reset } = useCopyShip();

  const copyShip = useCallback(() => {
    if (isNil(toFleet)) {
      setFormError('Select target fleet!');
    } else {
      fetch(fleetId, ship.id, { toFleet });
    }
  }, [toFleet, fleetId, ship.id, fetch, setFormError]);

  useEffect(() => setFormError(undefined), [toFleet, setFormError]);

  useEffect(() => {
    if (!isNil(data) && !isNil(toFleet)) {
      navigate(`/fleets/${toFleet}/ships/${data.id}`);
    }
  }, [data]);

  const errors = useMemo(() => [
    error, formError,
  ].filter(isNotNil), [error, formError]);

  return (
    <Modal
      visible={visible}
      onClose={onClose}
    >
      <div className={'relative flex flex-col items-stretch gap-2'}>
        <LoadingSplash
          visible={loading}
          className={'absolute inset-0'}
        />
        <h3 className={'glow-primary'}>Copy ship</h3>
        <div className={'flex flex-col'}>
          <label htmlFor={'fleet'}>Target fleet</label>
          <Input
            id={'fleet'}
            placeholder={'Search'}
            onChange={updateQuery}
          />
        </div>
        <WritableFleetsSearchResults
          query={query}
          selected={fleetId}
          onSelected={setToFleet}
        />
        {!isEmpty(errors) && (
          <div className={'flex flex-col text-error'}>
            {errors.map(error => (
              <span>{error}</span>
            ))}
          </div>
        )}
        <Button
          variant={'secondary'}
          onClick={copyShip}
        >
          Copy
        </Button>
      </div>
    </Modal>
  );
};

export default ShipCopyModal;
