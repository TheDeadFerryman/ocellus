import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { ShipWithAccessLevelDTO } from 'app/api/ocellus';
import classNames from 'classnames';
import { shipModelName } from 'data/coriolis';
import { formatDateTime } from 'app/config';
import { isNil, kebabCase } from 'lodash-es';
import datesEqual from 'date-fns/isEqual';
import { Button, LoadingSplash } from 'kit';
import LinkButton from 'kit/components/LinkButton';
import { hasAccess } from 'app/api';
import ShipCopyModal from 'app/components/fleet/ships/copy';
import { useDropShip } from 'app/api/hooks';
import ConfirmationButton from 'kit/components/ConfirmationButton';

export interface ShipProps {
  fleetId: string;
  ship: ShipWithAccessLevelDTO;
  onUpdate: () => unknown;
}

const Ship: React.FC<ShipProps> = ({
  fleetId,
  ship: { ship, level: accessLevel },
  onUpdate,
}) => {
  const descriptionLines = useMemo(() => [
    ['Model', shipModelName(ship.model)],
    ['Author', ship.author.name],
    ['Created', formatDateTime(ship.createdOn)],
    ...(!datesEqual(ship.createdOn, ship.updatedOn) ? [
      ['Updated by', ship.updatedBy.name],
      ['Updated on', formatDateTime(ship.updatedOn)],
    ] : []),
  ], [ship]);

  const [copyModal, setCopyModal] = useState(false);

  const {
    data, loading, error,
    fetch, reset,
  } = useDropShip();

  useEffect(() => {
    if (!isNil(data)) {
      reset();
      onUpdate();
    }
  }, [data, onUpdate, reset]);

  const dropShip = useCallback(
    () => fetch(fleetId, ship.id),
    [fetch],
  );

  return (
    <div className={classNames(
      'relative',
      'flex flex-row items-start gap-2',
      'p-4 border-secondary border',
      'bg-black text-neutral-300',
    )}>
      <LoadingSplash
        visible={loading}
        className={'absolute inset-0'}
      />
      <div className={'flex flex-col grow'}>
        <h2 className={'text-primary glow-primary'}>
          {ship.name}
        </h2>
        {descriptionLines.map(([key, value]) => (
          <h3 key={kebabCase(key)} className={'text-secondary text-sm'}>
            {key}: {value}
          </h3>
        ))}
      </div>
      <div className={'flex flex-col items-end gap-2'}>
        <div className={'flex flex-col grow items-stretch gap-2'}>
          {hasAccess(accessLevel, 'Read') && (
            <>
              <LinkButton
                to={ship.id}
                variant={'secondary'}
                className={'!no-underline select-none'}
              >
                Inspect
              </LinkButton>
              <Button
                variant={'secondary'}
                onClick={() => setCopyModal(true)}
              >
                Copy
              </Button>
            </>
          )}
          {hasAccess(accessLevel, 'Edit') && (
            <ConfirmationButton
              variant={'error'}
              onClick={dropShip}
              position={'bottom:right'}
              buttonClassName={'w-full'}
              popup={'Confirm your order, CMDR?'}
            >
              Dismiss
            </ConfirmationButton>
          )}
        </div>
        <ShipCopyModal
          ship={ship}
          fleetId={fleetId}
          visible={copyModal}
          onClose={() => setCopyModal(false)}
        />
        {error && (
          <div className={'text-error glow-error'}>
            {error}
          </div>
        )}
      </div>
    </div>
  );
};

export default Ship;
