import React, { useCallback, useEffect, useState } from 'react';
import { AccessLevel, FleetDTO } from 'app/api/ocellus';
import classNames from 'classnames';
import { Input, Pagination } from 'kit';
import { useSearchShips } from 'app/api/hooks';
import { useDebouncedSetState } from 'app/util/hooks';
import ShipsResults from 'app/components/fleet/ships/ShipsResults';

export interface ShipyardsProps {
  fleet: FleetDTO;
  accessLevel: AccessLevel;
}

const DefaultPageSize = 20;

const FleetShips: React.FC<ShipyardsProps> = ({
  fleet, accessLevel,
}) => {
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(DefaultPageSize);

  const [query, setQuery] = useState('');

  const {
    data,
    loading, error,
    fetch, reset,
  } = useSearchShips({
    resetData: false,
  });

  const refetch = useCallback(() => fetch(
    fleet.id, query, page, pageSize,
  ), [fleet.id, query, page, pageSize]);

  useEffect(() => refetch(), [refetch]);

  const updateQuery = useDebouncedSetState(setQuery, 500);

  return (
    <div className={classNames(
      'flex flex-col items-stretch gap-2',
      'border-shade border-t-2 mt-2 pt-2',
    )}>
      <div className={'flex flex-row items-end'}>
        <h3 className={'grow glow-primary'}>Ships</h3>
        <Input
          placeholder={'Search'}
          onChange={updateQuery}
        />
      </div>
      <ShipsResults
        data={data}
        error={error}
        loading={loading}
        fleetId={fleet.id}
        onUpdate={refetch}
        fleetAccessLevel={accessLevel}
      />
      <div className={'flex flex-col items-center mt-2'}>
        <Pagination
          page={page}
          pageSize={pageSize}
          totalPages={data?.pages ?? 1}
          onPageChange={setPage}
          onPageSizeChange={setPageSize}
        />
      </div>
    </div>
  );
};

export default FleetShips;
