import React, { useState } from 'react';
import classNames from 'classnames';
import PlusIcon from 'images/icons/PlusIcon.icon.svg';
import ImportIcon from 'images/icons/ImportIcon.icon.svg';
import { AccessLevel, PageDTOShipWithAccessLevelDTO } from 'app/api/ocellus';
import { Button, LoadingSplash } from 'kit';
import { Link } from 'react-router-dom';
import { hasAccess } from 'app/api';
import Ship from 'app/components/fleet/ships/Ship';
import { noop } from 'lodash-es';
import ImportShipsModal from 'app/components/fleet/ships/ImportShipsModal';

let actionBlockStyles = classNames(
  'flex flex-row items-center gap-4',
  'p-4 border-secondary border',
  'bg-black text-secondary',
  'hover:!bg-secondary hover:!text-black',
  'active:!bg-secondary/75 active:!border-black',
  'visited:text-secondary',
  'transition cursor-pointer',
  '!no-underline',
);

const NewShipButton = () => (
  <Link
    to={'new'}
    className={actionBlockStyles}
  >
    <PlusIcon className={'h-12'} />
    <h2 className={'select-none'}>New ship</h2>
  </Link>
);

const ImportShipsButton = ({ onClick }: { onClick: () => unknown; }) => (
  <div
    className={actionBlockStyles}
    onClick={onClick}
  >
    <ImportIcon className={'h-12'} />
    <h2 className={'select-none'}>Import ships</h2>
  </div>
);

export interface ShipyardsResultsProps {
  error?: string;
  fleetId: string;
  loading: boolean;
  fleetAccessLevel: AccessLevel;
  data?: PageDTOShipWithAccessLevelDTO;
  onUpdate: () => unknown;
}

const ShipsResults: React.FC<ShipyardsResultsProps> = ({
  fleetId,
  loading, error, data,
  fleetAccessLevel,
  onUpdate,
}) => {
  const [importModal, setImportModal] = useState(false);

  return (
    <div className={classNames(
      'relative',
      'grid grid-flow-row auto-rows-max gap-2',
      'min-h-[100px]',
      'xl:grid-cols-3 lg:grid-cols-2 grid-cols-1',
    )}>
      <LoadingSplash
        visible={loading}
        className={'absolute inset-0'}
      />
      {(data?.content ?? []).map(ship => (
        <Ship
          ship={ship}
          fleetId={fleetId}
          onUpdate={onUpdate}
        />
      ))}
      {hasAccess(fleetAccessLevel, 'Add') && (
        <>
          <NewShipButton />
          <ImportShipsButton onClick={() => setImportModal(true)} />
        </>
      )}
      <ImportShipsModal
        fleetId={fleetId}
        visible={importModal}
        onClose={() => setImportModal(false)}
        onUpdate={onUpdate}
      />
    </div>
  );
};

export default ShipsResults;
