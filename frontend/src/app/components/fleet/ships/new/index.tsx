import React, { useCallback, useEffect, useMemo } from 'react';
import classNames from 'classnames';
import CoriolisOutfitting from 'app/components/outfitting/CoriolisOutfitting';
import { Ship, Ships } from 'data/coriolis';
import { useCreateShip } from 'app/api/hooks';
import Modal from 'kit/components/Modal';
import { isNil } from 'lodash-es';
import { LoadingSplash } from 'kit';
import { useNavigate } from 'react-router-dom';

export interface NewShipProps {
  fleetId: string;
  model: string;
  onLoadingStateChange: (state: boolean) => unknown;
}

const NewShip: React.FC<NewShipProps> = ({
  fleetId, model,
  onLoadingStateChange,
}) => {
  const navigate = useNavigate();

  const ship = useMemo<Ship>(() => ({
    model,
    name: Ships[model]?.properties.name ?? '',
    data: '',
  }), [model]);

  const {
    data, loading, error,
    fetch, reset,
  } = useCreateShip();

  useEffect(() => {
    onLoadingStateChange(loading);
  }, [loading]);


  const saveShip = useCallback((newShip: Ship) => {
    fetch(fleetId, {
      ...newShip,
      data: btoa(newShip.data),
    });
  }, [fleetId, ship]);

  useEffect(() => {
    if (!isNil(data)) {
      reset();
      navigate(`../../${data.id}`);
    }
  }, [data]);

  return (
    <div className={classNames(
      'relative',
      'flex flex-col grow items-stretch',
      'max-w-full h-full',
      'overflow-auto',
    )}>
      <LoadingSplash
        visible={loading}
        className={'absolute inset-0'}
      />
      <Modal
        onClose={reset}
        visible={!isNil(error)}
      >
        <div className={'flex flex-col items-stretch'}>
          <h3 className={'w-48 glow-primary'}>Save ship</h3>
          <div className={'my-2 text-error glow-error'}>
            {error}
          </div>
        </div>
      </Modal>
      <div className={'overflow-auto scrollable'}>
        <CoriolisOutfitting
          canSave
          ship={ship}
          onSave={saveShip}
        />
      </div>
    </div>
  );
};

export default NewShip;
