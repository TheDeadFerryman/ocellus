import React, { useCallback, useEffect, useState } from 'react';
import Modal from 'kit/components/Modal';
import { Button, LoadingSplash } from 'kit';
import { tryGetError } from 'app/util';
import { isArray, isNil } from 'lodash-es';
import { detailedJsonToBuild } from 'app/coriolis/components/ModalImport';
import { ShipCreateDTO } from 'app/api/ocellus';
import { useCreateShipsBulk, useSearchShips } from 'app/api/hooks';

export interface ImportShipsModalProps {
  fleetId: string;
  visible: boolean;
  onClose: () => unknown;
  onUpdate: () => unknown;
}

const tryParseInput = (parsed: unknown): ReturnType<typeof detailedJsonToBuild>[] => (
  isArray(parsed)
    ? parsed.map(ship => detailedJsonToBuild(ship))
    : tryParseInput([parsed])
);

const ImportShipsModal: React.FC<ImportShipsModalProps> = ({
  fleetId, visible,
  onClose, onUpdate,
}) => {
  const [importData, setImportData] = useState('');
  const [parseError, setParseError] = useState<string>();

  const {
    data, loading,
    error, fetch, reset,
  } = useCreateShipsBulk();

  const tryImport = useCallback(() => {
    setParseError(undefined);
    try {
      let parsed = JSON.parse(importData);


      const imports = tryParseInput(parsed)
        .map(({ name, shipId, code }) => ({
          name, model: shipId, data: btoa(code),
        }));

      createFromImported(imports);
    } catch (e) {
      setParseError(tryGetError(e));
    }
  }, [importData]);

  const createFromImported = useCallback((ships: ShipCreateDTO[]) => {
    fetch(fleetId, ships);
  }, []);

  useEffect(() => {
    if (!isNil(data)) {
      reset();
      onUpdate();
      onClose();
    }
  }, [data, reset, onClose, onUpdate]);

  return (
    <Modal
      visible={visible}
      onClose={onClose}
    >
      <div className={'relative flex flex-col items-stretch gap-4'}>
        <LoadingSplash
          visible={loading}
          className={'absolute inset-0'}
        />
        <h3 className={'glow-primary'}>Import ships</h3>
        <div className={'flex flex-col'}>
          <label htmlFor={'importCode'}>Data to import</label>
          <textarea
            value={importData}
            className={'w-[70vw] h-48 px-1 font-mono text-xs'}
            onChange={e => setImportData(e.target.value)}
          />
        </div>
        <div className={'flex flex-row'}>
          <div className={'text-error glow-error ml-1'}>
            {error ?? parseError}
          </div>
          <div className={'grow'}>

          </div>
          <Button
            onClick={tryImport}
            variant={'secondary'}
          >
            Import
          </Button>
        </div>
      </div>
    </Modal>
  );
};

export default ImportShipsModal;
