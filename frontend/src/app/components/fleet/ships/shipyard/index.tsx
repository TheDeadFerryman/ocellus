import React, { useCallback, useEffect, useMemo } from 'react';
import { AccessLevel, ShipDTO } from 'app/api/ocellus';
import classNames from 'classnames';
import { isNil, noop } from 'lodash-es';
import CoriolisOutfitting from 'app/components/outfitting/CoriolisOutfitting';
import { Ship } from 'data/coriolis';
import { hasAccess } from 'app/api';
import { useUpdateShip } from 'app/api/hooks';
import { LoadingSplash } from 'kit';
import Modal from 'kit/components/Modal';

export interface ShipyardProps {
  fleetId: string;
  ship: ShipDTO;
  accessLevel: AccessLevel;
  onUpdate: () => unknown;
}

const Shipyard: React.FC<ShipyardProps> = ({
  fleetId, ship, accessLevel,
  onUpdate,
}) => {
  const shipUnpacked = useMemo<Ship>(() => ({
    name: ship.name,
    model: ship.model,
    data: atob(ship.data),
  }), [ship]);

  const {
    data, loading, error,
    fetch, reset,
  } = useUpdateShip();

  const saveShip = useCallback((newShip: Ship) => {
    fetch(fleetId, ship.id, {
      ...newShip,
      data: btoa(newShip.data),
    });
  }, [fleetId, ship.id]);

  useEffect(() => {
    if (!isNil(data)) {
      reset();
      onUpdate();
    }
  }, [onUpdate, data, reset]);

  return (
    <div className={classNames(
      'relative',
      'flex flex-col grow items-stretch',
      'max-w-full h-full',
      'overflow-auto',
    )}>
      <LoadingSplash
        visible={loading}
        className={'absolute inset-0'}
      />
      <Modal
        onClose={reset}
        visible={!isNil(error)}
      >
        <div className={'flex flex-col items-stretch'}>
          <h3 className={'w-48 glow-primary'}>Save ship</h3>
          <div className={'my-2 text-error glow-error'}>
            {error}
          </div>
        </div>
      </Modal>
      <div className={'overflow-auto scrollable'}>
        <CoriolisOutfitting
          onSave={saveShip}
          ship={shipUnpacked}
          canSave={hasAccess(accessLevel, 'Edit')}
        />
      </div>
    </div>
  );
};

export default Shipyard;
