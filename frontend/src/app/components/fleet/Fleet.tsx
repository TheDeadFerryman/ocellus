import React, { useCallback, useEffect } from 'react';
import { FleetDTO } from 'app/api/ocellus';
import classNames from 'classnames';
import { useMe } from 'app/router/MeContext';
import { useDropFleet } from 'app/api/hooks';
import { LoadingSplash } from 'kit';
import { isNil } from 'lodash-es';
import ConfirmationButton from 'kit/components/ConfirmationButton';
import { formatDate } from 'app/config';
import LinkButton from 'kit/components/LinkButton';

export interface FleetProps {
  fleet: FleetDTO;
  onUpdate: () => unknown;
}

const Fleet: React.FC<FleetProps> = ({
  fleet, onUpdate,
}) => {
  const me = useMe();

  const {
    data, loading, error, fetch, reset,
  } = useDropFleet();

  const dropFleet = useCallback(() => {
    fetch(fleet.id);
  }, [fetch, fleet.id]);

  useEffect(() => {
    if (!isNil(data)) {
      reset();
      onUpdate();
    }
  }, [data, reset]);

  const isOwner = me.id === fleet.owner.id;

  return (
    <div className={classNames(
      'relative',
      'flex flex-row p-4',
      'border not-last:border-b-0 border-shade',
      'transition snap-start',
    )}>
      <LoadingSplash
        visible={loading}
        className={'absolute inset-0'}
      />
      <div className={'flex flex-col grow'}>
        <h2 className={'text-shade'}>{fleet.name}</h2>
        <h3>Owner: CMDR <span className={'!normal-case'}>{fleet.owner.name}</span></h3>
        <h3>Created: {formatDate(fleet.createdOn)}</h3>
        <h3>Crew: {fleet.membersCount} members</h3>
        <h3>Fleet size: {fleet.shipsCount} ships</h3>
      </div>
      <div className={'flex flex-col items-end gap-2'}>
        <div className={'flex flex-col items-stretch justify-start gap-1'}>
          <LinkButton
            variant={'primary'}
            to={fleet.id}
            className={'uppercase !no-underline visited:text-black'}
          >
            Inspect
          </LinkButton>
          {isOwner && (
            <>
              <LinkButton
                variant={'primary'}
                to={`${fleet.id}/manage`}
                className={'uppercase !no-underline visited:text-black'}
              >
                Manage
              </LinkButton>
              <ConfirmationButton
                variant={'error'}
                position={'top:right'}
                onClick={dropFleet}
                className={'uppercase'}
                popup={'Confirm your order, CMDR?'}
              >
                Dissolve
              </ConfirmationButton>
            </>
          )}
        </div>
        <div className={'text-error text-lg'}>
          {error}
        </div>
      </div>
    </div>
  );
};


export default Fleet;
