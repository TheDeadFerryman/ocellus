import { useAppStorageValue } from 'app/config';

export const AvailableLanguages = ['en', 'de', 'es', 'fr', 'it', 'ru', 'pl', 'pt', 'cn', 'ko'] as const;

export type Lang = typeof AvailableLanguages[number];

export const useLang = () => {
  const [lang, setLang] = useAppStorageValue('lang');

  return [lang ?? AvailableLanguages[0], setLang] as const;
};
