import { SetStateAction, useCallback, useEffect, useState } from 'react';
import { applyStateAction, StateHandle } from 'app/util/hooks';
import { isNil } from 'lodash-es';
import { ObservableStorage } from 'app/util/observableStorage';

export const createStorageHook = (storage: ObservableStorage) => (
  (key: string): StateHandle<string | undefined> => {
    const [value, setValue] = useState<string>();

    useEffect(() => {
      setValue(storage.getItem(key));
    }, [setValue]);

    useEffect(() => {
      const listener = storage.subscribe(key, setValue);

      return () => {
        storage.unsubscribe(key, listener);
      };
    }, [key]);

    const setValueH = useCallback((value: SetStateAction<string | undefined>) => {
      const val = applyStateAction(storage.getItem(key), value);

      if (isNil(val)) {
        storage.dropItem(key);
      } else {
        storage.setItem(key, val);
      }
    }, []);

    return [value, setValueH];
  }
);
