import { SetStateAction, useCallback, useEffect, useState } from 'react';
import { isNil, noop, uniqueId, unset } from 'lodash-es';
import { applyStateAction } from 'app/util/hooks';

export type ObservableStorageListener = (value?: string) => unknown;

export interface ObservableStorage {
  subscribe(key: string, cb: ObservableStorageListener): string;

  unsubscribe(key: string, id: string): void;

  setItem(key: string, value: string): void;

  getItem(key: string): string | undefined;

  dropItem(key: string): void;
}

export class LocalObservableStorage implements ObservableStorage {
  private readonly listeners: Record<string, Record<string, ObservableStorageListener>>;

  constructor() {
    this.listeners = {};
  }

  subscribe(key: string, cb: ObservableStorageListener) {
    const listenerId = uniqueId('obstor-');
    this.listeners[key] = { ...this.listeners[key], [listenerId]: cb };
    return listenerId;
  }

  unsubscribe(key: string, id: string) {
    unset(this.listeners, [key, id]);
  }

  setItem(key: string, value: string) {
    localStorage.setItem(key, value);
    this.notify(key);
  }

  getItem(key: string) {
    return localStorage.getItem(key) ?? undefined;
  }

  dropItem(key: string) {
    localStorage.removeItem(key);
    this.notify(key);
  }

  private notify(key: string) {
    for (const cb of Object.values(this.listeners[key] ?? {})) {
      (cb ?? noop)(this.getItem(key));
    }
  }
}
