import { Dispatch, SetStateAction, useCallback, useMemo } from 'react';
import { debounce, isArray, isFunction } from 'lodash-es';
import { useLocation } from 'react-router-dom';
import queryString from 'query-string';
import { isNotNil } from 'app/util/index';

export type StateHandle<T> = [T, Dispatch<SetStateAction<T>>];

export const useDebouncedSetState = <T>(setState: Dispatch<SetStateAction<T>>, timeout: number) => (
  useCallback(debounce((value: SetStateAction<T>) => {
    setState(value);
  }, timeout), [setState])
);

export const applyStateAction = <T>(value: T, action: SetStateAction<T>) => (
  isFunction(action)
    ? action(value)
    : action
);

export const useQueryVariables = () => {
  const { hash } = useLocation();

  return queryString.parse(hash);
};

export const useQueryVariable = (name: string) => {
  const { [name]: value } = useQueryVariables();

  return useMemo((): string[] | string | undefined => {
    if (isArray(value)) {
      return value.filter(isNotNil);
    } else {
      return value ?? undefined;
    }
  }, [value]);
};

// export const useRelativeToCurrent = ()
