import { get, isNumber, isObject, isString } from 'lodash-es';
import { OneProperty } from 'app/util/index';


export const hasResponseStatus = <T>(value: T): value is T & OneProperty<'status', number> => (
  isObject(value)
  && isNumber(get(value, 'status'))
);

export const hasResponseStatusText = <T>(value: T): value is T & OneProperty<'statusText', string> => (
  isObject(value)
  && isString(get(value, 'statusText'))
);
