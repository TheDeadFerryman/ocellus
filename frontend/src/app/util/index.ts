import { get, isNil, isNumber, isObject, isString } from 'lodash-es';

export type OneProperty<S extends string | symbol, V> = {
  [k in S]: V;
};

const EmailRegex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

export const isEmail = (email: string) => email.search(EmailRegex) !== -1;

type Falsey = undefined | null | false | 0 | '';
export const isTruthy = <T>(value: T): value is Exclude<T, Falsey> => !!value;

export const isNotNil = <T>(value: T): value is NonNullable<Exclude<T, Falsey>> => !isNil(value);

export const hasStringProperty = <T, P extends string | symbol>(
  obj: T, key: P,
): obj is T & OneProperty<P, string> => (
  isObject(obj)
  && isString(get(obj, key))
);

export const tryGetJsonBodyError = (e: unknown) => {
  const body = get(e, 'body');
  if (!isString(body)) {
    return undefined;
  }
  try {
    const errorData = JSON.parse(body);
    let status = get(errorData, 'status');
    if (isNumber(status) && status >= 500) {
      console.error(errorData);
      return 'Server error';
    }
    return String(
      get(errorData, 'detail') ?? get(errorData, 'message'),
    );
  } catch {
    return undefined;
  }
};

const ErrorProperties = [
  'detail', 'error', 'message', 'statusText', 'data',
] as const;

export const tryGetPropertyError = (e: unknown) => {
  for (const prop of ErrorProperties) {
    if (hasStringProperty(e, prop)) {
      return e[prop];
    }
  }

  return undefined;
};

export const tryGetError = (e: unknown) => (
  tryGetJsonBodyError(e) ?? tryGetPropertyError(e)
);
