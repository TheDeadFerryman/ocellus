import React from 'react';
import LoadingSplash from 'kit/components/LoadingSplash';

const LoadingPage: React.FC = () => (
  <div className={'h-full w-full relative'}>
    <LoadingSplash
      visible
      className={'absolute h-full w-full'}
    />
  </div>
);

export default LoadingPage;
