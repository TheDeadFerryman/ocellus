import React from 'react';
import { useNavigate } from 'react-router-dom';

export interface PageNotFoundProps {
  error: string;
}

const ErrorPage: React.FC<PageNotFoundProps> = ({
  error,
}) => {
  const navigate = useNavigate();

  return (
    <div className={'h-full relative flex flex-row items-center'}>
      <div className={'flex flex-col items-center grow gap-6'}>
        <h1 className={'text-error text-6xl glow-error'}>
          {error}
        </h1>
        <a
          className={'text-3xl cursor-pointer'}
          onClick={() => navigate(-1)}
        >
          Go back
        </a>
      </div>
    </div>
  );
};

export default ErrorPage;
