import React from 'react';
import ErrorPage from 'app/router/ErrorPage';

export interface PageNotFoundProps {

}

const PageNotFound: React.FC<PageNotFoundProps> = () => (
  <ErrorPage error={'Page not found!'} />
);

export default PageNotFound;
