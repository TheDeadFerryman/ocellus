import React, { useEffect, useState } from 'react';
import { useFetchMe } from 'app/api/hooks';
import LoadingPage from 'app/router/LoadingPage';
import { isNil } from 'lodash-es';
import { useNavigate } from 'react-router-dom';
import { MeContext } from 'app/router/MeContext';
import { AppTokenProvider } from 'app/config';

export interface PrivateRouteProps {
  render: React.ReactNode;
}

const PrivateRoute: React.FC<PrivateRouteProps> = ({
  render,
}) => {
  const { data, loading, fetch } = useFetchMe();
  const [fetched, setFetched] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    fetch();
    setFetched(true);
  }, [fetch]);

  useEffect(() => {
    const listener = AppTokenProvider.subscribe(() => {
      fetch();
    });

    return () => {
      AppTokenProvider.unsubscribe(listener);
    };
  }, [fetch]);

  if (loading || !fetched) {
    return (
      <LoadingPage />
    );
  }

  if (isNil(data)) {
    AppTokenProvider.dropToken();
    navigate('/login');
    return (
      <LoadingPage />
    );
  }

  const contextData = {
    cmdr: data, refetch: fetch,
  };

  return (
    <MeContext.Provider value={contextData}>
      {render}
    </MeContext.Provider>
  );
};

export default PrivateRoute;
