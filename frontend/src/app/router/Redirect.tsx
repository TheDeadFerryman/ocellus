import React, { Fragment, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

export interface RedirectProps {
  to: string;
}

const Redirect: React.FC<RedirectProps> = ({
  to,
}) => {
  const navigate = useNavigate();

  useEffect(() => navigate(to), [to]);

  return (
    <Fragment />
  );
};

export default Redirect;
