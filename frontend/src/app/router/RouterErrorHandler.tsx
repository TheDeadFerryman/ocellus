import React from 'react';
import { useRouteError } from 'react-router-dom';
import { hasResponseStatus } from 'app/util/router';
import PageNotFound from 'app/router/PageNotFound';
import ErrorPage from 'app/router/ErrorPage';
import { tryGetError } from 'app/util';

export interface RouterErrorHandlerProps {

}


const RouterErrorHandler: React.FC<RouterErrorHandlerProps> = () => {
  const error = useRouteError();

  if (hasResponseStatus(error) && error.status === 404) {
    return <PageNotFound />;
  }

  const errorText = tryGetError(error) ?? 'Unexpected error!';

  return (
    <ErrorPage error={errorText} />
  );
};

export default RouterErrorHandler;
