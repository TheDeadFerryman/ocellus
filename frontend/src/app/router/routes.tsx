import Redirect from 'app/router/Redirect';
import Login from 'app/pages/Login';
import Register from 'app/pages/Register';
import PrivateRoute from 'app/router/PrivateRoute';
import Fleets from 'app/pages/Fleets';
import FleetManage from 'app/pages/fleet/Manage';
import FleetShipsPage from 'app/pages/fleet/ships';
import * as React from 'react';
import { createBrowserRouter } from 'react-router-dom';
import RouterErrorHandler from 'app/router/RouterErrorHandler';
import NewShipModelSelect from 'app/pages/fleet/ships/new/NewShipModelSelect';
import NewShipPage from 'app/pages/fleet/ships/new';
import ShipyardPage from 'app/pages/fleet/ships/ShipyardPage';
import Recover from 'app/pages/recover';
import RecoverCommit from 'app/pages/recover/RecoverCommit';

const privateRoute = (render: React.ReactNode) => (
  <PrivateRoute render={render} />
);

export const appRouter = createBrowserRouter([
  {
    path: '/',
    errorElement: <RouterErrorHandler />,
    children: [
      {
        path: '',
        element: <Redirect to={'fleets'} />,
      },
      {
        path: 'login',
        element: <Login />,
      },
      {
        path: 'recover',
        children: [
          {
            path: '',
            element: <Recover />,
          },
          {
            path: 'commit',
            element: <RecoverCommit />,
          },
        ],
      },
      {
        path: 'register',
        element: <Register />,
      },
      {
        path: 'fleet',
        element: <Redirect to={'/fleets'} />,
      },
      {
        path: 'fleets',
        children: [
          {
            path: '',
            element: privateRoute(<Fleets />),
          },
          {
            path: ':fleetId',
            children: [
              {
                path: '',
                element: <Redirect to={'ships'} />,
              },
              {
                path: 'manage',
                element: privateRoute(<FleetManage />),
              },
              {
                path: 'ships',
                children: [
                  {
                    path: '',
                    element: privateRoute(<FleetShipsPage />),
                  },
                  {
                    path: 'new',
                    children: [
                      {
                        path: '',
                        element: privateRoute(<NewShipModelSelect />),
                      },
                      {
                        path: ':shipModel',
                        element: privateRoute(<NewShipPage />),
                      },
                    ],
                  },
                  {
                    path: ':shipId',
                    element: privateRoute(<ShipyardPage />),
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
]);
