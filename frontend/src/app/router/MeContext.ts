import React, { useContext } from 'react';
import { CommanderDTO } from 'app/api/ocellus';
import { noop } from 'lodash-es';

export interface MeContextType {
  cmdr: CommanderDTO;
  refetch: () => unknown;
}

export const MeContext = React.createContext<MeContextType>({
  cmdr: {
    id: '',
    name: '',
  },
  refetch: noop,
});

export const useMe = () => useContext(MeContext).cmdr;

export const useRefetchMe = () => useContext(MeContext).refetch;
