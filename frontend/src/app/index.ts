import * as React from 'react';
import {render} from 'react-dom';
import Ocellus from "./Ocellus";

const startApp = () => {
  const element = React.createElement(Ocellus);

  render(element, document.getElementById('coriolis'));
}

export default startApp;
