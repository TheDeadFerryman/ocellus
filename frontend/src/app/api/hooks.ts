import { useCallback, useState } from 'react';
import { CommanderAPI, FleetAPI, FleetMembersAPI, ShipsAPI } from 'app/api/index';
import { tryGetError } from 'app/util';

export interface HookOptions {
  resetData?: boolean;
}

interface QueryHookStateInt<I extends any[], O> {
  data: O | undefined;
  loading: boolean;
  error: string | undefined;
  fetch: (...args: I) => void;
  reset: () => void;
}

export type QueryHookState<F> = F extends ((...args: infer I) => Promise<infer O>)
  ? QueryHookStateInt<I, O>
  : never;

export const createQueryHook = <I extends any[], O>(
  query: (...args: I) => Promise<O>,
) => (
  ({ resetData = true }: HookOptions = {}): QueryHookStateInt<I, O> => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState<string>();
    const [data, setData] = useState<O>();

    const fetch = useCallback((...args: I) => {
      setLoading(true);
      if (resetData) {
        setData(undefined);
      }
      setError(undefined);
      query(...args)
        .then(result => {
          setData(result);
          setLoading(false);
          setError(undefined);
        })
        .catch(e => {
          setData(undefined);
          setLoading(false);
          setError(tryGetError(e) ?? 'Network error');
        })
      ;
    }, [resetData]);

    const reset = useCallback(() => {
      setLoading(false);
      setError(undefined);
      setData(undefined);
    }, [setData, setLoading, setError]);

    return { error, loading, data, fetch, reset };
  }
);

export const useFetchMe = createQueryHook(CommanderAPI.me.bind(CommanderAPI));

export const useUpdatePassword = createQueryHook(CommanderAPI.changePassword.bind(CommanderAPI));

export const useStartRecovery = createQueryHook(CommanderAPI.recover.bind(CommanderAPI));

export const useCommitRecovery = createQueryHook(CommanderAPI.recoverCommit.bind(CommanderAPI));

export const useSearchCmdrs = createQueryHook(CommanderAPI.search.bind(CommanderAPI));

export const useSearchFleets = createQueryHook(FleetAPI.searchPage1.bind(FleetAPI));

export const useFetchFleet = createQueryHook(FleetAPI.get.bind(FleetAPI));
export const useFetchFleetWithAccess = createQueryHook(FleetAPI.getWithAccessLevel.bind(FleetAPI));

export const useSearchWritableFleets = createQueryHook(FleetAPI.searchWritable.bind(FleetAPI));

export const useFetchFleetMembers = createQueryHook(FleetMembersAPI.getMembers.bind(FleetMembersAPI));

export const useAddFleetMember = createQueryHook(FleetMembersAPI.addMember.bind(FleetMembersAPI));
export const useEditFleetMember = createQueryHook(FleetMembersAPI.editMember.bind(FleetMembersAPI));

export const useKickFleetMember = createQueryHook(FleetMembersAPI.kickMember.bind(FleetMembersAPI));

export const useCreateFleet = createQueryHook(FleetAPI.create1.bind(FleetAPI));
export const useEditFleet = createQueryHook(FleetAPI.update.bind(FleetAPI));
export const useDropFleet = createQueryHook(FleetAPI.drop.bind(FleetAPI));

export const useSearchShips = createQueryHook(ShipsAPI.searchPage.bind(ShipsAPI));

export const useCreateShip = createQueryHook(ShipsAPI.create.bind(ShipsAPI));
export const useCreateShipsBulk = createQueryHook(ShipsAPI.createBulk.bind(ShipsAPI));
export const useFetchShip = createQueryHook(ShipsAPI.get1.bind(ShipsAPI));

export const useUpdateShip = createQueryHook(ShipsAPI.update1.bind(ShipsAPI));
export const useCopyShip = createQueryHook(ShipsAPI.copy.bind(ShipsAPI));

export const useDropShip = createQueryHook(ShipsAPI.drop1.bind(ShipsAPI));
