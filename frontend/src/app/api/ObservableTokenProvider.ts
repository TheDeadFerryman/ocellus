import { TokenProvider } from 'app/api/ocellus';
import { ObservableStorage } from 'app/util/observableStorage';
import { noop, uniqueId, unset } from 'lodash-es';

class ObservableTokenProvider implements TokenProvider {
  private readonly listeners: Record<string, () => unknown>;

  constructor(private readonly storage: ObservableStorage) {
    this.listeners = {};
  }

  getToken(): Promise<string> | string {
    return this.storage.getItem('session') ?? '';
  }

  setToken(token: string) {
    this.storage.setItem('session', token);
    this.notify();
  }

  dropToken() {
    this.storage.dropItem('session');
    this.notify();
  }

  subscribe(cb: () => unknown) {
    const id = uniqueId('tokprov-');
    this.listeners[id] = cb;

    return id;
  }

  unsubscribe(id: string) {
    unset(this.listeners, [id]);
  }

  private notify() {
    for (const listener of Object.values(this.listeners)) {
      (listener ?? noop)();
    }
  }
}

export default ObservableTokenProvider;
