import {
  AccessLevel,
  CommanderControllerApi,
  FleetControllerApi,
  FleetMembersControllerApi,
  FleetShipsControllerApi,
} from 'app/api/ocellus';
import { OcellusAPIConfig } from 'app/config';

export const CommanderAPI = new CommanderControllerApi(OcellusAPIConfig);
export const FleetAPI = new FleetControllerApi(OcellusAPIConfig);
export const FleetMembersAPI = new FleetMembersControllerApi(OcellusAPIConfig);
export const ShipsAPI = new FleetShipsControllerApi(OcellusAPIConfig);


export const getAccessLevelValue = (level: AccessLevel) => {
  switch (level) {
    case 'None':
      return 0;
    case 'Read':
      return 1;
    case 'Add':
      return 2;
    case 'Edit':
      return 4;
    case 'Admin':
      return 256;
  }
};

export const hasAccess = (
  level: AccessLevel,
  requestedLevel: AccessLevel,
) => (
  getAccessLevelValue(level) >= getAccessLevelValue(requestedLevel)
);
