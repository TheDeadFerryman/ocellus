import * as React from 'react';
import { BrowserRouter, Route, RouterProvider, Routes } from 'react-router-dom';
import Login from './pages/Login';
import Register from 'app/pages/Register';
import Fleets from 'app/pages/Fleets';
import PrivateRoute from 'app/router/PrivateRoute';
import FleetManage from 'app/pages/fleet/Manage';
import PageNotFound from 'app/router/PageNotFound';
import FleetShipsPage from 'app/pages/fleet/ships';
import Redirect from 'app/router/Redirect';
import { appRouter } from 'app/router/routes';

const oldRoutes = (
  <BrowserRouter>
    <Routes>
      <Route path={'/login'} element={<Login />} />
      <Route path={'/register'} element={<Register />} />
      <Route path={'/'} element={<Redirect to={'/fleets'} />} />
      <Route path={'/fleet'} element={<Redirect to={'/fleets'} />} />
      <Route path={'/fleets'}>
        <Route path={''} element={<PrivateRoute render={<Fleets />} />} />
        <Route path={':fleetId'}>
          <Route path={'manage'} element={<PrivateRoute render={<FleetManage />} />} />
          <Route path={''} element={<Redirect to={'ships'} />} />
          <Route path={'ships'}>
            <Route path={''} element={<PrivateRoute render={<FleetShipsPage />} />} />
          </Route>
          <Route path={'*'} element={<PageNotFound />} />
        </Route>
        <Route path={'*'} element={<PageNotFound />} />
      </Route>
      <Route path={'/*'} element={<PageNotFound />} />
    </Routes>
  </BrowserRouter>
);

const Ocellus = () => {
  return (
    <div className={'w-full h-screen flex flex-col items-stretch'}>
      <RouterProvider router={appRouter} />
    </div>
  );
};

export default Ocellus;
