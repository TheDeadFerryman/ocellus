type WindowInjected = typeof window & {
  OCELLUS_SERVER_URL?: string | null;
}

const InjectedServerUrl = (window as WindowInjected).OCELLUS_SERVER_URL;


export const ServerUrl = InjectedServerUrl ?? 'http://localhost:8080';
