import ObservableTokenProvider from 'app/api/ObservableTokenProvider';
import { createConfiguration, ServerConfiguration } from 'app/api/ocellus';
import format from 'date-fns/format';
import { LocalObservableStorage } from 'app/util/observableStorage';
import { createStorageHook } from 'app/util/observableStorage/react';
import { ServerUrl } from 'app/config/injected';

export const AppStorage = new LocalObservableStorage();
export const useAppStorageValue = createStorageHook(AppStorage);

export const AppTokenProvider = new ObservableTokenProvider(AppStorage);

export const OcellusServer = new ServerConfiguration(
  ServerUrl,
  {},
);

export const OcellusAPIConfig = createConfiguration({
  baseServer: OcellusServer,
  authMethods: {
    'ocellus-bearer': {
      tokenProvider: AppTokenProvider,
    },
  },
});

const DateDisplayFormat = 'dd MMM yyyy';
const DateTimeDisplayFormat = 'dd MMM yyyy, HH:mm O';

export const formatDate = (date: Date) => (
  format(date, DateDisplayFormat)
);

export const formatDateTime = (date: Date) => (
  format(date, DateTimeDisplayFormat)
);
