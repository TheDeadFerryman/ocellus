import React, { useCallback, useEffect, useMemo, useState } from 'react';
import Input from 'kit/components/Input';
import classNames from 'classnames';
import Button from 'kit/components/Button';
import { isArray, isEmpty, isNil } from 'lodash-es';
import { useCommitRecovery, useStartRecovery } from 'app/api/hooks';
import FormPage from 'app/components/page/FormPage';
import { isNotNil, isTruthy } from 'app/util';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useQueryVariable } from 'app/util/hooks';

const RecoverCommit: React.FC = () => {
  const navigate = useNavigate();

  const tokenIn = useQueryVariable('token');

  const token = useMemo(() => (
    isArray(tokenIn)
      ? tokenIn[0] ?? undefined
      : tokenIn
  ), [tokenIn]);

  const [password, setPassword] = useState('');
  const [repass, setRepass] = useState('');
  const [formErrors, setFormErrors] = useState<string[]>([]);

  const {
    loading, error, data,
    fetch, reset,
  } = useCommitRecovery();

  const startRecovery = useCallback(() => {
    const errors: string[] = [];
    if (isNil(token)) {
      errors.push('Missing recovery token!');
    }
    if (repass !== password) {
      errors.push('Passwords must match!');
    }
    if (isEmpty(password)) {
      errors.push('Provide a password!');
    }

    setFormErrors(errors);

    if (isEmpty(errors) && !isNil(token)) {
      fetch({ token, password });
    }
  }, [token, password, repass, fetch]);

  const errors = useMemo(() => [
    ...formErrors, error,
  ].filter(isNotNil), [formErrors, error]);

  useEffect(() => {
    if (isTruthy(data)) {
      navigate('/login');
    }
  }, [data]);

  return (
    <FormPage
      loading={loading}
      title={<>
        Account recovery <span className={'whitespace-nowrap'}>(Step 2)</span>
      </>}
      footer={(
        <>
          <Button
            disabled={loading}
            onClick={startRecovery}
          >
            Set password
          </Button>
        </>
      )}
    >
      <div className={classNames(
        'flex flex-col items-stretch',
        { hidden: isEmpty(errors) },
      )}>
        {errors.map(error => (
          <div className={'text-error'}>
            {error}
          </div>
        ))}
      </div>

      <div className={'flex flex-col items-stretch'}>
        <label htmlFor={'password'}>New password</label>
        <Input
          id={'password'}
          name={'password'}
          type={'password'}
          value={password}
          onChange={setPassword}
        />
      </div>
      <div className={'flex flex-col items-stretch'}>
        <label htmlFor={'repass'}>Repeat password</label>
        <Input
          id={'repass'}
          name={'repass'}
          type={'password'}
          value={repass}
          onChange={setRepass}
        />
      </div>
      <div className={'w-64'} />
    </FormPage>
  );
};

export default RecoverCommit;
