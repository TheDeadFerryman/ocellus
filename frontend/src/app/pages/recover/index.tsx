import React, { useCallback, useState } from 'react';
import Input from 'kit/components/Input';
import classNames from 'classnames';
import Button from 'kit/components/Button';
import { isNil } from 'lodash-es';
import { useStartRecovery } from 'app/api/hooks';
import FormPage from 'app/components/page/FormPage';
import { isTruthy } from 'app/util';

const Recover: React.FC = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');

  const {
    loading, error, data,
    fetch, reset,
  } = useStartRecovery();

  const startRecovery = useCallback(() => {
    fetch({ name, email });
  }, [name, email, fetch]);

  return (
    <FormPage
      loading={loading}
      title={<>
        Account recovery <span className={'whitespace-nowrap'}>(Step 1)</span>
      </>}
      footer={(
        <>
          <Button
            disabled={loading}
            onClick={startRecovery}
          >
            Recover
          </Button>
        </>
      )}
    >
      <div className={classNames(
        'text-error',
        { hidden: isNil(error) },
      )}>
        {error}
      </div>
      <div className={classNames(
        'text-secondary',
        { 'invisible max-h-0': !isTruthy(data) },
      )}>
        Check your email for further instructions!
      </div>
      <div className={'flex flex-col'}>
        <label htmlFor={'login'}>Call sign</label>
        <Input
          id={'login'}
          name={'login'}
          value={name}
          onChange={setName}
        />
      </div>
      <div className={'flex flex-col'}>
        <label htmlFor={'email'}>Email</label>
        <Input
          id={'email'}
          name={'email'}
          type={'email'}
          value={email}
          onChange={setEmail}
        />
      </div>
    </FormPage>
  );
};

export default Recover;
