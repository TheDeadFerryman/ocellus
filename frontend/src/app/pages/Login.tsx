import React, { useCallback, useEffect, useState } from 'react';
import Input from 'kit/components/Input';
import classNames from 'classnames';
import Button from 'kit/components/Button';
import LoadingSplash from 'kit/components/LoadingSplash';
import { CommanderAPI } from 'app/api';
import { isNil } from 'lodash-es';
import { AppTokenProvider } from 'app/config';
import { useFetchMe } from 'app/api/hooks';
import { Link, useNavigate } from 'react-router-dom';
import FormPage from 'app/components/page/FormPage';

const Login: React.FC = () => {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [submitLoading, setSubmitLoading] = useState(false);
  const [error, setError] = useState<string | undefined>(undefined);

  const { loading: meLoading, data, fetch } = useFetchMe();
  const navigate = useNavigate();

  useEffect(() => {
    fetch();
  }, []);

  useEffect(() => {
    if (!isNil(data)) {
      navigate('/');
    }
  }, [data]);

  const onLogin = useCallback(() => {
    setSubmitLoading(true);
    CommanderAPI.login({
      name, password,
    })
      .then(({ token }) => {
        if (!isNil(token)) {
          setSubmitLoading(false);
          setError(undefined);
          AppTokenProvider.setToken(token);
          fetch();
        } else {
          setSubmitLoading(false);
          setError('Invalid credentials!');
        }
      })
      .catch(e => {
        setError('Network error!');
        setSubmitLoading(false);
      });
  }, [name, password]);

  const loading = meLoading || submitLoading;

  return (
    <FormPage
      loading={loading}
      title={'Welcome back, CMDR!'}
      footer={(
        <>
          <Button
            disabled={loading}
            onClick={onLogin}
          >
            Log in
          </Button>
          <Link to={'/register'}>
            Register
          </Link>
        </>
      )}
    >
      <div className={classNames(
        'text-error',
        { hidden: isNil(error) },
      )}>
        {error}
      </div>
      <div className={'flex flex-col'}>
        <label htmlFor={'login'}>Call sign</label>
        <Input
          id={'login'}
          name={'login'}
          value={name}
          onChange={setName}
        />
      </div>
      <div className={'flex flex-col'}>
        <div className={'flex flex-row justify-between items-center'}>
          <label htmlFor={'password'}>Password</label>
          <Link
            to={'/recover'}
            className={'text-sm'}
          >
            Recover
          </Link>
        </div>
        <Input
          id={'password'}
          name={'password'}
          type={'password'}
          value={password}
          onChange={setPassword}
        />
      </div>
    </FormPage>
  );
};

export default Login;
