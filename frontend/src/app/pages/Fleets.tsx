import React from 'react';
import BasicPage from 'app/components/page/BasicPage';
import FleetsList from 'app/components/fleet/FleetsList';

const Fleets: React.FC = () => (
  <BasicPage
    title={'Dashboard'}
    bodyClassName={'py-8'}
  >
    <div className={'flex flex-col items-stretch grow px-16'}>
      <FleetsList />
    </div>
  </BasicPage>
);

export default Fleets;
