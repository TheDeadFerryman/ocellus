import React, { useCallback, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import PageNotFound from 'app/router/PageNotFound';
import { isNil } from 'lodash-es';
import BasicPage from 'app/components/page/BasicPage';
import FleetManager from 'app/components/fleet/fleetManager';
import { useFetchFleet } from 'app/api/hooks';

export interface ManageProps {

}

const Manage: React.FC<ManageProps> = () => {
  const { fleetId } = useParams();

  const { data, loading, error, fetch } = useFetchFleet();

  const refetch = useCallback(() => {
    fetch(fleetId ?? '');
  }, [fleetId]);

  useEffect(() => {
    refetch();
  }, [refetch]);

  if (isNil(fleetId)) {
    return (
      <PageNotFound />
    );
  }

  return (
    <BasicPage
      error={error}
      loading={loading}
      bodyClassName={'py-8'}
      title={data?.name ?? 'Fleet'}
    >
      <div className={'flex flex-col px-16 grow items-stretch'}>
        <FleetManager
          fleet={data}
          onUpdate={refetch}
        />
      </div>
    </BasicPage>
  );
};

export default Manage;
