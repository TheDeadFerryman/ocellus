import React, { useCallback, useEffect } from 'react';
import BasicPage from 'app/components/page/BasicPage';
import { useParams } from 'react-router-dom';
import { useFetchShip } from 'app/api/hooks';
import { isNil } from 'lodash-es';
import Shipyard from 'app/components/fleet/ships/shipyard';

export interface ShipyardPageProps {

}

const ShipyardPage: React.FC<ShipyardPageProps> = () => {
  const { shipId, fleetId } = useParams();

  const {
    data, loading, error, fetch,
  } = useFetchShip();

  const refetch = useCallback(() => {
    console.log('logogo', fleetId, shipId);
    if (!isNil(fleetId) && !isNil(shipId)) {
      fetch(fleetId, shipId);
    }
  }, [fleetId, shipId, fetch]);

  useEffect(() => refetch(), [refetch]);

  return (
    <BasicPage
      error={error}
      loading={loading || isNil(fleetId)}
      title={data?.ship.name ?? 'Ship'}
    >
      {(data && !isNil(fleetId)) && (
        <Shipyard
          ship={data.ship}
          fleetId={fleetId}
          onUpdate={refetch}
          accessLevel={data.level}
        />
      )}
    </BasicPage>
  );
};

export default ShipyardPage;
