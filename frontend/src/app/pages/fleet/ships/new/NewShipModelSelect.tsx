import React, { useEffect, useState } from 'react';
import BasicPage from 'app/components/page/BasicPage';
import { Ships } from 'data/coriolis';
import classNames from 'classnames';
import { Select } from 'kit';
import { useNavigate } from 'react-router-dom';
import { useDebouncedSetState } from 'app/util/hooks';
import { isNil } from 'lodash-es';

const ShipOptions = Object.keys(Ships).map(ship => ({
  value: ship,
  render: Ships[ship]?.properties.name,
} as const));

const NewShipModelSelect: React.FC = () => {
  const [model, setModel] = useState<string>();
  const navigate = useNavigate();

  useEffect(() => {
    if (!isNil(model)) {
      navigate(`${model}`);
    }
  }, [model]);

  const selectModel = useDebouncedSetState(setModel, 2000);

  return (
    <BasicPage
      title={'New ship'}
    >
      <div className={'flex flex-col grow items-center'}>
        <div className={'flex flex-row grow items-center'}>
          <div className={classNames(
            'flex flex-col items-stretch p-3 gap-2',
            'border-shade border-2',
          )}>
            <h3 className={'glow-primary'}>Creating a ship</h3>
            <div className={'flex flex-col'}>
              <label>Ship model</label>
              <Select
                maxHeight={200}
                className={'w-64'}
                options={ShipOptions}
                value={model}
                onChange={setModel}
              />
            </div>
          </div>
        </div>
      </div>
    </BasicPage>
  );
};

export default NewShipModelSelect;
