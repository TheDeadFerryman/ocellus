import React, { useMemo, useState } from 'react';
import { isNil } from 'lodash-es';
import { useParams } from 'react-router-dom';
import BasicPage from 'app/components/page/BasicPage';
import { Ships } from 'data/coriolis';
import NewShip from 'app/components/fleet/ships/new';

export interface IndexProps {

}

const NewShipPage: React.FC<IndexProps> = () => {
  const { shipModel, fleetId } = useParams();
  const [editorLoading, setEditorLoading] = useState(false);

  const modelName = useMemo(() => (
    (
      isNil(shipModel)
        ? undefined
        : Ships[shipModel]?.properties.name
    ) ?? 'Ship'
  ), [shipModel]);

  const pageLoading = useMemo(() => (
    isNil(shipModel) || isNil(fleetId)
  ), [shipModel, fleetId, editorLoading]);

  return (
    <BasicPage
      title={modelName}
      loading={pageLoading}
      bodyClassName={'py-4'}
    >
      {shipModel && fleetId && (
        <NewShip
          model={shipModel}
          fleetId={fleetId}
          onLoadingStateChange={setEditorLoading}
        />
      )}
    </BasicPage>
  );
};

export default NewShipPage;
