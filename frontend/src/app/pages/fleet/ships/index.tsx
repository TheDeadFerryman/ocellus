import React, { useCallback, useEffect } from 'react';
import BasicPage from 'app/components/page/BasicPage';
import { useParams } from 'react-router-dom';
import { isNil } from 'lodash-es';
import PageNotFound from 'app/router/PageNotFound';
import { useFetchFleetWithAccess } from 'app/api/hooks';
import FleetShips from 'app/components/fleet/ships';

export interface InspectProps {

}

const FleetShipsPage: React.FC<InspectProps> = () => {
  const { fleetId } = useParams();

  const { data, loading, error, fetch } = useFetchFleetWithAccess();

  const refetch = useCallback(() => {
    fetch(fleetId ?? '');
  }, [fleetId]);

  useEffect(() => {
    refetch();
  }, [refetch]);

  if (isNil(fleetId)) {
    return (
      <PageNotFound />
    );
  }

  return (
    <BasicPage
      error={error}
      loading={loading}
      bodyClassName={'py-8'}
      title={data?.fleet.name ?? 'Fleet'}
    >
      <div className={'flex flex-col px-16 grow items-stretch'}>
        <h1 className={'glow-primary'}>{data?.fleet.name ?? 'Fleet'}</h1>
        <h2 className={'text-shade'}>
          Owner: CMDR <span className={'normal-case'}>{data?.fleet.owner.name}</span>
        </h2>
        {!isNil(data) && (
          <FleetShips
            fleet={data.fleet}
            accessLevel={data.level}
          />
        )}
      </div>
    </BasicPage>
  );
};

export default FleetShipsPage;
