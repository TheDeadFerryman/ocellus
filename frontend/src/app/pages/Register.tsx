import React, { useCallback, useEffect, useState } from 'react';
import FormPage from 'app/components/page/FormPage';
import Input from 'kit/components/Input';
import classNames from 'classnames';
import { isEmpty, isNil } from 'lodash-es';
import Button from 'kit/components/Button';
import { useFetchMe } from 'app/api/hooks';
import { Link, useNavigate } from 'react-router-dom';
import { isEmail, isTruthy, tryGetError } from 'app/util';
import { CommanderAPI } from 'app/api';
import { AppTokenProvider } from 'app/config';


export interface RegisterProps {

}

const Register: React.FC<RegisterProps> = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [repass, setRepass] = useState('');
  const [errors, setErrors] = useState<string[]>([]);
  const [submitLoading, setSubmitLoading] = useState(false);

  const { loading: meLoading, data, fetch } = useFetchMe();
  const navigate = useNavigate();

  useEffect(() => {
    fetch();
  }, []);

  useEffect(() => {
    if (!isNil(data)) {
      navigate('/');
    }
  }, [data]);

  const onRegister = useCallback(() => {
    const errors = [
      isEmpty(name) && 'Name is empty',
      !isEmail(email) && 'Email is invalid',
      isEmpty(password) && 'Password is empty',
      (password !== repass) && 'Passwords don\'t match',
    ].filter(isTruthy);

    if (!isEmpty(errors)) {
      setErrors(errors);
      return;
    }
    setErrors([]);
    setSubmitLoading(true);

    CommanderAPI.register({
      name, email, password,
    })
      .then(({ token }) => {
        setSubmitLoading(false);
        if (isNil(token)) {
          setErrors(['Failed to register!']);
        } else {
          AppTokenProvider.setToken(token);
          fetch();
        }
      })
      .catch(e => {
        setErrors([
          tryGetError(e) ?? 'Network error!',
        ]);
        setSubmitLoading(false);
      })
    ;

  }, [password, repass, name, email]);

  const loading = submitLoading || meLoading;

  return (
    <FormPage
      loading={loading}
      title={'Register'}
      footer={(
        <>
          <Button
            disabled={loading}
            onClick={onRegister}
          >
            Register
          </Button>
          <Link to={'/login'}>
            Login
          </Link>
        </>
      )}
    >
      <div className={classNames(
        'flex flex-col',
        'text-error',
        { hidden: isEmpty(errors) },
      )}>
        {errors.map(err => (
          <div>{err}</div>
        ))}
      </div>

      <div className={'flex flex-row gap-2'}>
        <div className={'flex flex-col'}>
          <label htmlFor={'name'}>Call sign</label>
          <Input
            id={'name'}
            value={name}
            onChange={setName}
            autoComplete={'new-password'}
          />
        </div>
        <div className={'flex flex-col'}>
          <label htmlFor={'email'}>Email</label>
          <Input
            id={'email'}
            value={email}
            type={'email'}
            onChange={setEmail}
            autoComplete={'new-password'}
          />
        </div>
      </div>
      <div className={'flex flex-row gap-2'}>
        <div className={'flex flex-col'}>
          <label htmlFor={'password'}>Password</label>
          <Input
            id={'password'}
            value={password}
            type={'password'}
            onChange={setPassword}
            autoComplete={'new-password'}
          />
        </div>
        <div className={'flex flex-col'}>
          <label htmlFor={'repass'}>Repeat password</label>
          <Input
            id={'repass'}
            value={repass}
            type={'password'}
            onChange={setRepass}
            autoComplete={'new-password'}
          />
        </div>
      </div>
    </FormPage>
  );
};

export default Register;
