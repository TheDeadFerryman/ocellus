export interface Ship {
  name: string;
  model: string;
  data: string;
}

export interface ShipConfig {
  properties: {
    name: string;
    manufacturer: string;
  };
}

const Ships = require('coriolis-data/ships') as Record<string, ShipConfig | undefined>;
const Modifications = require('coriolis-data/modifications') as Record<string, any>;
const Modules = require('coriolis-data/modules') as Record<string, any>;

export {
  Ships, Modules, Modifications,
};


export const shipModelName = (modelId: string) => (
  Ships[modelId]?.properties.name ?? modelId
);
