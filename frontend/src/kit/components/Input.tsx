import React, { forwardRef } from 'react';
import classNames from 'classnames';
import { noop } from 'lodash-es';

export interface InputControls {
  blur(): unknown;
}

export interface InputProps extends Omit<React.InputHTMLAttributes<HTMLInputElement>, 'onChange'> {
  onChange?: (value: string) => unknown;
}

const Input = forwardRef<HTMLInputElement, InputProps>(({
  className, onChange,
  ...props
}, ref) => {

  return (
    <input
      {...props}
      ref={ref}
      className={classNames(
        className,
        'font-display px-1 text-primary',
      )}
      onChange={e => (onChange ?? noop)(e.target.value)}
    />
  );
});

export default Input;
