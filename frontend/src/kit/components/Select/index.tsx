import classNames from 'classnames';
import { Input } from 'kit';
import useArrowPicker from './arrow-picker';
import { isNil, isString, noop, uniqueId } from 'lodash-es';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import scroll from 'kit/utils/scroll';

export interface BaseOption<T extends string> {
  value: T;
  render: React.ReactNode;
}

const optionsBase = classNames([
  'absolute',
  'w-full',
  'inset-x-0 z-50',
  'max-h-0',
  'bg-black',
  'text-primary',
  'opacity-0',
  'overflow-x-auto scrollable',
  'transition-all',
  'pointer-events-none',
]);

const optionsVisible = (needsMaxHeight: boolean) => classNames([
  '!opacity-100',
  '!pointer-events-auto',
  { '!max-h-96': needsMaxHeight },
]);

const optionBase = classNames([
  'p-1',
  'cursor-pointer',
  'transition',
  'active:bg-primary',
  'border border-primary',
  'not-last:!border-b-0',
]);

const optionHovered = classNames([
  'bg-primary',
  'text-black',
]);

const valueViewer = classNames([
  'absolute',
  'inset-x-0 bottom-0',
  'text-primary',
  'px-1',
  'pointer-events-none',
]);

interface SelectProps<V extends string, T extends BaseOption<V>> {
  className?: string;
  queryClassName?: string;

  maxHeight?: number;

  label?: string;
  tooltip?: React.ReactNode;

  name?: string;
  value?: V;

  options: readonly T[];

  onChange?: (value: V) => unknown;
  filter?: (option: T, query: string) => boolean,

  onFocus?: (event: React.FocusEvent<HTMLInputElement>) => unknown;
  onBlur?: (event: React.FocusEvent<HTMLInputElement>) => unknown;
}

const cssValueImportance = (value: string | number | undefined) => (
  isNil(value)
    ? value
    : (
      isString(value)
        ? `${value} !important`
        : `${value}px !important`
    )
);

const Select = <V extends string, T extends BaseOption<V>>({
  value, name,
  className, queryClassName, maxHeight,
  options, filter: inFilter,
  onChange = noop, onFocus = noop, onBlur = noop,
}: SelectProps<V, T>): JSX.Element => {
  const queryField = useRef<HTMLInputElement>(null);
  const optionsList = useRef<HTMLDivElement>(null);

  const [query, setQuery] = useState<string>();
  const [randName] = useState(uniqueId('##select-input-'));
  const [selection, setSelection] = useState<T | undefined>();
  const [focused, setFocused] = useState(false);
  const [keyPick, setKeyPick] = useState<number>();

  const realName = useMemo(() => (name ?? randName), [name, randName]);
  const filter = useMemo(() => (
    inFilter ?? ((o: T, q: string) => (
      o.value.toLowerCase().includes(q.toLowerCase())
    ))
  ), [inFilter]);
  const displaysValue = useMemo(
    () => (!isNil(selection) && !focused),
    [focused, selection],
  );
  const onQueryBlur = useCallback(
    (e: React.FocusEvent<HTMLInputElement>) => {
      onBlur(e);
      setTimeout(() => setFocused(false), 200);
    },
    [onBlur],
  );

  const filteredOptions = useMemo(() => (
    isNil(query)
      ? options
      : options.filter(e => filter(e, query))
  ), [query, options, filter]);

  const pickedValue = isNil(keyPick)
    ? undefined
    : filteredOptions[keyPick]?.value;

  const onQueryFocus = useCallback(
    (e: React.FocusEvent<HTMLInputElement>) => {
      setFocused(true);
      onFocus(e);
    },
    [onFocus],
  );

  const select = useCallback((newValue: T) => {
    setSelection(newValue);
    setQuery('');
    onChange(newValue?.value);
  }, [onChange]);

  const setArrowSelection = (id: number) => {
    select(filteredOptions[id]);
    queryField.current?.blur();
  };
  const { onKeyDown } = useArrowPicker({
    current: keyPick,
    total: filteredOptions.length,
    setPick: setKeyPick,
    setSelection: setArrowSelection,
  });

  useEffect(
    () => setSelection(options.find(o => o.value === value)),
    [options, value],
  );

  useEffect(
    () => setKeyPick(undefined),
    [filteredOptions],
  );
  useEffect(() => {
    if (optionsList.current && pickedValue) {
      scroll(optionsList.current).toId(pickedValue);
    }
  }, [pickedValue]);

  return (
    <div
      className={className}
    >
      <div className={'relative'}>
        <Input
          ref={queryField}
          className={classNames(
            queryClassName,
            'w-full',
          )}
          value={displaysValue ? '' : query}
          name={`##sel-qf--${realName}`}
          onBlur={onQueryBlur}
          onFocus={onQueryFocus}
          onKeyDown={onKeyDown}
          onChange={setQuery}
        />
        {displaysValue && (
          <div className={classNames(
            valueViewer,
          )}>
            {selection?.render}
          </div>
        )}
      </div>
      <div className={'relative'}>
        <div
          ref={optionsList}
          className={classNames(
            optionsBase,
            { [optionsVisible(isNil(maxHeight))]: focused },
          )}
          style={{
            maxHeight: (
              focused
                ? maxHeight
                : undefined
            ),
          }}
        >
          {filteredOptions.map((op, id) => (
            <div
              id={op.value}
              key={op.value}
              className={classNames(
                optionBase,
                { [optionHovered]: keyPick === id },
              )}
              onMouseEnter={() => setKeyPick(id)}
              onMouseLeave={() => setKeyPick(undefined)}
              onClick={() => select(op)}
            >
              {op.render}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Select;
