import React, { PropsWithChildren, useCallback, useEffect, useRef, useState } from 'react';
import { isNil, noop, uniqueId } from 'lodash-es';
import ReactDOM from 'react-dom';
import classNames from 'classnames';

export interface ModalProps extends PropsWithChildren {
  visible?: boolean;
  onClose?: () => unknown;
}

const Modal: React.FC<ModalProps> = ({
  visible = false,
  onClose = noop,
  children,
}) => {
  const [portal, setPortal] = useState<HTMLDivElement>();
  const box = useRef<HTMLDivElement>(null);

  const [id] = useState(uniqueId('modal-container-'));

  useEffect(() => {
    const elem = document.createElement('div');

    elem.id = id;

    document.body.append(elem);

    setPortal(elem);

    return () => {
      if (!isNil(portal) && document.body.contains(portal)) {
        document.body.removeChild(portal);
      }
    };
  }, [setPortal]);

  const consumeClick = useCallback((e: React.MouseEvent) => {
    e.preventDefault();
    e.stopPropagation();
  }, []);

  if (isNil(portal) || !visible) {
    return null;
  }

  return ReactDOM.createPortal((
    <div className={'relative'}>
      <div
        className={classNames(
          'fixed inset-0 z-40',
          'flex flex-row items-center',
          'bg-black/75')}
        onClick={onClose}
      >
        <div className={'flex flex-col grow items-center'}>
          <div
            ref={box}
            className={classNames(
              'flex flex-row items-stretch',
              'bg-black border-2 border-shade p-4',
            )}
            onClick={consumeClick}
          >
            {children}
          </div>
        </div>
      </div>
    </div>
  ), portal);
};

export default Modal;
