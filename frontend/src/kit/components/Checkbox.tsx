import React from 'react';
import classNames from 'classnames';

export interface CheckboxProps {
  label: React.ReactNode;
  value: boolean;
  onChange: (value: boolean) => unknown;
}

const Checkbox: React.FC<CheckboxProps> = ({
  label, value,
  onChange,
}) => {

  return (
    <div
      className={'flex flex-row items-center gap-2 cursor-pointer mr-2'}
      onClick={() => onChange(!value)}
    >
      <div
        className={classNames(
          'w-4 h-4',
          'bg-black',
          'border-shade border',
          'border-glow-shade',
          { '!border-secondary !border-glow-secondary !bg-secondary': value },
          'transition',
        )}
      >

      </div>
      <span className={'grow leading-none select-none'}>
        {label}
      </span>
    </div>
  );
};

export default Checkbox;
