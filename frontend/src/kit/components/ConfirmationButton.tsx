import React, { PropsWithChildren, useCallback, useState } from 'react';
import { Button, ButtonVariant } from 'kit/index';
import classNames from 'classnames';

const VariantMap = {
  error: {
    border: 'border-error',
    glow: 'glow-error',
    text: 'text-error',
    bg: 'bg-error',
  },
  primary: {
    border: 'border-primary',
    glow: 'glow-primary',
    text: 'text-primary',
    bg: 'bg-primary',
  },
  secondary: {
    border: 'border-secondary',
    glow: 'glow-secondary',
    text: 'text-secondary',
    bg: 'bg-secondary',
  },
  shade: {
    border: 'border-shade',
    glow: 'glow-shade',
    text: 'text-shade',
    bg: 'bg-shade',
  },
};

const PositionMap = {
  'top:left': 'bottom-full left-0 mb-2',
  'top:right': 'bottom-full right-0 mb-2',
  'bottom:left': 'top-full left-0 mt-2',
  'bottom:right': 'top-full right-0 mt-2',
  'left:top': 'right-full top-0 mr-2',
  'left:bottom': 'right-full bottom-0 mr-2',
  'right:top': 'left-full top-0 ml-2',
  'right:bottom': 'left-full bottom-0 ml-2',
};

export type PopupPosition = keyof typeof PositionMap;

export interface DissolveFleetButtonProps extends PropsWithChildren {
  variant: ButtonVariant;
  position: PopupPosition;

  className?: string;
  buttonClassName?: string;

  popup: React.ReactNode;

  onClick: () => unknown;
}

const ConfirmationButton: React.FC<DissolveFleetButtonProps> = ({
  variant, position,
  className, buttonClassName,
  children, popup: popupText,
  onClick,
}) => {
  const [visible, setVisible] = useState(false);

  const confirm = useCallback(() => {
    setVisible(false);
    onClick();
  }, [setVisible, onClick]);

  return (
    <div className={classNames(
      className,
      'relative overflow-visible w-full',
    )}>
      <Button
        variant={variant}
        onClick={() => setVisible(true)}
        className={classNames(buttonClassName)}
      >
        {children}
      </Button>
      <div
        className={classNames(
          'absolute z-20',
          PositionMap[position],
          'flex flex-row items-center gap-2',
          'p-2 border',
          'bg-black',
          VariantMap[variant].border,
          { hidden: !visible },
        )}
      >
        <h3 className={classNames(
          VariantMap[variant].text,
          VariantMap[variant].glow,
          'whitespace-nowrap',
        )}>
          {popupText}
        </h3>
        <Button
          variant={variant}
          onClick={onClick}
        >
          Confirm
        </Button>
        <Button
          variant={variant}
          onClick={() => setVisible(false)}
        >
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default ConfirmationButton;
