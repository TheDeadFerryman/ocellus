import React, { PropsWithChildren } from 'react';
import { Link } from 'react-router-dom';
import { buttonStyleVariants, ButtonVariant } from 'kit/components/Button';
import classNames from 'classnames';

export interface LinkButtonProps extends PropsWithChildren {
  to: string;

  className?: string;
  variant?: ButtonVariant;
  disabled?: boolean;
}

const LinkButton: React.FC<LinkButtonProps> = ({
  variant = 'primary', disabled = false,
  to, className,
  children,
}) => (
  <Link
    to={to}
    className={classNames(
      className,
      'px-3 py-0.5 text-lg uppercase',
      'visited:text-black',
      buttonStyleVariants(variant, disabled),
      'transition',
    )}
  >
    {children}
  </Link>
);

export default LinkButton;
