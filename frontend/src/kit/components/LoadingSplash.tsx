import React from 'react';
import Loading from 'images/icons/LoadingIcon.icon.svg';
import classNames from 'classnames';

export interface LoadingSplashProps {
  className?: string;
  visible: boolean;
}

const LoadingSplash: React.FC<LoadingSplashProps> = ({
  className, visible,
}) => (
  <div className={classNames(
    className,
    'z-20',
    'flex flex-row items-center',
    'bg-black/75',
    { hidden: !visible },
  )}>
    <div className={'flex flex-col items-center grow'}>
      <Loading className={'w-10'} />
    </div>
  </div>
);

export default LoadingSplash;
