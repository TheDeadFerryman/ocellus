import React, { useCallback, useMemo, useState } from 'react';
import { isEmpty, last, toNumber } from 'lodash-es';
import classNames from 'classnames';
import Input from 'kit/components/Input';

export interface PaginationProps {
  page: number;
  pageSize: number;

  totalPages: number;

  onPageChange: (page: number) => unknown;
  onPageSizeChange: (pageSize: number) => unknown;
}

const buildSparsePageArray = (current: number, totalPages: number) => {
  const allPages = Array(totalPages).fill(0).map((_, i) => i + 1);
  let collected: number[] = [];

  for (const page of allPages) {
    if (
      Math.abs(page - (current + 1)) <= 2
      || Math.abs(page - 1) <= 2
      || Math.abs(page - totalPages) <= 2
    ) {
      collected = [...collected, page];
    } else if (last(collected) !== -1) {
      collected = [...collected, -1];
    }
  }

  if (isEmpty(collected)) {
    return [1];
  }


  return collected;
};

const Pagination: React.FC<PaginationProps> = ({
  page, pageSize, totalPages,
  onPageChange, onPageSizeChange,
}) => {
  const pageIds = useMemo(() => (
    buildSparsePageArray(page, totalPages)
  ), [totalPages, page]);

  const [pageSizeIn, setPageSizeIn] = useState(pageSize);

  const catchKeyup = useCallback((e: React.KeyboardEvent) => {
    if (e.key === 'Enter') {
      onPageSizeChange(pageSizeIn);
    }
  }, [pageSizeIn]);

  return (
    <div className={'flex flex-row gap-2'}>
      <div className={'flex flex-row'}>
        {pageIds.map(id => (
          id === -1
            ? (
              <div
                className={classNames(
                  'p-2 border border-shade',
                  'leading-half text-shade',
                )}
              >
                &middot;&middot;&middot;
              </div>
            )
            : (
              <div
                key={id}
                className={classNames(
                  'p-2 border border-shade',
                  'leading-half text-shade bg-black',
                  'hover:!bg-shade hover:!text-black',
                  { '!bg-shade !text-black': id === (page + 1) },
                  { 'cursor-pointer': id !== (page + 1) },
                  'transition',
                )}
                onClick={() => onPageChange(id - 1)}
              >
                {id}
              </div>
            )
        ))}
      </div>
      <Input
        type={'number'}
        className={'w-16'}
        value={pageSizeIn}
        onChange={value => setPageSizeIn(toNumber(value))}
        onKeyUp={catchKeyup}
      />
    </div>
  );
};

export default Pagination;
