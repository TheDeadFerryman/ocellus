import React from 'react';
import classNames from 'classnames';
import { noop } from 'lodash-es';

export type ButtonVariant = 'primary' | 'secondary' | 'shade' | 'error';

export const buttonStyleVariants = (
  variant: ButtonVariant,
  disabled: boolean,
) => {
  const variantStyles = {
    primary: classNames(
      'border border-primary',
      'bg-primary text-black',
      'hover:bg-black hover:text-primary',
      'active:bg-primary/25',
      { '!bg-primary/75 !border-black !text-black': disabled },
    ),
    secondary: classNames(
      'border border-secondary',
      'bg-secondary text-black',
      'hover:bg-black hover:text-secondary',
      'active:bg-secondary/25',
      { '!bg-secondary/75 !border-black !text-black': disabled },
    ),
    shade: classNames(
      'border border-shade',
      'bg-shade text-black',
      'hover:bg-black hover:text-shade',
      'active:bg-shade/25',
      { '!bg-shade/75 !border-black !text-black': disabled },
    ),
    error: classNames(
      'border border-error',
      'bg-error text-black',
      'hover:bg-black hover:text-error',
      'active:bg-error/25',
      { '!bg-error/75 !border-black !text-black': disabled },
    ),
  } as const;

  return classNames(
    variantStyles[variant],
  );
};

export interface ButtonProps extends React.PropsWithChildren {
  className?: string;

  variant?: ButtonVariant;
  disabled?: boolean;

  onClick?: () => unknown;
}

const Button: React.FC<ButtonProps> = ({
  className, variant = 'primary',
  disabled = false,
  onClick, children,
}) => (
  <button
    className={classNames(
      className,
      'px-3 py-0.5 text-lg uppercase',
      buttonStyleVariants(variant, disabled),
      'transition',
    )}
    onClick={disabled ? noop : onClick}
  >
    {children}
  </button>
);

export default Button;
