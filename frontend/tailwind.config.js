/** @type {import('tailwindcss').Config} */
const plugin = require('tailwindcss/plugin');
module.exports = {
  content: [
    './src/{app,kit}/**/*.{ts,tsx}',
    './index.ejs',
    './src/app/coriolis/pages/OutfittingPage.jsx',
  ],
  theme: {
    extend: {
      colors: {
        primary: '#FF8C0D',
        shade: '#ff7100',
        secondary: '#00b3f7',
        error: '#FF3B00',
      },
      fontFamily: {
        display: ['Eurostile', 'sans-serif'],
        mono: ['JetBrains Mono', 'monospace']
      },
      lineHeight: {
        collapse: 0,
        half: '0.5',
      },
    },
  },
  plugins: [
    plugin(({ addVariant }) => {
      addVariant('not-last', '&:not(:last-child)');
      addVariant('not-first', '&:not(:first-child)');
      addVariant('in-hover', ':hover > &');
    }),
    plugin(({ matchUtilities, theme }) => {
      matchUtilities(
        {
          glow: value => ({
            textShadow: ('0 0 8px ' + value),
          }),
          'border-glow': value => ({
            boxShadow: (
              '0 0 4px ' + value + ', ' +
              'inset 0 0 4px' + value
            ),
          }),
        },
        { values: theme('colors') },
      );
    }),
  ],
};

